//
//  DeviceKit.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 06/10/2021.
//

import UIKit

extension UIDevice {
    
    var modelType: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        return mapToDevice(identifier: identifier)
    }
    
    func mapToDevice(identifier: String) -> UIDevice.Model {
        
        switch identifier {
        case "iPod5,1":
            return .iPodTouch
        case "iPod7,1":
            return .iPodTouch
        case "iPod9,1":
            return .iPodTouch
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":
            return .iPhone4
        case "iPhone4,1":
            return .iPhone4s
        case "iPhone5,1", "iPhone5,2":
            return .iPhone5
        case "iPhone5,3", "iPhone5,4":
            return .iPhone5c
        case "iPhone6,1", "iPhone6,2":
            return .iPhone5s
        case "iPhone7,2":
            return .iPhone6
        case "iPhone7,1":
            return .iPhone6Plus
        case "iPhone8,1":
            return .iPhone6s
        case "iPhone8,2":
            return .iPhone6sPlus
        case "iPhone8,4":
            return .iPhoneSE
        case "iPhone9,1", "iPhone9,3":
            return .iPhone7
        case "iPhone9,2", "iPhone9,4":
            return .iPhone7Plus
        case "iPhone10,1", "iPhone10,4":
            return .iPhone8
        case "iPhone10,2", "iPhone10,5":
            return .iPhone8Plus
        case "iPhone10,3", "iPhone10,6":
            return .iPhoneX
        case "iPhone11,2":
            return .iPhoneXS
        case "iPhone11,4", "iPhone11,6":
            return .iPhoneXSMax
        case "iPhone11,8":
            return .iPhoneXR
        case "iPhone12,1":
            return .iPhone11
        case "iPhone12,3":
            return .iPhone11Pro
        case "iPhone12,5":
            return .iPhone11ProMax
        case "iPhone12,8":
            return .iPhoneSE2th
        case "iPhone13,1":
            return .iPhone12Mini
        case "iPhone13,2":
            return .iPhone12
        case "iPhone13,3":
            return .iPhone12Pro
        case "iPhone13,4":
            return .iPhone12ProMax
        case "iPhone14,4":
            return .iPhone13Mini
        case "iPhone14,5":
            return .iPhone13
        case "iPhone14,2":
            return .iPhone13Pro
        case "iPhone14,3":
            return .iPhone13ProMax
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":
            return .iPad2th
        case "iPad3,1", "iPad3,2", "iPad3,3":
            return .iPad3th
        case "iPad3,4", "iPad3,5", "iPad3,6":
            return .iPad4th
        case "iPad6,11", "iPad6,12":
            return .iPad5th
        case "iPad7,5", "iPad7,6":
            return .iPad6th
        case "iPad7,11", "iPad7,12":
            return .iPad7th
        case "iPad11,6", "iPad11,7":
            return .iPad8th
        case "iPad12,1", "iPad12,2":
            return .iPad9th
        case "iPad4,1", "iPad4,2", "iPad4,3":
            return .iPadAit1th
        case "iPad5,3", "iPad5,4":
            return .iPadAit2th
        case "iPad11,3", "iPad11,4":
            return .iPadAit3th
        case "iPad13,1", "iPad13,2":
            return .iPadAit4th
        case "iPad2,5", "iPad2,6", "iPad2,7":
            return .iPadMini1th
        case "iPad4,4", "iPad4,5", "iPad4,6":
            return .iPadMini2th
        case "iPad4,7", "iPad4,8", "iPad4,9":
            return .iPadMini3th
        case "iPad5,1", "iPad5,2":
            return .iPadMini4th
        case "iPad11,1", "iPad11,2":
            return .iPadMini5th
        case "iPad14,1", "iPad14,2":
            return .iPadMini6th
        case "iPad6,3", "iPad6,4":
            return .iPadPro9_7Inches
        case "iPad7,3", "iPad7,4":
            return .iPadPro10_5Inches
        case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":
            return .iPadPro11Inches1th
        case "iPad8,9", "iPad8,10":
            return .iPadPro11Inches2th
        case "iPad13,4", "iPad13,5", "iPad13,6", "iPad13,7":
            return .iPadPro11Inches3th
        case "iPad6,7", "iPad6,8":
            return .iPadPro12_9Inches1th
        case "iPad7,1", "iPad7,2":
            return .iPadPro12_9Inches2th
        case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":
            return .iPadPro12_9Inches3th
        case "iPad8,11", "iPad8,12":
            return .iPadPro12_9Inches4th
        case "iPad13,8", "iPad13,9", "iPad13,10", "iPad13,11":
            return .iPadPro12_9Inches5th
        case "i386", "x86_64":
            
            guard let simulatorIndentifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] else {
                return .notSupportedDevice
            }
           
            return mapToDevice(identifier: simulatorIndentifier)
            
        default:
            return .notSupportedDevice
        }
    }
}

extension UIDevice {
    enum Model {
        case iPodTouch
        
        case iPhone4
        case iPhone4s
        
        case iPhone5
        case iPhone5c
        case iPhone5s
        
        case iPhone6
        case iPhone6Plus
        case iPhone6s
        case iPhone6sPlus
        case iPhoneSE
        
        case iPhone7
        case iPhone7Plus
        
        case iPhone8
        case iPhone8Plus
        
        case iPhoneX
        case iPhoneXS
        case iPhoneXSMax
        case iPhoneXR
        
        case iPhone11
        case iPhone11Pro
        case iPhone11ProMax
        
        case iPhoneSE2th
        
        case iPhone12Mini
        case iPhone12
        case iPhone12Pro
        case iPhone12ProMax
        
        case iPhone13Mini
        case iPhone13
        case iPhone13Pro
        case iPhone13ProMax
        
        case iPad2th
        case iPad3th
        case iPad4th
        case iPad5th
        case iPad6th
        case iPad7th
        case iPad8th
        case iPad9th
        
        case iPadAit1th
        case iPadAit2th
        case iPadAit3th
        case iPadAit4th
        
        case iPadMini1th
        case iPadMini2th
        case iPadMini3th
        case iPadMini4th
        case iPadMini5th
        case iPadMini6th
        
        case iPadPro9_7Inches
        case iPadPro10_5Inches
        
        case iPadPro11Inches1th
        case iPadPro11Inches2th
        case iPadPro11Inches3th
        
        case iPadPro12_9Inches1th
        case iPadPro12_9Inches2th
        case iPadPro12_9Inches3th
        case iPadPro12_9Inches4th
        case iPadPro12_9Inches5th
        
        case notSupportedDevice
        
        var isIPadDevices: Bool {
            
            switch self {
            case .iPad2th, .iPad3th, .iPad4th, .iPad5th, .iPad6th, .iPad7th, .iPad8th,.iPad9th:
                return true
                
            case .iPadAit1th, .iPadAit2th, .iPadAit3th, .iPadAit4th:
                return true
                
            case .iPadMini1th, .iPadMini2th, .iPadMini3th, .iPadMini4th, .iPadMini5th, .iPadMini6th:
                return true
                
            case .iPadPro9_7Inches, .iPadPro10_5Inches:
                return true
                
            case .iPadPro11Inches1th, .iPadPro11Inches2th, .iPadPro11Inches3th:
                return true
                
            case .iPadPro12_9Inches1th, .iPadPro12_9Inches2th, .iPadPro12_9Inches3th, .iPadPro12_9Inches4th, .iPadPro12_9Inches5th:
                return true
                
            default:
                return false
            }
        }
    }
}

//
//  UITextView+Extension.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 18/09/2021.
//

import UIKit
import RxCocoa
import RxSwift

extension UITextView {
    func turnOnExitMode() {
        setupDoneToolbar()
    }
    
    private func setupDoneToolbar() {
        let toolbar = UIToolbar()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                        target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Xong", style: .done,
                                         target: self, action: #selector(doneButtonTapped))
        
        toolbar.setItems([flexSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        
        self.inputAccessoryView = toolbar
    }
    
    @objc func doneButtonTapped() {
        self.endEditing(true)
    }
}

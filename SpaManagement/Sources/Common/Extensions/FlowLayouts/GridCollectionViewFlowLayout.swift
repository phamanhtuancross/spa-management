//
//  GridCollectionViewFlowLayout.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 20/09/2021.
//

import UIKit

public class GridCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    private var numberOfColumns: Int = 0
    private var contentCellHeight: CGFloat = 0
    private var cellPadding: CGFloat = 0
    
    private var contentWidth: CGFloat {
        
        guard let collectionView = collectionView else { return .zero }
        
        let insets = collectionView.contentInset
        let collectionViewWidth = collectionView.bounds.width
        
        return collectionViewWidth - (insets.left + insets.right)
    }
    
    private var cellWidth: CGFloat {
        let columns = CGFloat(numberOfColumns)
        return (contentWidth - (columns - 1) * cellPadding) / columns
    }
    
    private var cellHeight: CGFloat = 0
    
    override public func prepare() {
        super.prepare()
        itemSize = CGSize(width: cellWidth, height: cellHeight)
        minimumLineSpacing = cellPadding
    }
    
    override public func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        guard let context = super.invalidationContext(forBoundsChange: newBounds) as? UICollectionViewFlowLayoutInvalidationContext else {
            return UICollectionViewLayoutInvalidationContext()
        }
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }
    
    @discardableResult
    public func setCellPadding(_ padding: CGFloat) -> GridCollectionViewFlowLayout {
        self.cellPadding = padding
        return self
    }
    
    @discardableResult
    public func setCellHeight(_ value: CGFloat) -> GridCollectionViewFlowLayout {
        self.cellHeight = value
        return self
    }
    
    @discardableResult
    public func setNumberOfColumns(_ value: Int) -> GridCollectionViewFlowLayout {
        self.numberOfColumns = value
        return self
    }
    
    @discardableResult
    public func setScrollDirection(_ direction: UICollectionView.ScrollDirection) -> GridCollectionViewFlowLayout {
        scrollDirection = direction
        return self
    }
}

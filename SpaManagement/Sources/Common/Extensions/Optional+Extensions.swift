//
//  Optional+Extensions.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/9/21.
//

import Foundation

// MARK: - Extensions -
public extension Optional {

    func `or`(_ value: Wrapped?) -> Optional {
        return self ?? value
    }

    func `or`(_ value: Wrapped) -> Wrapped {
        return self ?? value
    }
}

extension Optional where Wrapped == String {
    var orEmpty: String {
        return self.or("")
    }
}

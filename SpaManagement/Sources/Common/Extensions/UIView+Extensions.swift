//
//  UIView+Extensions.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/14/21.
//

import UIKit
import SwiftUI

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(
            roundedRect: bounds,
            byRoundingCorners: corners,
            cornerRadii: CGSize(width: radius, height: radius)
        )
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

extension UIViewController {
    func topMostViewController() -> UIViewController {
        if self.presentedViewController == nil {
            return self
        }
        if let navigation = self.presentedViewController as? UINavigationController {
            return navigation.visibleViewController!.topMostViewController()
        }
        if let tab = self.presentedViewController as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController!.topMostViewController()
    }
}


extension UIView {
    func makeAppBorderStyle(borderWidth: CGFloat = 0.5,
                            borderColor: UIColor = Colors.blue200,
                            conerRadius: CGFloat = 8) {
       
        makeCornerRadidus(radius: conerRadius)
        makeBorder(borderWidth: borderWidth, borderColor: borderColor)
    }
    
    func makeBorder(borderWidth: CGFloat = 1, borderColor: UIColor = Colors.blue200) {
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
    
    func makeCornerRadidus(radius: CGFloat) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
}

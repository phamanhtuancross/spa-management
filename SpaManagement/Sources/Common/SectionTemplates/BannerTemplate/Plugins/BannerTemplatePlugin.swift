//
//  BannerTemplatePlugin.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/18/21.
//

import Foundation

class BannerTemplatePlugin: HomeSectionPlugin {
    
    var listener: HomeSectionPluginListener?
    let id: String
    let extraData: [String : Any]
    
    var bannerService: BannerServiceType
    required init(id: String, extraData: [String : Any], service: AnyObject?) {
        
        self.id = id
        self.extraData = extraData
        
        guard let service = service as? BannerServiceType else {
            fatalError("Need inject banner service to banner template")
        }
        
        self.bannerService = service
    }
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return BannerTemplateWireframe(bannerService: bannerService, listener: self)
    }
}

extension BannerTemplatePlugin: BannerTemplateListener {
    func bannerTemplateSelected(categoryId: String) {
        listener?.pluginSelectedData(self, data: categoryId)
    }
}

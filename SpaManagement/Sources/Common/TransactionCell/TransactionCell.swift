//
//  TransactionCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//

import UIKit
import Reusable

struct TransactionViewModel {
    let model: Transaction
    
    var total: String {
        return Double(model.total).toVNDFormat()
    }
    
    var createDate: String {
        return model.createdAt.timeStamptoDate(DateFormat.dayMothYear)
    }
    
    var icon: UIImage {
        switch model.paymentMethod {
        case .cash:
            return Asset.Icon.Payment.icPaymentMoney.image
            
        case .momo:
            return Asset.Icon.Payment.icPaymentMomo.image
            
        case .transfer:
            return Asset.Icon.Payment.icPaymentTransfer.image
            
        case .free:
            return Asset.Icon.Payment.icPaymentFree.image
        }
    }
    
    var title: String {
        
        let prefix: String
        switch model.paymentMethod {
        case .cash:
            prefix = "Thanh toán tiền mặt"
            
        case .momo:
            prefix = "Thanh toán bằng momo"
            
        case .transfer:
            prefix = "Thanh toán chuyển khoản"
            
        case .free:
            prefix = "Giao dịch miễn phí"
        }
        
        let content = model.invoices.map { $0.title }.joined(separator: " ")
        
        return "\(prefix) Mua \(content)"
    }
    
    var statusTitle: String {
        switch model.paymentStatus {
        case .received:
            return "Đã nhận"
        case .pending:
            return "Chưa thanh toán"
        }
    }
    
    var statusColor: UIColor {
        switch model.paymentStatus {
        case .received:
            return Colors.green500
        case .pending:
            return Colors.red400
        }
    }
}

class TransactionCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var transactionIconImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var paymentDateLabel: UILabel!
    @IBOutlet private weak var paymentValueLabel: UILabel!
    @IBOutlet private weak var paymentStatusLabel: UILabel!
    @IBOutlet private weak var dividerLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        
        titleLabel.setStyle(DS.T16R(color: Colors.ink500))
        paymentValueLabel.setStyle(DS.T16R(color: Colors.ink500))
        
        paymentDateLabel.setStyle(DS.T14R(color: Colors.ink400s))
        paymentStatusLabel.setStyle(DS.T14R(color: Colors.green400s))
        dividerLineView.backgroundColor = Colors.blue200
    }
}

extension TransactionCell {
    func configureCell(viewModel: TransactionViewModel) {
//        titleLabel.text = viewModel.title
//        paymentValueLabel.text = viewModel.total
//        paymentDateLabel.text = viewModel.createDate
//        transactionIconImageView.image = viewModel.icon
        
//        paymentStatusLabel.text = viewModel.statusTitle
//        paymentStatusLabel.textColor = viewModel.statusColor
    }
}

enum DateFormat {
    static let dayMothYear = "dd-MM-yyyy, HH:mm"
}

extension Int {
    func timeStamptoDate(_ tempate: String) -> String {
        let timeStamp = Double(self)
        
        guard let timeInterval = TimeInterval.init(exactly: timeStamp) else { return ""}
        
        let date = Date(timeIntervalSince1970: timeInterval)
        let dayTimePeriodFormater = DateFormatter()
        dayTimePeriodFormater.dateFormat = tempate
        return dayTimePeriodFormater.string(from: date)
    }
}

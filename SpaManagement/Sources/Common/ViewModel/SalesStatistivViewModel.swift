//
//  SalesStatistivViewModel.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 03/10/2021.
//

import Foundation
import Charts
import SwiftDate

struct SalesStatistivViewModel {
    let model: SalesStatistic
    var filteredType: FilteredDateType = .thisWeek
}

extension SalesStatistivViewModel {
    
    private var allSoldOutProducts: [ProductViewModel] {
        let invoices = model.transactions.map { $0.invoices }.flatMap { $0 }
        let group = Dictionary(grouping: invoices) { invoice in
            return invoice.productCode
        }
        
        let soldOutProducts = group
            .map { $0.value }
            .compactMap { invoices -> ProductViewModel? in
                guard let invoice = invoices.first else { return nil }
                
                let product: Product = .init(owner: "",
                                             name: invoice.title,
                                             code: invoice.productCode,
                                             price: invoice.pricePerItem,
                                             description: invoice.description,
                                             imageUrl: nil)
                
                let total = invoices.map { $0.quantity }.reduce(0, { $0 + $1 })
                
                return .init(model: product, total: total, isDiplayTotalSolDout: true)
            }
        
        return soldOutProducts
    }
    
    var topProducts: [ProductViewModel] {
        
       
        return allSoldOutProducts
            .sorted { lhs, rhs in
                return lhs.total >= rhs.total
            }
            .suffix(10)
        
    }
}

extension SalesStatistivViewModel {
    var totalSalesTitle: String {
        switch filteredType {
        case .thisWeek:
            return "Tổng thu nhập tuần này"
        case .yesterday:
            return "Tổng thu nhập hôm qua"
        case .thisYear:
            return "Tổng thu nhập năm nay"
        case .thisMonth:
            return "Tổng thu nhập tháng này"
        case .lasWeek:
            return "Tổng thu nhập tuần trước"
        case .lastMonth:
            return "Tổng thu nhập tháng trước"
        case .today:
            return "Tổng thu nhập hôm qua"
        case .chooseARange:
            return "Tổng thu nhập"
        }
    }
    
    var totalSalesValue: Double {
        return model.transactions.map { $0.total }.reduce(0, { $0 + $1 }).toDouble
    }
    
    var totalNewMemberTitle: String {
        switch filteredType {
        case .thisWeek:
            return "Thành viên mới tuần này"
        case .yesterday:
            return "Thành viên mới hôm qua"
        case .thisYear:
            return "Thành viên mới năm nay"
        case .thisMonth:
            return "Thành viên mới tháng này"
        case .lasWeek:
            return "Thành viên mới tuần trước"
        case .lastMonth:
            return "Thành viên mới tháng trước"
        case .today:
            return "Thành viên mới hôm qua"
        case .chooseARange:
            return "Thành viên mới"
        }
    }
    
    var totalNewMember: Int {
        return model.customers.count
    }
}

extension SalesStatistivViewModel {
    var barChartData: BarChartData? {
        switch filteredType {
        
        case .today, .yesterday:
            return barChardDataInDay
            
        case .thisWeek, .lasWeek:
            return barChartDataSevenDays
            
        case .lastMonth, .thisMonth:
            return barChardDataInThirtyDays
            
        case .thisYear:
            return barCharDataInYear
            
        case .chooseARange:
            return nil
        }
    }
    
    private var barChardDataInDay: BarChartData? {
        
        let yVals = model.transactions.map { $0.total.toDouble }.enumerated().map { index, value -> BarChartDataEntry in
            return BarChartDataEntry(x: Double(index), y: value)
        }
        
        let set = BarChartDataSet(entries: yVals, label: "")
        set.valueFormatter = MoneyAxisValueFormatter()
        set.colors = [
            #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),#colorLiteral(red: 1, green: 0.1857388616, blue: 0.5733950138, alpha: 0.5476740149),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        ].map { $0.withAlphaComponent(0.5) }
        
        let data = BarChartData(dataSet: set)
        let groupSpace = 0.06
        let barSpace = 0.0
        let barWidth = 0.5
        data.barWidth = barWidth
        
        data.groupBars(fromX: 0, groupSpace: groupSpace, barSpace: barSpace)
        
        return data
    }
    
    private var barCharDataInYear: BarChartData? {
        let group = Dictionary(grouping: model.transactions) {
            return Date(timeIntervalSince1970: TimeInterval($0.createdAt)).month
        }
        .sorted { lhs, rhs in
            return lhs.key <= rhs.key
        }
    
        var formatedValues: [Double] = Array(repeating: .zero, count: 12)
        
        group.map { $0.value }.forEach { transactions in
            
            if let currentDateInterval = transactions.first?.createdAt.toDouble,
               let index = (Date(timeIntervalSince1970: currentDateInterval) - filteredType.startDate).month,
               index >= .zero {
                formatedValues[index] = transactions.map { $0.total }.reduce(0, { $0 + $1 }).toDouble
            }
        }
        
        let yVals = formatedValues.enumerated().map { index, value -> BarChartDataEntry in
            return BarChartDataEntry(x: Double(index), y: value)
        }
        
        let set = BarChartDataSet(entries: yVals, label: "")
        set.valueFormatter = MoneyAxisValueFormatter()
        set.colors = [
            #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),#colorLiteral(red: 1, green: 0.1857388616, blue: 0.5733950138, alpha: 0.5476740149),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        ].map { $0.withAlphaComponent(0.5) }
        
        let data = BarChartData(dataSet: set)
        let barWidth = 0.5
        data.barWidth = barWidth
        
        return data
    }
    
    private var barChardDataInThirtyDays: BarChartData? {
        let group = Dictionary(grouping: model.transactions) { transaction -> Int in
            let week = Date(timeIntervalSince1970: TimeInterval(transaction.createdAt)).weekOfMonth
            return week
        }
    
        var formatedValues: [Double] = Array(repeating: .zero, count: 5)
        
        group.map { $0.value }.forEach { transactions in
            
            if let currentDateInterval = transactions.first?.createdAt.toDouble {
                let index = Date(timeIntervalSince1970: currentDateInterval).weekOfMonth
                formatedValues[index - 1] = transactions.map { $0.total }.reduce(0, { $0 + $1 }).toDouble
            }
        }
        
        let yVals = formatedValues.enumerated().map { index, value -> BarChartDataEntry in
            return BarChartDataEntry(x: Double(index), y: value)
        }
        
        let set = BarChartDataSet(entries: yVals, label: "")
        set.valueFormatter = MoneyAxisValueFormatter()
        set.colors = [
            #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),#colorLiteral(red: 1, green: 0.1857388616, blue: 0.5733950138, alpha: 0.5476740149),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        ].map { $0.withAlphaComponent(0.5) }
        
        let data = BarChartData(dataSet: set)
        let barWidth = 0.4
        data.barWidth = barWidth
        
        return data
    }
    
    private var barChartDataSevenDays: BarChartData? {
        
        let group = Dictionary(grouping: model.transactions) {
            return Date(timeIntervalSince1970: TimeInterval($0.createdAt)).toFormat("dd MM yyyy")
        }
        .sorted { lhs, rhs in
            guard let lhsDate = lhs.key.toDate(),
                  let rhsDate = rhs.key.toDate() else { return true }
            return lhsDate.isBeforeDate(rhsDate, granularity: .month)
        }
    
        var formatedValues: [Double] = Array(repeating: .zero, count: 7)
        
        group.map { $0.value }.forEach { transactions in
        
            if let currentDateInterval = transactions.first?.createdAt.toDouble,
               let index = (Date(timeIntervalSince1970: currentDateInterval) - filteredType.startDate).day {
                
                formatedValues[index] = transactions.map { $0.total }.reduce(0, { $0 + $1 }).toDouble
            }
        }
 
        let yVals = formatedValues.enumerated().map { index, value -> BarChartDataEntry in
            return BarChartDataEntry(x: Double(index), y: value)
        }
        
        let set = BarChartDataSet(entries: yVals, label: "")
        set.valueFormatter = MoneyAxisValueFormatter()
        set.colors = [
            #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),#colorLiteral(red: 1, green: 0.1857388616, blue: 0.5733950138, alpha: 0.5476740149),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        ].map { $0.withAlphaComponent(0.5) }
        
        let data = BarChartData(dataSet: set)
    
        let barWidth = 0.5
        data.barWidth = barWidth
        
        return data
    }
}

extension SalesStatistivViewModel {
    var lineChardData: LineChartData? {
        switch filteredType {
        
        case .today, .yesterday:
            return lineChartDataInDay
            
        case .thisWeek, .lasWeek:
            return linechartDataSevenDays
            
        case .lastMonth, .thisMonth:
            return lineChartDataInThirtyDays
            
        case .thisYear:
            return lineChartDataInYear
            
        case .chooseARange:
            return nil
        }
    }
    
    private var lineChartDataInDay: LineChartData? {
        
        let entries = model.transactions.map { $0.total.toDouble }.enumerated().map { index, value -> ChartDataEntry in
            return ChartDataEntry(x: Double(index), y: value)
        }

        return makeLineChartData(from: entries)
    }
    
    private var lineChartDataInYear: LineChartData? {
        let group = Dictionary(grouping: model.transactions) {
            return Date(timeIntervalSince1970: TimeInterval($0.createdAt)).month
        }
        .sorted { lhs, rhs in
            return lhs.key <= rhs.key
        }
    
        var formatedValues: [Double] = Array(repeating: .zero, count: 12)
        
        group.map { $0.value }.forEach { transactions in
            
            if let currentDateInterval = transactions.first?.createdAt.toDouble,
               let index = (Date(timeIntervalSince1970: currentDateInterval) - filteredType.startDate).month,
               index >= .zero {
                formatedValues[index] = transactions.map { $0.total }.reduce(0, { $0 + $1 }).toDouble
            }
        }
        
        let entries = formatedValues.enumerated().map { index, value -> ChartDataEntry in
            return ChartDataEntry(x: Double(index), y: value)
        }
        
        return makeLineChartData(from: entries)
    }
    
    private func makeLineChartData(from entries: [ChartDataEntry]) -> LineChartData {
        let set = LineChartDataSet(entries: entries, label: "Line DataSet")
        set.setColor(Colors.yellow)
        set.lineWidth = 2
        set.setCircleColor(Colors.red400)
        set.circleRadius = 1.5
        set.circleHoleRadius = .zero
        set.fillColor = Colors.green500
        set.fillAlpha = 1
        set.mode = .cubicBezier
        set.drawValuesEnabled = true
        set.drawFilledEnabled = true
        set.valueFormatter = MoneyAxisValueFormatter()
        set.fill = Fill.fillWithLinearGradient(.init(colorsSpace: .none,
                                                     colors: [Colors.green400.cgColor,  Colors.orange200s.cgColor] as CFArray,
                                                     locations: [1.0, 0.0])!, angle: 90)
        set.valueFont = .systemFont(ofSize: 8)
        set.valueTextColor = Colors.ink400
        
        return LineChartData(dataSets: [set])
    }
    
    private var lineChartDataInThirtyDays: LineChartData? {
        let group = Dictionary(grouping: model.transactions) { transaction -> Int in
            let week = Date(timeIntervalSince1970: TimeInterval(transaction.createdAt)).weekOfMonth
            return week
        }
    
        var formatedValues: [Double] = Array(repeating: .zero, count: 5)
        
        group.map { $0.value }.forEach { transactions in
            
            if let currentDateInterval = transactions.first?.createdAt.toDouble {
                let index = Date(timeIntervalSince1970: currentDateInterval).weekOfMonth
                formatedValues[index - 1] = transactions.map { $0.total }.reduce(0, { $0 + $1 }).toDouble
            }
        }
        
        let entries = formatedValues.enumerated().map { index, value -> ChartDataEntry in
            return ChartDataEntry(x: Double(index), y: value)
        }
        
        return makeLineChartData(from: entries)
    }
    
    private var linechartDataSevenDays: LineChartData? {
        
        let group = Dictionary(grouping: model.transactions) {
            return Date(timeIntervalSince1970: TimeInterval($0.createdAt)).toFormat("dd MM yyyy")
        }
        .sorted { lhs, rhs in
            guard let lhsDate = lhs.key.toDate(),
                  let rhsDate = rhs.key.toDate() else { return true }
            return lhsDate.isBeforeDate(rhsDate, granularity: .month)
        }
    
        var formatedValues: [Double] = Array(repeating: .zero, count: 7)
        
        group.map { $0.value }.forEach { transactions in
        
            if let currentDateInterval = transactions.first?.createdAt.toDouble,
               let index = (Date(timeIntervalSince1970: currentDateInterval) - filteredType.startDate).day {
                
                formatedValues[index] = transactions.map { $0.total }.reduce(0, { $0 + $1 }).toDouble
            }
        }
 
        let entries = formatedValues.enumerated().map { index, value -> ChartDataEntry in
            return ChartDataEntry(x: Double(index), y: value)
        }
        
        return makeLineChartData(from: entries)
    }
}

extension SalesStatistivViewModel {
    var pieChartData: PieChartData? {
        let entries = allSoldOutProducts
            .enumerated()
            .map { index, viewModel -> PieChartDataEntry in
                
                let value = (viewModel.total * viewModel.model.price).toDouble
                let label = viewModel.model.name
                return .init(value: value, label: label)
            }
        
        let set = PieChartDataSet(entries: entries, label: "tỉ lệ sản phẩm bán ra.")
        set.drawIconsEnabled = false
        set.sliceSpace = 0.8
        
        set.colors = ChartColorTemplates.vordiplom()
        + ChartColorTemplates.joyful()
        + ChartColorTemplates.colorful()
        + ChartColorTemplates.liberty()
        + ChartColorTemplates.pastel()
        + [UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1)]
        
        let data = PieChartData(dataSet: set)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = "đ"
        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        
        data.setValueFont(.systemFont(ofSize: 8, weight: .light))
        data.setValueTextColor(Colors.ink400)
        
        return data
    }
}

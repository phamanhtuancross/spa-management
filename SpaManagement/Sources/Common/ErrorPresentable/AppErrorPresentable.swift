//
//  ErrorPresentable.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 02/10/2021.
//

import UIKit

protocol AppErrorPresentable: AnyObject {
    func handleError(error: AppError, completion: VoidCallBack?)
}

extension AppErrorPresentable where Self: UIViewController {
    func handleError(error: AppError, completion: VoidCallBack?) {
        switch error {
        case .authenticateError:
            showAuthenticatedErrorAlert()
            
        case .invalidLoginInfo:
            showInvalidLoginInfoErrorAlert()

        case .productNotFoundError:
            showProductNotFoundErrorAlert()

        case .internalServerError:
            showInternalServerErrorAlert()

        case .badGateWayError:
            showBadGateWayErrorAlert()

        case .existedError:
            showExistedErrorAlert()

        case .unknow:
            showUnknownErrorAlert()
        }
    }
    
    private func showAuthenticatedErrorAlert() {
        let alertController = UIAlertController(title: "Thông báo", message: "Authenticated error, vui lòng thử lại sau!", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .destructive)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
    
    private func showInvalidLoginInfoErrorAlert() {
        let alertController = UIAlertController(title: "Thông báo", message: "Thông tin đăng nhập không hợp lệ, vui lòng thử lại!", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .destructive)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
    
    private func showProductNotFoundErrorAlert() {
        
    }
    
    private func showInternalServerErrorAlert() {
        let alertController = UIAlertController(title: "Thông báo", message: "Hệ thống đang lỗi, vui lòng thử lại!", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .destructive)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
    
    private func showBadGateWayErrorAlert() {
        let alertController = UIAlertController(title: "Thông báo", message: "Không tìm thấy sản phẩm trong database!", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .destructive)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
    
    private func showExistedErrorAlert() {
        let alertController = UIAlertController(title: "Thông báo", message: "Sản phẩm đã tồn tại, vui lòng thêm sản phẩm khác!", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .destructive)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
    
    private func showUnknownErrorAlert() {
        let alertController = UIAlertController(title: "Thông báo", message: "Lỗi không xác đinh, vui lòng thử lại sau!", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .destructive)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
}


//
//  DSTextStyles.swift
//  DesignSystem
//
//  Created by TAMPT12 on 3/18/20.
//

import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command type_name

public protocol DesignSystemTextStyle {
    var fontSize: CGFloat { get }
    var weight: UIFont.Weight { get }
    var lineHeight: CGFloat { get }
}

extension DesignSystemTextStyle {
    public var font: UIFont { UIFont.systemFont(ofSize: fontSize, weight: weight) }
}

public protocol DSTextStyle: DesignSystemTextStyle {
    var fontSize: CGFloat { get }
    var weight: UIFont.Weight { get }
    var color: UIColor { get }
    var lineHeight: CGFloat { get }
}

public enum DS {

    public struct T48R: DSTextStyle {
        public let fontSize: CGFloat = 48.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 56
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T48M: DSTextStyle {
        public let fontSize: CGFloat = 48.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 56
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T30R: DSTextStyle {
        public let fontSize: CGFloat = 30.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 38
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T30M: DSTextStyle {
        public let fontSize: CGFloat = 30.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 38
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T24R: DSTextStyle {
        public let fontSize: CGFloat = 24.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 32
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T24M: DSTextStyle {
        public let fontSize: CGFloat = 24.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 32
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T20R: DSTextStyle {
        public let fontSize: CGFloat = 20.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 28
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T20M: DSTextStyle {
        public let fontSize: CGFloat = 20.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 28
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }
    
    public struct T20SM: DSTextStyle {
        public let fontSize: CGFloat = 20.0
        public let weight: UIFont.Weight = .semibold
        public let color: UIColor
        public let lineHeight: CGFloat = 28
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T16L: DSTextStyle {
        public let fontSize: CGFloat = 16.0
        public let weight: UIFont.Weight = .light
        public let color: UIColor
        public let lineHeight: CGFloat = 20
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }
    
    public struct T16R: DSTextStyle {
        public let fontSize: CGFloat = 16.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 24
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T16M: DSTextStyle {
        public let fontSize: CGFloat = 16.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 24
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T16R20: DSTextStyle {
        public let fontSize: CGFloat = 16.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 20
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T16M20: DSTextStyle {
        public let fontSize: CGFloat = 16.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 20
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T14R: DSTextStyle {
        public let fontSize: CGFloat = 14.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 20
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T14M: DSTextStyle {
        public let fontSize: CGFloat = 14.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 20
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }
    
    public struct T12L: DSTextStyle {
        public let fontSize: CGFloat = 12.0
        public let weight: UIFont.Weight = .light
        public let color: UIColor
        public let lineHeight: CGFloat = 16
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T12R: DSTextStyle {
        public let fontSize: CGFloat = 12.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 16
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T12M: DSTextStyle {
        public let fontSize: CGFloat = 12.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 16
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T10R: DSTextStyle {
        public let fontSize: CGFloat = 10.0
        public let weight: UIFont.Weight = .regular
        public let color: UIColor
        public let lineHeight: CGFloat = 12
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }

    public struct T10M: DSTextStyle {
        public let fontSize: CGFloat = 10.0
        public let weight: UIFont.Weight = .medium
        public let color: UIColor
        public let lineHeight: CGFloat = 12
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }
    
    public struct T24SM: DSTextStyle {
        public let fontSize: CGFloat = 24.0
        public let weight: UIFont.Weight = .semibold
        public let color: UIColor
        public let lineHeight: CGFloat = 28
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }
    
    public struct T32SM: DSTextStyle {
        public let fontSize: CGFloat = 32.0
        public let weight: UIFont.Weight = .semibold
        public let color: UIColor
        public let lineHeight: CGFloat = 40
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }
    
    public struct T28SM: DSTextStyle {
        public let fontSize: CGFloat = 28.0
        public let weight: UIFont.Weight = .semibold
        public let color: UIColor
        public let lineHeight: CGFloat = 32
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }
    
    public struct AppTitle32SemiBold: DSTextStyle {
        public let fontSize: CGFloat = 22.0
        public let weight: UIFont.Weight = .semibold
        public let color: UIColor
        public let lineHeight: CGFloat = 28
        
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
        
    }
    
    public struct AppDescription16Light: DSTextStyle {
        public let fontSize: CGFloat = 16.0
        public let weight: UIFont.Weight = .light
        public let color: UIColor
        public let lineHeight: CGFloat = 20
        
        public init(color: UIColor = Colors.ink500) {
            self.color = color
        }
    }
}

// swiftlint:enable type_name

extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
}

//
//  DSSecurityTextField.swift
//  SpaManagement
//
//  Created by Tuan Pham on 16/10/2021.
//

import UIKit
import RxCocoa
import RxSwift

class DSSecurityTextField: UITextField {
    
    private let disposeBag = DisposeBag()
    
    private var passwordImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        let holderView = UIView()
        holderView.addSubview(passwordImageView)
        
        passwordImageView.snp.makeConstraints { $0.edges.equalToSuperview().inset(16) }
        
        self.addSubview(holderView)
        holderView.snp.makeConstraints {
            $0.top.bottom.trailing.equalToSuperview()
            $0.width.equalTo(self.bounds.height)
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapSecurityImage))
        holderView.addGestureRecognizer(tapGesture)
        
        isSecureTextEntry = true
        updateSecurityImageView()
        
        rx.text.orEmpty
            .map { $0.isEmpty }
            .asDriver(onErrorDriveWith: .never())
            .driveNext { [weak self] isHidden in
                guard let self = self else { return }
                self.passwordImageView.isHidden = isHidden
            }
            .disposed(by: disposeBag)
    }
    
    @objc
    private func handleTapSecurityImage() {
        isSecureTextEntry.toggle()
        updateSecurityImageView()
    }
    
    private func updateSecurityImageView() {
        if isSecureTextEntry {
            passwordImageView.image = Asset.Icon.Common.icShowPassword.image.color(Colors.ink400s)
            return
        }

        passwordImageView.image = Asset.Icon.Common.icHideShowPassword.image.color(Colors.ink400s)
        return
    }
}

//
//  DSCounterButton.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/8/21.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

class DSCounterButton: UIView, NibOwnerLoadable {
    
    @IBOutlet weak private var increasingButtonWrapView: UIView!
    @IBOutlet weak private var increasingButton: UIButton!
    @IBOutlet weak private var increasingLabel: UILabel!
    
    
    @IBOutlet weak private var valueLabelWrapView: UIView!
    @IBOutlet weak var valueLabel: UILabel!
    
    @IBOutlet weak private var descreasingButtonWrapView: UIView!
    @IBOutlet weak private var descreasingButton: UIButton!
    @IBOutlet weak private var descreasingLabel: UILabel!
    
    let valueDidChanged = BehaviorRelay<Int>(value: .zero)
    private let disposeBag = DisposeBag()
    
    var currentValue: Int = 0 {
        didSet {
            if currentValue >= 0 {
                valueLabel.text = "\(currentValue)"
                valueDidChanged.accept(currentValue)
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    private func setupViews() {
        self.loadNibContent()
        
        currentValue = 0
        
        increasingLabel.setStyle(DS.T20M(color: Colors.ink400s))
        valueLabel.setStyle(DS.T16M(color: Colors.ink400s))
        descreasingLabel.setStyle(DS.T20M(color: Colors.ink400s))
        
        [
            increasingButtonWrapView,
            valueLabelWrapView,
            descreasingButtonWrapView
        ]
        .forEach { view in
            view?.makeBorder()
            view?.layer.cornerRadius = 4
            view?.layer.masksToBounds = true
        }
        
        increasingButton
            .rx
            .tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.currentValue += 1
            }
            .disposed(by: disposeBag)
        
        descreasingButton
            .rx
            .tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.currentValue -= 1
            }
            .disposed(by: disposeBag)
    }
}

extension Reactive where Base: DSCounterButton {
    var valueDidChanged: BehaviorRelay<Int> {
        return base.valueDidChanged
    }
}

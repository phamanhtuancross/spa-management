//
//  DSCurrencyTextField.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 16/09/2021.
//

import UIKit
import RxCocoa
import RxSwift

class DSCurrencyTextField: UITextField {
    //1
    var passTextFieldText: ((String, Double?) -> Void)?

    let valueDidChanged = PublishRelay<Double>()

    public var amountAsDouble: Double? {
        didSet {
            guard let amount = amountAsDouble else { return }
            valueDidChanged.accept(amount)
        }
    }
    
    var startingValue: Double? {
        didSet {
            let nsNumber = NSNumber(value: startingValue.or(.zero))
            self.text = numberFormatter.string(from: nsNumber)
        }
    }
    
    //3
    private lazy var numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = "đ"
        formatter.currencyGroupingSeparator = ","
        formatter.locale = Locale(identifier: "vi")
        formatter.groupingSize = 3
        formatter.maximumFractionDigits = 0
        return formatter
    }()
    
    //4
    private var roundingPlaces: Int {
        return numberFormatter.maximumFractionDigits
    }
    
    //5
    private var isSymbolOnRight = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    //6
    private func setup() {
        self.textAlignment = .right
        self.keyboardType = .numberPad
        self.contentScaleFactor = 1.5
        delegate = self

        self.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        textFieldDidChange()
    }

    @objc private func textFieldDidChange() {
        updateTextField()
    }

    private func updateTextField() {
        var cleanedAmount = ""
        
        for character in self.text.orEmpty {
            if character.isNumber {
                cleanedAmount.append(character)
            }
        }
        
        if isSymbolOnRight {
            cleanedAmount = String(cleanedAmount.dropLast())
        }
        
        let amount = Double(cleanedAmount).or(.zero)
        amountAsDouble = (amount / 100.0)
        let amountAsString = numberFormatter.string(from: NSNumber(value: amountAsDouble.or(.zero))).or("0 đ")
        self.text = amountAsString
        
        //Format the number based on number of decimal digits
        if self.roundingPlaces > 0 {
            
            let amount = Double(cleanedAmount).or(.zero)
            amountAsDouble = (amount / 100.0)
            let amountAsString = numberFormatter.string(from: NSNumber(value: amountAsDouble.or(.zero))).or("0 đ")
            
            self.text = amountAsString
        } else {
         
            let amountAsNumber = Double(cleanedAmount).or(.zero)
            amountAsDouble = amountAsNumber
            let amountAsString = numberFormatter.string(from: NSNumber(value: amountAsDouble.or(.zero))).or("0 đ")
            self.text = amountAsString
        }
        
        passTextFieldText?(self.text!, amountAsDouble)
    }
    
    override func closestPosition(to point: CGPoint) -> UITextPosition? {
        let beginning = self.beginningOfDocument
        let end = self.position(from: beginning, offset: self.text?.count ?? 0)
        return end
    }
}

extension Double {
    func toVNDFormat() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyGroupingSeparator = ","
        formatter.currencySymbol = "đ"
        formatter.locale = Locale(identifier: "vi")
        formatter.groupingSize = 3
        formatter.maximumFractionDigits = 0
        
        let amountInString = formatter.string(from: NSNumber(value: self)).or("0 đ")
        return "\(amountInString)"
    }
}
extension DSCurrencyTextField: UITextFieldDelegate {
    
    //9
    //BEFORE entered string is registered in the textField
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let lastCharacterInTextField = (textField.text ?? "").last
        
        //Note - not the most straight forward implementation but this subclass supports both right and left currencies
        if string == "" && lastCharacterInTextField!.isNumber == false {
            //For hitting backspace and currency is on the right side
            isSymbolOnRight = true
        } else {
            isSymbolOnRight = false
        }
        
        let currentText = textField.text.orEmpty
        
        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        // make sure the result is under 16 characters
        return updatedText.count <= 18
    }
}

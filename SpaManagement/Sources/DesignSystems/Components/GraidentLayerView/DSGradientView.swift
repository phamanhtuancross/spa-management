//
//  DSGradientView.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/16/21.
//

import UIKit
import Reusable

extension CAGradientLayer {
    
    enum Point {
        case topLeft
        case centerLeft
        case bottomLeft
        case topCenter
        case center
        case bottomCenter
        case topRight
        case centerRight
        case bottomRight
        
        var point: CGPoint {
            switch self {
            case .topLeft:
                return CGPoint(x: 0, y: 0)
            case .centerLeft:
                return CGPoint(x: 0, y: 0.5)
            case .bottomLeft:
                return CGPoint(x: 0, y: 1.0)
            case .topCenter:
                return CGPoint(x: 0.5, y: 0)
            case .center:
                return CGPoint(x: 0.5, y: 0.5)
            case .bottomCenter:
                return CGPoint(x: 0.5, y: 1.0)
            case .topRight:
                return CGPoint(x: 1.0, y: 0.0)
            case .centerRight:
                return CGPoint(x: 1.0, y: 0.5)
            case .bottomRight:
                return CGPoint(x: 1.0, y: 1.0)
            }
        }
    }
    
    convenience init(start: Point, end: Point, colors: [CGColor], type: CAGradientLayerType = .axial) {
        self.init()
        self.startPoint = start.point
        self.endPoint = end.point
        self.colors = colors
        var ratio: Double = 0
        if colors.count > 1 {
            ratio = 1.0 / Double(colors.count - 1)
        }
        self.locations = colors.indices.map { NSNumber(value: Double($0) * ratio) }
        self.type = type
    }
}


public struct DSGradientConfig {
    
    public enum Direction: Int {
        case topToBottom = 0
        case leftToRight = 1
        case topLeftToBottomRight = 2
        case topRightToBottomLeft = 3
        
        var points: (start: CAGradientLayer.Point, end: CAGradientLayer.Point) {
            switch self {
            
            case .topToBottom:
                return (start: .topCenter, end: .bottomCenter)
                
            case .leftToRight:
                return (start: .centerLeft, end: .centerRight)
                
            case .topLeftToBottomRight:
                return (start: .topLeft, end: .bottomRight)
                
            case .topRightToBottomLeft:
                return (start: .topRight, end: .bottomLeft)
            }
        }
        
        var startPoint: CGPoint {
            return points.start.point
        }
        
        var endPoint: CGPoint {
            return points.end.point
        }
    }
    
    let colors: [UIColor]
    let direction: Direction
    
    public init(colors: [UIColor], direction: Direction) {
        self.colors = colors
        self.direction = direction
    }
}
class DSGradientView: UIView, NibOwnerLoadable {
    
    @IBOutlet private weak var gradientView: UIView!
    private var gradientLayer: CAGradientLayer?
    
    public var gradientConfig: DSGradientConfig = DSGradientConfig(colors: [Colors.white500.withAlphaComponent(0), Colors.ink400],
                                                                   direction: .topToBottom) {
        didSet {
            removeGradientLayer()
            addGradientLayer()
        }
    }
    
    private func removeGradientLayer() {
        gradientView
            .layer
            .sublayers?
            .filter { $0.name == headerBackgroundGradientLayerName }
            .forEach { $0.removeFromSuperlayer() }
        gradientLayer = nil
    }
    
    private func addGradientLayer() {
        let gradient = CAGradientLayer(start: gradientConfig.direction.points.start,
                                       end: gradientConfig.direction.points.end,
                                       colors: gradientConfig.colors.map { $0.cgColor })
        gradient.name = headerBackgroundGradientLayerName
        gradientView.layer.addSublayer(gradient)
        gradientLayer = gradient
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    private func setupViews() {
        self.loadNibContent()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientSize()
    }
    
    private func updateGradientSize() {
        gradientLayer?.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
    }
}
private let headerBackgroundGradientLayerName = "header_background_gradient_layer"

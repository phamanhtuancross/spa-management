//
//  AppCalendar.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 07/10/2021.
//

import Foundation

struct Event: Codable {
    let id: String
    let description: String
    let happenAt: Int
}

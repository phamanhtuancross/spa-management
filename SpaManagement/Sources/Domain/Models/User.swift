//
//  User.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 21/09/2021.
//

import Foundation

struct User: Decodable {
    let username: String
    let phoneNumber: String
}

//
//  Transaction.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 19/09/2021.
//

import Foundation

struct Invoice: Decodable, Encodable {
    let title: String
    let quantity: Int
    let pricePerItem: Int
    let description: String
    let productCode: String
}
struct Transaction: Decodable, Encodable {
    let id: String
    let total: Int
    let createdAt: Int
    let method: String
    let status: String
    let customer: String
    let invoices: [Invoice]
}

extension Transaction {
    var paymentMethod: PaymentMethodType {
        return .init(rawValue: method).or(.cash)
    }
    
    var paymentStatus: PaymentStatus {
        return .init(rawValue: status).or(.received)
    }
}

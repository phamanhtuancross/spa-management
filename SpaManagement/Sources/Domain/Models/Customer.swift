//
//  Customer.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//

import Foundation

struct Customer: Decodable {
    
    let id: String
    let name: String
    let lastJoinedDate: Int
    let totalSpend: Int
    let phoneNumber: String
    let email: String?
    let rank: String
    let createdAt: Int
    
    let dateOfBirth: Int
    
    var rankType: RankType {
        return .init(rawValue: rank).or(.bronze)
    }
    
    enum RankType: String {
        case bronze
        case silver
        case gold
    }
    
    private enum CodingKeys: String, CodingKey {
        case id, name, lastJoinedDate, totalSpend, phoneNumber, email, rank, createdAt, dateOfBirth
    }
    
    init(from decoder: Decoder) throws {
        let containter = try decoder.container(keyedBy: CodingKeys.self)
        
        id = (try? containter.decode(String.self, forKey: .id)).orEmpty
        name = (try? containter.decode(String.self, forKey: .name)).orEmpty
        lastJoinedDate = (try? containter.decode(Int.self, forKey: .lastJoinedDate)).or(Int(Date.trueTime.timeIntervalSince1970))
        totalSpend = (try? containter.decode(Int.self, forKey: .totalSpend)).or(.zero)
        phoneNumber = (try? containter.decode(String.self, forKey: .phoneNumber)).orEmpty
        email = (try? containter.decode(String.self, forKey: .email)).orEmpty
        rank = (try? containter.decode(String.self, forKey: .rank)).orEmpty
        createdAt = (try? containter.decode(Int.self, forKey: .createdAt)).or(Int(Date.trueTime.timeIntervalSince1970))
        dateOfBirth = (try? containter.decode(Int.self, forKey: .dateOfBirth)).or(Int(Date.trueTime.timeIntervalSince1970))
    }
}

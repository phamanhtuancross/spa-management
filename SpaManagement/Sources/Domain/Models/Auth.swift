//
//  Auth.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 21/09/2021.
//

import Foundation

struct AuthenticationInfo: Decodable {
    let user: User
    let token: String
}

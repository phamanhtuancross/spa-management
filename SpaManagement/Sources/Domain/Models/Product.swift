//
//  Product.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 29/09/2021.
//

import Foundation

struct Product: Codable {
    let owner: String
    let name: String
    let code: String
    let price: Int
    let description: String
    let imageUrl: String?
}

//
//  SalesStatisticService.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 25/09/2021.
//

import Foundation
import RxSwift

struct SalesStatistic: Decodable {
    let customers: [Customer]
    let transactions: [Transaction]
}

protocol SalesStatisticServiceType {
    func getSalesStatistic(startDate: Int, endDate: Int) -> Observable<SalesStatistic>
}

class SalesStatisticService {
    static var `default` = SalesStatisticService(networkManager: NetworkManager.default)
    private let networkManager: Networkable
    private let networkScheduler: SchedulerType
    
    init(networkManager: Networkable) {
        self.networkManager = networkManager
        self.networkScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
    }
}

extension SalesStatisticService: SalesStatisticServiceType {
    func getSalesStatistic(startDate: Int, endDate: Int) -> Observable<SalesStatistic> {
        let target = SalesStatisticTargets.GetSalesStatistic(startDate: startDate, endDate: endDate)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(SalesStatistic.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
}

import Moya

protocol SalesStatisticTargetType: TargetType, AccessTokenAuthorizable {}

extension SalesStatisticTargetType {
    var baseURL: URL {
        guard let url = URL(string: Natrium.apiBase) else {
            fatalError("Invalid API Base : \(Natrium.apiBase)")
        }
        
        return url
    }
    
    var authorizationType: AuthorizationType? {
        return .bearer
    }
}

enum SalesStatisticTargets {
    struct GetSalesStatistic: SalesStatisticTargetType {
        
        let startDate: Int
        let endDate: Int
        
        var path: String {
            return "/salesStatistic"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: urlParameters.or([:]), encoding: URLEncoding.default)
        }
        
        var urlParameters: [String : Any]? {
            return [
                "startDate": startDate,
                "endDate": endDate
            ]
        }
    }
}
    

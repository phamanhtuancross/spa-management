//
//  IPAHelper.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 09/10/2021.
//

import StoreKit
import Resolver
import Action
import RxSwift
import RxCocoa

public typealias ProductIdentifier = String
public typealias ProductsRequestCompletionHandler = (_ success: Bool, _ products: [SKProduct]?) -> Void

extension Notification.Name {
    static let IAPHelperPurchasedSuccess = Notification.Name("IAPHelperPurchaseNotification")
    static let IAPHelperPurchaseFailed = Notification.Name("IAPHelperPurchaseFailed")
}

open class IAPHelper: NSObject  {
    
    @LazyInjected var iapInternalService: IAPInternalServiceType
    
    private let productIdentifiers: Set<ProductIdentifier>
    private var purchasedProductIdentifiers: Set<ProductIdentifier> = []
    private var productsRequest: SKProductsRequest?
    private var productsRequestCompletionHandler: ProductsRequestCompletionHandler?
    private let disposeBag = DisposeBag()
    
//    public let didPurchasedFailedRelay = PublishRelay<String>()
//    public let didPurchaseSuccessfulRelay = PublishRelay<String>()
    
    private lazy var syncCloudReceiptDatabaseAction = makeSyncCloudReceiptDatabaseAction()
    
    public init(productIds: Set<ProductIdentifier>) {
        productIdentifiers = productIds
        super.init()
        
        SKPaymentQueue.default().add(self)
        verityReceipt()
    }
}

// MARK: - StoreKit API

extension IAPHelper {
    
    public func requestProducts(_ completionHandler: @escaping ProductsRequestCompletionHandler) {
        productsRequest?.cancel()
        productsRequestCompletionHandler = completionHandler
        
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest?.delegate = self
        productsRequest?.start()
    }
    
    public func buyProduct(_ product: SKProduct) {
        print("Buying \(product.productIdentifier)...")
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    public func isProductPurchased(_ productIdentifier: ProductIdentifier) -> Bool {
        return purchasedProductIdentifiers.contains(productIdentifier)
    }
    
    public class func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    public func restorePurchases() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}

// MARK: - SKProductsRequestDelegate

extension IAPHelper: SKProductsRequestDelegate {
    
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("Loaded list of products...")
        let products = response.products
        productsRequestCompletionHandler?(true, products)
        clearRequestAndHandler()
        
        for p in products {
            print("Found product: \(p.productIdentifier) \(p.localizedTitle) \(p.price.floatValue)")
        }
    }
    
    public func requestDidFinish(_ request: SKRequest) {

        clearRequestAndHandler()
        if request is SKReceiptRefreshRequest {
            verityReceipt()
        }
        NotificationCenter.default.post(name: .IAPHelperPurchaseFailed, object: nil, userInfo: nil)
    }

    public func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Failed to load list of products.")
        print("Error: \(error.localizedDescription)")
        productsRequestCompletionHandler?(false, nil)
        clearRequestAndHandler()
        NotificationCenter.default.post(name: .IAPHelperPurchaseFailed, object: nil, userInfo: nil)
    }
    
    private func clearRequestAndHandler() {
        productsRequest?.cancel()
        productsRequest = nil
        productsRequestCompletionHandler = nil
    }
}

// MARK: - SKPaymentTransactionObserver

extension IAPHelper: SKPaymentTransactionObserver {
    
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch (transaction.transactionState) {
            case .purchased:
                complete(transaction: transaction)
                break
            case .failed:
                fail(transaction: transaction)
                break
            case .restored:
                restore(transaction: transaction)
                break
            case .deferred:
                break
            case .purchasing:
                break
            @unknown default:
                break
            }
        }
    }
    
    private func complete(transaction: SKPaymentTransaction) {

        SKPaymentQueue.default().finishTransaction(transaction)
        
        guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
        iapInternalService
            .syncCloudReceiptDatabase()
            .materialize()
            .subscribeNext { _ in
                NotificationCenter.default.post(name: .IAPHelperPurchasedSuccess, object: nil, userInfo: ["productID": productIdentifier])
            }
            .disposed(by: disposeBag)
        
    }
    
    private func restore(transaction: SKPaymentTransaction) {
        guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
        
        print("restore... \(productIdentifier)")
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    private func fail(transaction: SKPaymentTransaction) {
        print("fail...")
        if let transactionError = transaction.error as NSError?,
           let localizedDescription = transaction.error?.localizedDescription,
           transactionError.code != SKError.paymentCancelled.rawValue {
            print("Transaction Error: \(localizedDescription)")
        }
        
        SKPaymentQueue.default().finishTransaction(transaction)
        NotificationCenter.default.post(name: .IAPHelperPurchaseFailed, object: nil)
    }
}

extension IAPHelper {
    private func makeCreateInternalPurchaseAction() -> Action<PurchaseInfo,PurchaseInfo> {
        return .init { [weak self] request in
            guard let self = self else { return .empty() }
            return self.iapInternalService.purchaeProduct(request: request)
        }
    }
    
    private func makeSyncCloudReceiptDatabaseAction() -> Action<Void, Bool> {
        return .init { [weak self] in
            guard let self = self else { return  .empty() }
            return self.iapInternalService.syncCloudReceiptDatabase()
        }
    }
}

extension IAPHelper {
    
    private func refrehReceipt() {
        let refreshRequest = SKReceiptRefreshRequest()
        refreshRequest.delegate = self
        refreshRequest.start()
    }
    
    func verityReceipt() {
        guard let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
              FileManager.default.fileExists(atPath: appStoreReceiptURL.path) else {
            
            refrehReceipt()
            return
        }
        
        do {
            let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
            let receiptString = receiptData.base64EncodedString(options: [])
            print(receiptString)
            print("end")
            iapInternalService.updateReceiptData(data: receiptString)
            syncCloudReceiptDatabaseAction.execute()
            
        } catch {
            
        }
    }
}

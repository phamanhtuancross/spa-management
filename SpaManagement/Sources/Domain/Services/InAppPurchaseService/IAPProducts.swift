//
//  InAppPurchaseService.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 09/10/2021.
//

import Foundation

public struct IAProducts {
    
    public static let WeeklyPurchase = "app.purchase.weekly"
    public static let MonthlyPurhcase = "app.purchase.monthly"
    public static let YearlyPurhcase = "app.purchase.yearly"
    
    private static let productIdentifiers: Set<ProductIdentifier> = [IAProducts.WeeklyPurchase,
                                                                     IAProducts.MonthlyPurhcase,
                                                                     IAProducts.YearlyPurhcase]
    
    public static let store = IAPHelper(productIds: IAProducts.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}


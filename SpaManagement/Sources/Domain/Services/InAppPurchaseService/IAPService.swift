//
//  IAPService.swift
//  SpaManagement
//
//  Created by Tuan Pham on 11/10/2021.
//

import Foundation

public protocol IAPServiceType {
    func verifyReceipt(receiptData: String, password: String) -> Observable<IAPReceiptInfo>
}

import Foundation
import RxSwift

class IAPService: IAPServiceType {
    
    static var `default` = IAPService(networkManager: NetworkManager.default)
    private let networkManager: Networkable
    private let networkScheduler: SchedulerType
    
    init(networkManager: Networkable) {
        self.networkManager = networkManager
        self.networkScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
    }
    
    func verifyReceipt(receiptData: String, password: String) -> Observable<IAPReceiptInfo> {
        
        let target = IAPTargets.VerifyReceipt(receiptData: receiptData, password: password)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(IAPReceiptInfo.self)
            .asObservable()
    }
}

import Moya

protocol IAPTargetType: TargetType {}

extension IAPTargetType {
    var baseURL: URL {
        
        #if DEBUG
        let urlString = "https://sandbox.itunes.apple.com/verifyReceipt"
        #else
        let urlString = "https://buy.itunes.apple.com/verifyReceipt"
        #endif
        
        guard let url = URL(string: urlString) else {
            fatalError("Invalid API ")
        }
        
        return url
    }
}

enum IAPTargets {
    struct VerifyReceipt: IAPTargetType {
        
        let receiptData: String
        let password: String
        
        var path: String {
            return ""
        }
        
        var method: Moya.Method {
            return .post
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String: Any]? {
            return [
                "receipt-data": receiptData,
                "password": password,
                "exclude-old-transactions": false
            ]
        }
    }
}

public struct IAPReceiptInfo: Decodable {
    let environment: String
    let latest_receipt_info: [IAPReceipt]
    let status: Int
}

public struct IAPReceipt: Decodable {
    let quantity: String
    let product_id: String
    let transaction_id: String
    let original_transaction_id: String
    let purchase_date_ms: String
    let original_purchase_date_ms: String
    let expires_date_ms: String
    let is_trial_period: String
    let is_in_intro_offer_period: String
    let in_app_ownership_type: String
}

//
//  IAPInternalAppService.swift
//  SpaManagement
//
//  Created by Tuan Pham on 13/10/2021.
//

import Foundation
import RxSwift

public struct PurchaseInfo: Codable {
    let transactionId: String
    let originalTransactionId: String
    let bundleId: String
    let productId: String
    let purchaseDate: Int
    let expirationDate: Int
    let quantity: Int
}

public protocol IAPInternalServiceType {
    func getProductIndentifiers() -> Observable<[String]>
    func purchaeProduct(request: PurchaseInfo) -> Observable<PurchaseInfo>
    func getPurchasedProducts() -> Observable<[PurchaseInfo]>
    func checkAccessAvailability() -> Observable<Bool>
    
    func updateReceiptData(data: String)
    func syncCloudReceiptDatabase() -> Observable<Bool>
}

class IAPInternalService {
    static var `default` = IAPInternalService(networkManager: NetworkManager.default)
    private let networkManager: Networkable
    private let networkScheduler: SchedulerType
    
    private var receiptData: String = ""
    
    init(networkManager: Networkable) {
        self.networkManager = networkManager
        self.networkScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
    }
}

extension IAPInternalService: IAPInternalServiceType {
    
    func getProductIndentifiers() -> Observable<[String]> {
        let target = IAPInternalTargets.GetProductIDs()
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map([String].self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func purchaeProduct(request: PurchaseInfo) -> Observable<PurchaseInfo> {
        let target = IAPInternalTargets.PurchaseProduct(request: request)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(PurchaseInfo.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func getPurchasedProducts() -> Observable<[PurchaseInfo]> {
        let target = IAPInternalTargets.GetPurchasedProducts()
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map([PurchaseInfo].self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func checkAccessAvailability() -> Observable<Bool> {
        return .just(true)
//        let target = IAPInternalTargets.CheckAccessAvailability()
//        return networkManager
//            .request(target: target)
//            .observe(on: networkScheduler)
//            .map(Bool.self, atKeyPath: "accessAvailability")
//
//            .observe(on: MainScheduler.instance)
//            .asObservable()
    }
    
    func updateReceiptData(data: String) {
        self.receiptData = data
    }
    
    func syncCloudReceiptDatabase() -> Observable<Bool> {
        let target = IAPInternalTargets.SyncCloudReceiptDatabase(receiptData: self.receiptData)
        
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(Bool.self, atKeyPath: "verifyStatus")
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
}

import Moya

protocol IAPInternalTargetType: TargetType, AccessTokenAuthorizable {}

extension IAPInternalTargetType {
    var baseURL: URL {
        guard let url = URL(string: Natrium.apiBase) else {
            fatalError("Invalid API Base : \(Natrium.apiBase)")
        }
        
        return url
    }
    
    var authorizationType: AuthorizationType? {
        return .bearer
    }
}

enum IAPInternalTargets {
    struct GetProductIDs: IAPInternalTargetType {
        var path: String {
            return "/store/inApps-purchase/products"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestPlain
        }
    }
    
    struct GetPurchasedProducts: IAPInternalTargetType {
        var path: String {
            return "/store/inApps-purchase/purchased"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestPlain
        }
    }
    
    struct CheckAccessAvailability: IAPInternalTargetType {
        var path: String {
            return "/store/inApps-purchase/accessAvailability"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestPlain
        }
    }
    
    struct PurchaseProduct: IAPInternalTargetType {
        
        let request: PurchaseInfo
        
        var path: String {
            return "/store/inApps-purchase/purchase"
        }
        
        var method: Moya.Method {
            return .post
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String: Any]? {
            return request.dictionary
        }
    }
    
    struct SyncCloudReceiptDatabase: IAPInternalTargetType {
        
        let receiptData: String
        
        var path: String {
            return "/store/inApps-purchase/verify-receipt"
        }
        
        var method: Moya.Method {
            return .post
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String: Any]? {
            return [
                "receiptData": receiptData
            ]
        }
    }
}

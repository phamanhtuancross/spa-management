//
//  ProductService.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 29/09/2021.
//

import Foundation
import RxSwift

protocol ProductServiceType {
    func createProduct(product: Product) -> Observable<Product>
    func getProduct(by code: String) -> Observable<Product>
    func getProducts(paginationRequest: PagingnationRequest, searchtText: String) -> Observable<[Product]>
    func updateProduct(by code: String, updateProduct: Product) -> Observable<Product>
    func deleteProduct(by code: String) -> Observable<Product>
}

class ProductService {
    static var `default` = ProductService(networkManager: NetworkManager.default)
    private let networkManager: Networkable
    private let networkScheduler: SchedulerType
    
    init(networkManager: Networkable) {
        self.networkManager = networkManager
        self.networkScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
    }
}

extension ProductService: ProductServiceType {
    func createProduct(product: Product) -> Observable<Product> {
        let target = ProductTargets.CreateProduct(product: product)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(Product.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func getProduct(by code: String) -> Observable<Product> {
        let target = ProductTargets.GetProduct(code: code)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(Product.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func getProducts(paginationRequest: PagingnationRequest, searchtText: String) -> Observable<[Product]> {
        let target = ProductTargets.GetProducts(searchText: searchtText, paginationRequest: paginationRequest)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map([Product].self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func updateProduct(by code: String, updateProduct: Product) -> Observable<Product> {
        let target = ProductTargets.UpdateProduct(code: code, product: updateProduct)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(Product.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func deleteProduct(by code: String) -> Observable<Product> {
        let target = ProductTargets.DeleteProduct(code: code)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(Product.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    
}

import Moya

protocol ProductTargetType: TargetType, AccessTokenAuthorizable {}

extension ProductTargetType {
    var baseURL: URL {
        guard let url = URL(string: Natrium.apiBase) else {
            fatalError("Invalid API Base : \(Natrium.apiBase)")
        }
        
        return url
    }
    
    var authorizationType: AuthorizationType? {
        return .bearer
    }
}

enum ProductTargets {
    struct CreateProduct: ProductTargetType {
        
        let product: Product
        
        var path: String {
            return "/products"
        }
        
        var method: Moya.Method {
            return .post
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String : Any]? {
            return product.dictionary
        }
        
        
    }
    
    struct GetProduct: ProductTargetType {
        
        let code: String
        
        var path: String {
            return "/products/\(code)"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestPlain
        }
    }
    
    struct GetProducts: ProductTargetType {
        
        let searchText: String
        let paginationRequest: PagingnationRequest
        
        var path: String {
            return "/products"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: urlParameters.or([:]), encoding: URLEncoding.queryString)
        }
        
        var urlParameters: [String : Any]? {
            return [
                "searchText": searchText,
                "skip": paginationRequest.skip,
                "limit": paginationRequest.limit
            ]
        }
    }
    
    struct UpdateProduct: ProductTargetType {
        
        let code: String
        let product: Product
        
        var path: String {
            return "/products/\(code)"
        }
        
        var method: Moya.Method {
            return .patch
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String : Any]? {
            return [
                "name": product.name,
                "price": product.price,
                "description": product.description,
                "imageUrl": product.imageUrl.orEmpty
            ]
        }
    }
    
    struct DeleteProduct: ProductTargetType {
        
        let code: String
        
        var path: String {
            return "/products/\(code)"
        }
        
        var method: Moya.Method {
            return .delete
        }
        
        var task: Task {
            return .requestPlain
        }
    }
}

//
//  AuthService.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 21/09/2021.
//

import Foundation
import RxSwift

protocol UserServiceType {
    func signUp(username: String, phoneNumber: String, password: String) -> Observable<User>
    func signIn(phoneNumber: String, password: String) -> Observable<User>
    func getProfile() -> Observable<User>
    func logout() -> Observable<Bool>
    func changePassword(oldPassword: String, newPassword: String) -> Observable<User>
}

class UserService {
    
    static var `default` = UserService(networkManager: NetworkManager.default)
    let networkManager: Networkable
    private let networkScheduler: SchedulerType
    
    init(networkManager: Networkable) {
        self.networkManager = networkManager
        self.networkScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
    }
}

extension UserService: UserServiceType {
    func signUp(username: String, phoneNumber: String, password: String) -> Observable<User> {
        
        let target = AuthenTargets.SignUp(username: username, phoneNumber: phoneNumber, password: password)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(AuthenticationInfo .self)
            .asObservable()
            .doOnNext { [weak self] authenInfo  in
                guard let self = self else { return }
                self.networkManager.storage(tokenInString: authenInfo.token)
            }
            .map { $0.user }
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func getProfile() -> Observable<User> {
        let target = AuthenTargets.GetProfile()
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(User .self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func signIn(phoneNumber: String, password: String) -> Observable<User> {
        
        let target = AuthenTargets.SignIn(phoneNumber: phoneNumber, password: password)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(AuthenticationInfo .self)
            .asObservable()
            .doOnNext{ [weak self] authenInfo in
                guard let self = self else { return }
                self.networkManager.storage(tokenInString: authenInfo.token)
            }
            .map { $0.user }
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func logout() -> Observable<Bool> {
        let target = AuthenTargets.Logout()
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .asObservable()
            .map{ _ in true }
            .doOnNext{ [weak self] _ in
                guard let self = self else { return }
                self.networkManager.clearToken()
            }
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func changePassword(oldPassword: String, newPassword: String) -> Observable<User> {
        let target = AuthenTargets.ChangePassword(oldPassword: oldPassword, newPassword: newPassword)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(User.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
}

import Moya

protocol UserTargetType: TargetType {}
protocol UserAuthenTargetType: UserTargetType, AccessTokenAuthorizable {}

extension UserTargetType {
    var baseURL: URL {
        guard let url = URL(string: Natrium.apiBase) else {
            fatalError("Invalid API Base : \(Natrium.apiBase)")
        }
        
        return url
    }
}

extension UserAuthenTargetType {
    var authorizationType: AuthorizationType? {
        return .bearer
    }
}

enum AuthenTargets {
    struct SignUp: UserTargetType {
        
        let username: String
        let phoneNumber: String
        let password: String
        
        var path: String {
            return "/users"
        }
        
        var method: Moya.Method {
            return .post
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String : Any]? {
            return [
                "username": username,
                "phoneNumber": phoneNumber,
                "password": password
            ]
        }
    }
    
    struct SignIn: UserTargetType {
        
        let phoneNumber: String
        let password: String
        
        var path: String {
            return "/users/login"
        }
        
        var method: Moya.Method {
            return .post
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String : Any]? {
            return [
                "phoneNumber": phoneNumber,
                "password": password
            ]
        }
    }
    
    struct ChangePassword: UserAuthenTargetType {
        
        let oldPassword: String
        let newPassword: String
        
        var path: String {
            return "/users/changePassword"
        }
        
        var method: Moya.Method {
            return .post
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String : Any]? {
            return [
                "oldPassword": oldPassword,
                "newPassword": newPassword
            ]
        }
    }
    
    
    struct GetProfile: UserAuthenTargetType {
        
        var path: String {
            return "/users/me"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestPlain
        }
    }
    
    struct Logout: UserAuthenTargetType {
        
        var path: String {
            return "/users/logout"
        }
        
        var method: Moya.Method {
            return .post
        }
        
        var task: Task {
            return .requestPlain
        }
    }
}
    

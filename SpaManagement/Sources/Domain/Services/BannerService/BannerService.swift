//
//  BannerService.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//

import Foundation
import RxSwift

protocol BannerServiceType {
    func getBanners() -> Observable<[Banner]>
}

class BannerService {
    static var `default` = BannerService()
}

extension BannerService: BannerServiceType {
    func getBanners() -> Observable<[Banner]> {
        return .of([
            .init(urlString: "https://media.istockphoto.com/photos/woman-enjoying-in-head-massage-during-beauty-treatment-in-the-spa-picture-id1185812873?b=1&k=20&m=1185812873&s=170667a&w=0&h=d4I8Jvs5qx_qSPnrPeqmVYzav3C2Q1au0HTRrJsj9pM="),
            .init(urlString:"https://media.istockphoto.com/photos/black-mom-and-daughter-in-bathrobes-lying-with-cucumber-slices-on-picture-id1284082197?b=1&k=20&m=1284082197&s=170667a&w=0&h=PdlKzYiAwnWVWY5vDs1uRIJbinrrK59-gfbo1XC4qOw="),
            .init(urlString: "https://media.istockphoto.com/photos/beauty-treatment-items-for-spa-procedures-on-white-wooden-table-picture-id1286682876?b=1&k=20&m=1286682876&s=170667a&w=0&h=I_b9K2r8CVmVN87aaSSkIjwRDmSF02MhcMP2bJRTz-I=")
        ])
    }
}



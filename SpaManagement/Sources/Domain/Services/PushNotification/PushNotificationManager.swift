//
//  PushNotificationManager.swift
//  SpaManagement
//
//  Created by Tuan Pham on 31/10/2021.
//

import Foundation
import UserNotifications
import UIKit

class PushNotificationManager {
    static let shared = PushNotificationManager()
    
    private init() {}
    
    func register() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { [weak self] granted, error in
            guard granted, let self = self else { return }
            self.getSettings()
        }
    }
    
    private func getSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
}

//
//  UploadFileService.swift
//  SpaManagement
//
//  Created by Tuan Pham on 17/10/2021.
//

import Foundation
import FirebaseStorage
import RxSwift

enum UploadFilesDestination: String {
    case products
    case customers
}

protocol UploadFilesServiceType {
    func uploadImage(imageData: Data, to destination: UploadFilesDestination) -> Observable<String>
}

class UploadFilesService {
    
    static let `default` = UploadFilesService()
    
    private let storageRef: StorageReference
    private let imageStorageRef: StorageReference
    init() {
        storageRef = Storage.storage().reference()
        imageStorageRef = storageRef.child("images")
    }
}

extension UploadFilesService: UploadFilesServiceType {
    func uploadImage(imageData: Data, to destination: UploadFilesDestination) -> Observable<String> {
        
        let id = UUID().uuidString
        let destImageStorageRef = imageStorageRef.child("\(destination.rawValue)/\(id)")
       
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        return Observable.create { observable in
            
            destImageStorageRef.putData(imageData, metadata: metadata) { metaData, error in
                if let error = error {
                    observable.onError(UploadFilesError.uploadImageError(error: error))
                    return
                }
                
                destImageStorageRef.downloadURL { url, error in
                    
                    if let error = error {
                        observable.onError(UploadFilesError.downloadImageError(error: error))
                        return
                    }
                    
                    guard let urlString = url?.absoluteString else {
                        observable.onError(UploadFilesError.invalidDownloadImageURL)
                        return
                    }
                    
                    observable.onNext(urlString)
                }
            }
            
            return Disposables.create()
        }
    }
}

enum UploadFilesError: Error {
    case invalidDownloadImageURL
    case uploadImageError(error: Error)
    case downloadImageError(error: Error)
}

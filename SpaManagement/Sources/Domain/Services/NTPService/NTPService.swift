//
//  NTPService.swift
//  SpaManagement
//
//  Created by Tuan Pham on 15/10/2021.
//

import Foundation
import Kronos

public protocol NTPClientType {
    func syncDate()
}

public final class NTPClient: NTPClientType {
    public static let `default` = NTPClient()
    
    public func syncDate() {
        Clock.sync()
    }
}

extension Date {
    static var trueTime: Date {
        return Clock.now.or(Date())
    }
}

//
//  TransactionService.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 19/09/2021.
//

import Foundation
import RxSwift

protocol TransactionServiceType {
    func createTransaction(transaction: Transaction, customerId: String) -> Observable<Transaction>
    func getTransactions(customerId: String,
                         pagingnationRequest: PagingnationRequest) -> Observable<[Transaction]>
    
    func updateTransaction(for transactionId: String, status: PaymentStatus?, method: PaymentMethodType?) -> Observable<Transaction>
}

class TransactionService {
    static var `default` = TransactionService(networkManager: NetworkManager.default)
    let networkManager: Networkable
    private let networkScheduler: SchedulerType
    
    init(networkManager: Networkable) {
        self.networkManager = networkManager
        self.networkScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
    }
}
   
extension TransactionService: TransactionServiceType {
    func createTransaction(transaction: Transaction, customerId: String) -> Observable<Transaction> {
        let target = TransactionTargets.CreateTransaction(transaction: transaction, customerId: customerId)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(Transaction.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func getTransactions(customerId: String,
                         pagingnationRequest: PagingnationRequest) -> Observable<[Transaction]> {
        
        let target = TransactionTargets.GetTransactions(customerId: customerId,
                                                        pagingnationRequest: pagingnationRequest)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map([Transaction].self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func updateTransaction(for transactionId: String, status: PaymentStatus?, method: PaymentMethodType?) -> Observable<Transaction> {
        let target = TransactionTargets.UpdateTransaction(transactionId: transactionId,
                                                          paymentStatus: status,
                                                          paymentMethod: method)
        
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(Transaction.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
}

import Moya

protocol TransactionTargetType: TargetType, AccessTokenAuthorizable {}

extension TransactionTargetType {
    var baseURL: URL {
        guard let url = URL(string: Natrium.apiBase) else {
            fatalError("Invalid API Base : \(Natrium.apiBase)")
        }
        
        return url
    }
    
    var authorizationType: AuthorizationType? {
        return .bearer
    }
}
enum TransactionTargets {
    
    struct GetTransactions: TransactionTargetType {
        
        let customerId: String
        let pagingnationRequest: PagingnationRequest
        
        var path: String {
            return "/transactions/\(customerId)"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: urlParameters, encoding: URLEncoding.queryString)
        }
        
        var urlParameters: [String: Any] {
            return [
                "skip": pagingnationRequest.skip,
                "limit": pagingnationRequest.limit
            ]
        }
    }
    struct CreateTransaction: TransactionTargetType {
        
        let transaction: Transaction
        let customerId: String
        
        var path: String {
            return "/transactions/\(customerId)"
        }
        
        var method: Moya.Method {
            return .post
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String: Any]? {
            return transaction.dictionary
        }
    }
    
    struct UpdateTransaction: TransactionTargetType {
        let transactionId: String
        let paymentStatus: PaymentStatus?
        let paymentMethod: PaymentMethodType?
        
        var path: String {
            return "/transactions/\(transactionId)"
        }
        
        var method: Moya.Method {
            return .patch
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String: Any]? {
            var result: [String: Any] = [:]
            
            if let paymentStatus = paymentStatus {
                result["status"] = paymentStatus.rawValue
            }
            
            if let paymentMethod = paymentMethod {
                result["method"] = paymentMethod.rawValue
            }
            
            return result
        }
    }
}

extension Encodable {
  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}

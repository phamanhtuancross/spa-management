//
//  CalendarServiceType.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 07/10/2021.
//

import Foundation
import RxSwift

protocol EventsServiceType {
    func createEvent(request: CreateEventRequest,
                     for customerId: String) -> Observable<Event>
    
    func getEvents(of customerId: String,
                   pagingnationRequest: PagingnationRequest) -> Observable<[Event]>
    
    func deleteEvent(id: String) -> Observable<Event>
}

class EventsService {
    static var `default` = EventsService(networkManager: NetworkManager.default)
    let networkManager: Networkable
    private let networkScheduler: SchedulerType
    
    init(networkManager: Networkable) {
        self.networkManager = networkManager
        self.networkScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
    }
}

extension EventsService: EventsServiceType {
    
    func createEvent(request: CreateEventRequest,
                     for customerId: String) -> Observable<Event> {
        
        let target = CalendarTargets.CreateCalendar(request: request, customerId: customerId)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(Event.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func getEvents(of customerId: String, pagingnationRequest: PagingnationRequest) -> Observable<[Event]> {
        let target = CalendarTargets.GetCalendars(customerId: customerId,
                                                  pagingnationRequest: pagingnationRequest)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map([Event].self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func deleteEvent(id: String) -> Observable<Event> {
        let target = CalendarTargets.DeleteCalendar(id: id)
        
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(Event.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
}

import Moya

protocol CalendarTargetType: TargetType, AccessTokenAuthorizable {}

extension CalendarTargetType {
    var baseURL: URL {
        guard let url = URL(string: Natrium.apiBase) else {
            fatalError("Invalid API Base : \(Natrium.apiBase)")
        }
        
        return url
    }
    
    var authorizationType: AuthorizationType? {
        return .bearer
    }
}

enum CalendarTargets {
    struct CreateCalendar: CalendarTargetType {
        let request: CreateEventRequest
        let customerId: String
        
        var path: String {
            return "/calendars/\(customerId)"
        }
        
        var method: Moya.Method {
            return .post
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String: Any]? {
            return [
                "description": request.calendarDescription,
                "happenAt": request.happenAt
            ]
        }
    }
    
    struct GetCalendars: CalendarTargetType {
        
        let customerId: String
        let pagingnationRequest: PagingnationRequest
        
        var path: String {
            return "/calendars/\(customerId)"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: urlParameters, encoding: URLEncoding.queryString)
        }
        
        var urlParameters: [String: Any] {
            return [
                "skip": pagingnationRequest.skip,
                "limit": pagingnationRequest.limit
            ]
        }
    }
    
    struct DeleteCalendar: CalendarTargetType {
        
        let id: String
        
        var path: String {
            return "/calendars/\(id)"
        }
        
        var method: Moya.Method {
            return .delete
        }
        
        var task: Task {
            return .requestPlain
        }
    }
}

//
//  CustomerService.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//

import Foundation
import RxSwift

struct PagingnationRequest {
    let skip: Int
    let limit: Int
    
    init(skip: Int = 0, limit: Int = 10) {
        self.skip = skip
        self.limit = limit
    }
    
    func next() -> PagingnationRequest {
        return .init(skip: skip + limit, limit: limit)
    }
    
    static let `default` = PagingnationRequest()
}

protocol CustomerServiceType {
   
    func getCustomers(searchText: String, paginationRequest: PagingnationRequest) -> Observable<[Customer]>
    func getTopRankingCustomer() -> Observable<[Customer]>
    func updateCustomer(name: String, phoneNumber: String, email: String?, customerId: String, dateOfBirth: Int) -> Observable<Customer>
    func createCustomer(name: String, phoneNumber: String, email: String?, dateOfBirth: Int) -> Observable<Customer>
}

class CustomerService {
    
    static var `default` = CustomerService(networkManager: NetworkManager.default)
    let networkManager: Networkable
    private let networkScheduler: SchedulerType
    
    init(networkManager: Networkable) {
        self.networkManager = networkManager
        self.networkScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
    }
}

extension CustomerService: CustomerServiceType {
    func getCustomers(searchText: String, paginationRequest: PagingnationRequest) -> Observable<[Customer]> {
        
        let target = CustomerTargets.GetCustomers(searchText: searchText, paginationRequest: paginationRequest)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map([Customer].self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func getTopRankingCustomer() -> Observable<[Customer]> {
        let target = CustomerTargets.GetTopRankingCustomers(limit: 8)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map([Customer].self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func updateCustomer(name: String, phoneNumber: String, email: String?, customerId: String, dateOfBirth: Int) -> Observable<Customer> {
        let target = CustomerTargets.UpdateCustomers(name: name, phoneNumber: phoneNumber, email: email,customerId: customerId, dateOfBirth: dateOfBirth)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(Customer.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func createCustomer(name: String, phoneNumber: String, email: String?, dateOfBirth: Int) -> Observable<Customer> {
        let target = CustomerTargets.CreateCustomer(name: name, phoneNumber: phoneNumber, email: email, dateOfBirth: dateOfBirth)
        return networkManager
            .request(target: target)
            .observe(on: networkScheduler)
            .map(Customer.self)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
}

import Moya

protocol CustomerTargetType: TargetType, AccessTokenAuthorizable {}

extension CustomerTargetType {
    var baseURL: URL {
        guard let url = URL(string: Natrium.apiBase) else {
            fatalError("Invalid API Base : \(Natrium.apiBase)")
        }
        
        return url
    }
    
    var authorizationType: AuthorizationType? {
        return .bearer
    }
}
enum CustomerTargets {
    struct GetCustomers: CustomerTargetType {
        
        let searchText: String
        let paginationRequest: PagingnationRequest
        
        var path: String {
            return "/customers"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: urlParameters.or([:]), encoding: URLEncoding.queryString)
        }
        
        var urlParameters: [String : Any]? {
            return [
                "searchText": searchText,
                "skip": paginationRequest.skip,
                "limit": paginationRequest.limit
            ]
        }
    }
    
    struct GetTopRankingCustomers: CustomerTargetType {
    
        let limit: Int
        
        var path: String {
            return "/customers/topRanking"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: urlParameters.or([:]), encoding: URLEncoding.queryString)
        }
        
        var urlParameters: [String : Any]? {
            return [
                "limit": limit
            ]
        }
    }
    
    struct CreateCustomer: CustomerTargetType {
        
        let name: String
        let phoneNumber: String
        let email: String?
        let dateOfBirth: Int
        
        var path: String {
            return "/customers"
        }
        
        var method: Moya.Method {
            return .post
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String : Any]? {
            return [
                "name": name,
                "phoneNumber": phoneNumber,
                "email": email.or(""),
                "dateOfBirth": dateOfBirth
            ]
        }
    }
    
    struct UpdateCustomers: CustomerTargetType {
        
        let name: String
        let phoneNumber: String
        let email: String?
        let customerId: String
        let dateOfBirth: Int
        
        var path: String {
            return "/customers/\(customerId)"
        }
        
        var method: Moya.Method {
            return .patch
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters.or([:]), encoding: JSONEncoding.default)
        }
        
        var parameters: [String : Any]? {
            return [
                "name": name,
                "phoneNumber": phoneNumber,
                "email": email.or(""),
                "dateOfBirth": dateOfBirth
            ]
        }
    }
}

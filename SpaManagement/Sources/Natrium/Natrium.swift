//
//  Natrium.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//

import Foundation

enum AppEnvironment {
    case dev
    case staging
    case uat
    case production
}

protocol NatriumMappable {
    var evironmentParameters: [AppEnvironment: String] { get }
}

extension NatriumMappable {
    var value: String {
        return (evironmentParameters[AppEnvironment.current]).or((evironmentParameters.first?.value).or(""))
    }
}

enum ApiBase: NatriumMappable {
    case `current`
    
    var evironmentParameters: [AppEnvironment : String] {
        return [
            .dev : "http://localhost:3000",
            .staging: "https://spa-management.herokuapp.com",
            .uat: "https://spa-management.herokuapp.com",
            .production: "https://spa-management.herokuapp.com"
        ]
    }
}

enum AppLink: NatriumMappable {
    case `current`
    var evironmentParameters: [AppEnvironment : String] {
        return [
            .dev: "https://www.facebook.com/pham.tuan.507679"
        ]
    }
}

enum AppID: NatriumMappable {
    case `current`
    var evironmentParameters: [AppEnvironment : String] {
        return [
            .dev: "id959379869"
        ]
    }
}

enum AppStaticUrls {
    
    case `current`
    var rating: String {
        return "itms-apps://itunes.apple.com/app/" + AppID.current.value
    }
}


extension AppEnvironment {
    
    static let current: AppEnvironment = .production
}

enum Natrium {
    public static var apiBase: String = ApiBase.current.value
    public static var appLink: String {
        return AppLink.current.value
    }
    
    public static var appStaticUrls = AppStaticUrls.current
}

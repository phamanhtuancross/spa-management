import UIKit

protocol ViewInterface: AnyObject {
    func showNotifyAlert(title: String?,
                         message: String?,
                         completion: VoidCallBack?)
}

extension ViewInterface {
}

extension ViewInterface where Self: UIViewController {
    func showNotifyAlert(title: String? = nil,
                         message: String? = nil,
                         completion: VoidCallBack?) {
        
        let alertControler = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel) { _ in
            completion?()
        }
        
        alertControler.addAction(okAction)
        present(alertControler, animated: true, completion: nil)
    }
}

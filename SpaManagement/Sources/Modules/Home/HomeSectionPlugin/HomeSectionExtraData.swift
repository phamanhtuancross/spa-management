//
//  HomeSectionExtraDaa.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/17/21.
//

import Foundation

struct HomeSectionItem {
    let code: HomeSectionCodeType
    let extraData: [String: Any]
}

//
//  HomeSectionPlugin.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/17/21.
//

import Foundation

protocol HomeSectionPlugin: AnyObject {
    
    var listener: HomeSectionPluginListener? { get set }
    var id: String { get }
    var extraData: [String: Any] { get }
    
    init(id: String, extraData: [String: Any], service: AnyObject?)
    func build(listener: HomeSectionPluginListener) -> BaseWireframe
}

enum HomeSectionCodeType: String {
    case none = ""
    case bannerSection = "banner"
    case salesOverviewSection = "salesOverview"
    case connectedAppSection = "connectedApp"
    case eventsSection = "events"
    case statisticSection = "statistic"
    case ranking = "ranking"
    case userProfileSection = "userProfile"
    case topSoldOutProductsSection = "topSoldOutProducts"
}

enum HomeSectionRegisteredPlugins {
    static var registeredPluginsMapping: [String: HomeSectionPlugin.Type] {
        return [
            HomeSectionCodeType.bannerSection.rawValue: BannerTemplatePlugin.self,
            HomeSectionCodeType.salesOverviewSection.rawValue: SalesOverviewPlugin.self,
            HomeSectionCodeType.connectedAppSection.rawValue: ConnectedAppPlugin.self,
            HomeSectionCodeType.eventsSection.rawValue: EventsPlugin.self,
            HomeSectionCodeType.statisticSection.rawValue: StatisticsPlugin.self,
            HomeSectionCodeType.ranking.rawValue: RankingPlugin.self,
            HomeSectionCodeType.userProfileSection.rawValue: UserProfilePlugin.self,
            HomeSectionCodeType.topSoldOutProductsSection.rawValue: TopSoldProductPlugin.self
        ]
    }
}


//
//  EventsPlugin.swift
//  SpaManagement
//
//  Created by Tuan Pham on 07/11/2021.
//
import Foundation

class EventsPlugin: HomeSectionPlugin {
    
    var listener: HomeSectionPluginListener?
    let id: String
    let extraData: [String : Any]
    
    required init(id: String, extraData: [String : Any], service: AnyObject?) {
        
        self.id = id
        self.extraData = extraData
    }
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return EventsWidgetWireframe()
    }
}

//
//  EventsWidgetWireframe.swift
//  SpaManagement
//
//  Created by Tuan Pham on 07/11/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class EventsWidgetWireframe: BaseWireframe {

    // MARK: - Private properties -

    private let storyboard = UIStoryboard(name: "EventsWidget", bundle: nil)

    // MARK: - Module setup -

    init() {
        let moduleViewController = storyboard.instantiateViewController(ofType: EventsWidgetViewController.self)
        super.init(viewController: moduleViewController)

        let interactor = EventsWidgetInteractor()
        let presenter = EventsWidgetPresenter(view: moduleViewController, interactor: interactor, wireframe: self)
        moduleViewController.presenter = presenter
    }

}

// MARK: - Extensions -

extension EventsWidgetWireframe: EventsWidgetWireframeInterface {
    
    func navigate(to option: EventsWidgetNavigationOption) {
        switch option {
        case .events:
            let wireframe = EventsWireframe()
            navigationController?.pushWireframe(wireframe)
        }
    }
}

//
//  StatisticsPlugin.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 20/09/2021.
//

import Foundation

class StatisticsPlugin: HomeSectionPlugin {
    
    var listener: HomeSectionPluginListener?
    let id: String
    let extraData: [String : Any]
    
    required init(id: String, extraData: [String : Any], service: AnyObject?) {
        
        self.id = id
        self.extraData = extraData
    }
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return StatisticsWidgetWireframe()
    }
}

//
//  StatisticsWidgetViewController.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 20/09/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit
import Charts
import RxSwift
import RxCocoa
import SkeletonView

protocol StatisticsWidgetPresenterInterface: PresenterInterface {
}

final class StatisticsWidgetViewController: UIViewController {

    // MARK: - Public properties -
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet private weak var chartView: LineChartView!
    
    @IBOutlet private weak var chartHeightConstraint: NSLayoutConstraint!
    private let disposeBag = DisposeBag()
    
    var presenter: StatisticsWidgetPresenterInterface!
    let viewModel = BehaviorRelay<SalesStatistivViewModel?>(value: nil)
    // MARK: - Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        defer { presenter.viewDidLoad() }
        
        configureView()
        bindingData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        chartHeightConstraint.constant = UIScreen.main.bounds.width * 9 / 16
    }
    
    private func configureView() {
        
        configureLoadingView()
        configureChartView()
    }
    
    private func configureLoadingView() {
        loadingView.makeAppBorderStyle()
        loadingView.showAnimatedSkeleton()
    }
    
    private func configureChartView() {
        
        containerView.makeBorder(borderWidth: 1.5)
        containerView.makeCornerRadidus(radius: 20)
        containerView.backgroundColor = Colors.appBackgroudColor.withAlphaComponent(0.1)
        
        chartView.borderColor = .clear
        chartView.drawBordersEnabled = false
        chartView.drawGridBackgroundEnabled = false
        chartView.gridBackgroundColor = .white
        chartView.animate(yAxisDuration: 2)
        chartView.rightAxis.enabled = false
        chartView.leftAxis.enabled = false
        chartView.chartDescription?.enabled = false
        
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.gridColor = .clear
        chartView.xAxis.labelTextColor = Colors.ink400s
        chartView.xAxis.labelFont = .systemFont(ofSize: 8, weight: .light)
        chartView.xAxis.wordWrapEnabled = true
        chartView.xAxis.axisLineColor = Colors.blue200
        chartView.xAxis.granularity = 1
        chartView.xAxis.labelCount = 31
        chartView.xAxis.valueFormatter = DateAxisValueFormatter(filterDateType: .thisWeek)
        
        chartView.legend.enabled = false
        
    }
    
    private func bindingData() {
        viewModel
            .filterNil()
            .map { $0.lineChardData }
            .asDriver(onErrorDriveWith: .never())
            .driveNext { [weak self] data in
                guard let self = self else { return }
                self.chartView.data = data
            }
            .disposed(by: disposeBag)
    }
}

// MARK: - Extensions -

extension StatisticsWidgetViewController: StatisticsWidgetViewInterface {
}

extension StatisticsWidgetViewController: StatePresentable {
    func stateLoadingView() -> UIView? {
        return loadingView
    }
}

//
//  StatisticsWidgetInterfaces.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 20/09/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

protocol StatisticsWidgetWireframeInterface: WireframeInterface {
}

protocol StatisticsWidgetViewInterface: ViewInterface {
}

protocol StatisticsWidgetPresenterInterface: PresenterInterface {
}

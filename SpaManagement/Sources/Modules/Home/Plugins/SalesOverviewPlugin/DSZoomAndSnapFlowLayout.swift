//
//  DSZoomAndSnapFlowLayout.swift
//  SpaManagement
//
//  Created by Tuan Pham on 07/11/2021.
//

import UIKit

public class DSZoomAndSnapFlowLayout: UICollectionViewFlowLayout {
    
    private var activeDistance: CGFloat = 128
    private var zoomFactor: CGFloat = 0.28
    
    override init() {
        super.init()
        
        scrollDirection = .horizontal
        minimumLineSpacing = 44
        minimumInteritemSpacing = 44
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func prepare() {
        guard let collectionView = collectionView, collectionView.numberOfSections > 0
        else { return }
        
        let fraction = 1 - zoomFactor
        let cellWidth = UIScreen.main.bounds.width * fraction
        let cellHeight = collectionView.bounds.height * fraction
       
        itemSize = .init(width: cellWidth,
                         height: cellHeight)
        
        let horizontalInsets = (UIScreen.main.bounds.width - cellWidth) / 2
        
        sectionInset = UIEdgeInsets(top: .zero, left: horizontalInsets, bottom: .zero, right: horizontalInsets)
    
        super.prepare()
    }
    
    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        guard let collectionView = collectionView else { return nil }
        guard let rectAttributes = super.layoutAttributesForElements(in: rect)?
                .compactMap({ $0.copy() as? UICollectionViewLayoutAttributes })
        else { return nil }
        
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.frame.size)
        
        for attributes in rectAttributes where attributes.frame.intersects(visibleRect) {
            let distance = visibleRect.midX - attributes.center.x
            let normalizedDistance = distance / activeDistance
            
            if distance.magnitude < activeDistance {
                let zoom = 1 + zoomFactor * (1 - normalizedDistance.magnitude)
                attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1)
                attributes.zIndex = Int(zoom.rounded())
            }
        }
        
        return rectAttributes
    }
    
    override public func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint,
                                             withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collectionView = collectionView else { return .zero }
        
        let targetRect = CGRect(x: proposedContentOffset.x,
                                y: 0,
                                width: collectionView.frame.width,
                                height: collectionView.frame.height)
        guard let rectAttributes = super.layoutAttributesForElements(in: targetRect)
        else { return .zero }
        
        var offsetAdjustment = CGFloat.greatestFiniteMagnitude
        let horizontalCenter = proposedContentOffset.x + collectionView.frame.width / 2
        
        for layoutAttributes in rectAttributes {
            let itemHorizontalCenter = layoutAttributes.center.x
            if (itemHorizontalCenter - horizontalCenter).magnitude < offsetAdjustment.magnitude {
                offsetAdjustment = itemHorizontalCenter - horizontalCenter
            }
        }
        
        return CGPoint(x: proposedContentOffset.x + offsetAdjustment,
                       y: proposedContentOffset.y)
    }
    
    override public func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}

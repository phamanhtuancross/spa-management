//
//  SalesOverviewItemCell.swift
//  SpaManagement
//
//  Created by Tuan Pham on 07/11/2021.
//

import UIKit
import Reusable

struct SalesOverviewItemViewModel {
    
}


class SalesOverviewItemCell: UICollectionViewCell, NibReusable {
    
    private let colorStyles: [UIColor] = [Colors.appColor, #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)]

    @IBOutlet private weak var categoryImageContainerView: UIView!
    @IBOutlet private weak var categoryTitleLabel: UILabel!
    
    @IBOutlet private weak var diffPercentValueLabel: UILabel!
    @IBOutlet private weak var moreInfoDescriptionLabel: UILabel!
    @IBOutlet private weak var mainContainerView: UIView!
    
    @IBOutlet private weak var mainValueLabel: UILabel!
    @IBOutlet private weak var containerWrapView: UIView!
    @IBOutlet private weak var mainShadowView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        mainContainerView.makeAppBorderStyle(borderWidth: 0.5, borderColor: Colors.ink400s, conerRadius: 20.0)
        mainContainerView.backgroundColor = Colors.appColor.withAlphaComponent(0.02)
        containerWrapView.makeCornerRadidus(radius: 20)
        mainShadowView.setShadowStyle(.shadow8)
        
        categoryImageContainerView.makeCircular()
        
        diffPercentValueLabel.setStyle(DS.T16M(color: Colors.appColor))
        moreInfoDescriptionLabel.setStyle(DS.T14R(color: Colors.ink400s))
        
        categoryTitleLabel.setStyle(DS.T16M(color: Colors.ink500))
        mainValueLabel.setStyle(DS.T28SM(color: Colors.appColor))
    }

    func configureCell(viewModel: SalesOverviewItemViewModel, index: Int) {
        
        let color = colorStyles[index % colorStyles.count]
        mainContainerView.backgroundColor = color.withAlphaComponent(0.02)
        mainValueLabel.textColor = color
        
        categoryImageContainerView.backgroundColor = color
        
        mainContainerView.makeAppBorderStyle(borderWidth: 0.5, borderColor: color, conerRadius: 20.0)
    }
}

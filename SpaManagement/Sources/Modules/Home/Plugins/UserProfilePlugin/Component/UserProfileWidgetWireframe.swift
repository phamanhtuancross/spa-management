//
//  UserProfileWidgetWireframe.swift
//  SpaManagement
//
//  Created by Tuan Pham on 07/11/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class UserProfileWidgetWireframe: BaseWireframe {

    // MARK: - Private properties -

    private let storyboard = UIStoryboard(name: "UserProfileWidget", bundle: nil)

    // MARK: - Module setup -

    init() {
        let moduleViewController = storyboard.instantiateViewController(ofType: UserProfileWidgetViewController.self)
        super.init(viewController: moduleViewController)

        let interactor = UserProfileWidgetInteractor()
        let presenter = UserProfileWidgetPresenter(view: moduleViewController, interactor: interactor, wireframe: self)
        moduleViewController.presenter = presenter
    }

}

// MARK: - Extensions -

extension UserProfileWidgetWireframe: UserProfileWidgetWireframeInterface {
}

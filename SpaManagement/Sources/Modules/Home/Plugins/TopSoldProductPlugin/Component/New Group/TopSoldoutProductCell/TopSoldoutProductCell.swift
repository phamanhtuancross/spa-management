//
//  TopSoldoutProductCell.swift
//  SpaManagement
//
//  Created by Tuan Pham on 07/11/2021.
//

import UIKit
import Reusable

struct TopSoldoutProductViewModel {
    
}

class TopSoldoutProductCell: UICollectionViewCell, NibReusable {

    @IBOutlet private weak var mainShadowView: UIView!
    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var productImageContainerView: UIView!
    
    @IBOutlet private weak var productNameLabel: UILabel!
    @IBOutlet private weak var productValueLabel: UILabel!
    @IBOutlet private weak var totalSoldoutLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews() {
        
        productImageContainerView.makeCornerRadidus(radius: 20)
        productNameLabel.setStyle(DS.T20R(color: Colors.ink500))
        
        productValueLabel.setStyle(DS.T20M(color: Colors.appColor))
        totalSoldoutLabel.setStyle(DS.T20R(color: Colors.blue400))
    }
    
    func configureCell(viewModel: TopSoldoutProductViewModel) {}
}

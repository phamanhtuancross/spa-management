//
//  TopCustomerCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 20/09/2021.
//

import UIKit
import Reusable

class TopCustomerCell: UICollectionViewCell, NibReusable {

    private let avatarColors: [UIColor] = [
        #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),#colorLiteral(red: 1, green: 0.1857388616, blue: 0.5733950138, alpha: 0.5476740149),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
    ]
    
    @IBOutlet private weak var bannerContainerView: UIView!
    @IBOutlet private weak var avatarContainerView: UIView!
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var shortcutCustomerNameLabel: UILabel!
    @IBOutlet private weak var customerNameLabel: UILabel!
    @IBOutlet private weak var totalSpendValueLabel: UILabel!
    @IBOutlet private weak var dividerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    private func setupView() {
        mainContainerView.makeAppBorderStyle(borderWidth: 1.5)
        avatarContainerView.makeAppBorderStyle(borderWidth: 1, conerRadius: 24)
        
        dividerView.backgroundColor = Colors.blue200
    }
    
//    func configureCell(viewModel: TopCustomerViewModel) {
//        shortcutCustomerNameLabel.text = viewModel.shortcutName
//        customerNameLabel.text = viewModel.name
//        totalSpendValueLabel.text = viewModel.totalSpend
//    }
//
    func updateAvatarBackground(for index: Int) {
        avatarContainerView.backgroundColor = avatarColors[index % avatarColors.count]
        bannerContainerView.backgroundColor = avatarColors[index % avatarColors.count].withAlphaComponent(0.25)
    }
}

//
//  ConnectedAppItemView.swift
//  SpaManagement
//
//  Created by Tuan Pham on 07/11/2021.
//

import UIKit
import Reusable

struct ConnectedAppItem {
    let title: String
    let connectedDescription: String
    let icon: UIImage?
    let mainColor: UIColor
}

class ConnectedAppItemView: UIView, NibOwnerLoadable {
    
    @IBOutlet weak var mainContainerView: DSDashedBorderView!
    @IBOutlet private weak var connectedIconContainerView: UIView!
    @IBOutlet private weak var connectedIconImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        loadNibContent()
        
        connectedIconContainerView.makeCornerRadidus(radius: 14)
        titleLabel.setStyle(DS.T14M(color: Colors.ink500))
        descriptionLabel.setStyle(DS.T12L(color: Colors.ink400))
    }
    
    private func updateMainColor(mainColor: UIColor) {
        
        connectedIconContainerView.backgroundColor = mainColor
        mainContainerView.backgroundColor = mainColor.withAlphaComponent(0.35)
    }
    
    func configureView(item: ConnectedAppItem) {
        updateMainColor(mainColor: item.mainColor)
        
        titleLabel.text = item.title
        descriptionLabel.text = item.connectedDescription
        connectedIconImageView.image = item.icon
    }
}

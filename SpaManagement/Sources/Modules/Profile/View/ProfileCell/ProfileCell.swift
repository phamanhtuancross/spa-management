//
//  ProfileCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 21/09/2021.
//

import UIKit
import Reusable

struct ProfileViewModel {
    let model: User
    
    var userName: String {
        return model.username
    }
    
    var phoneNumber: String {
        return model.phoneNumber
    }
    
    var shortcutName: String {
        guard let shortcutChar = model.username.split(separator: " ").last(where: { !$0.isEmpty })?.first else { return "--" }
        return String(shortcutChar).uppercased()
    }
}

class ProfileCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var avatarWrapView: UIView!
    @IBOutlet private weak var shopNameLabel: UILabel!
    @IBOutlet private weak var shopPhoneNumberLabel: UILabel!
    
    @IBOutlet private weak var shopShortcutNameLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarWrapView.makeCircular()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    private func setupView() {
        
        containerView.makeAppBorderStyle(conerRadius: 16)
        
        shopShortcutNameLabel.setStyle(DS.T16M(color: .white))
        
        shopNameLabel.setStyle(DS.T14M(color: Colors.ink500))
        shopPhoneNumberLabel.setStyle(DS.T14R(color: Colors.ink400s))
        
        avatarWrapView.backgroundColor = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
    }
    
    func configureCell(viewModel: ProfileViewModel) {
        shopNameLabel.text = viewModel.userName
        shopPhoneNumberLabel.text = viewModel.phoneNumber
        shopShortcutNameLabel.text = viewModel.shortcutName
    }
}

//
//  InAppPurchaseCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 09/10/2021.
//

import UIKit
import Reusable
import RxSwift

class InAppPurchaseCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var purchaseButton: UIButton!
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    private let disposeBag = DisposeBag()
    
    var didSelectPurchaseOption: VoidCallBack?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.makeAppBorderStyle(conerRadius: 16)
        
        titleLabel.text = L10n.Profile.Purchase.title
        descriptionLabel.text = L10n.Profile.Purchase.description
        
        purchaseButton.setTitle("", for: .normal)
        
        purchaseButton
            .rx.tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.didSelectPurchaseOption?()
            }
            .disposed(by: disposeBag)
    }
}

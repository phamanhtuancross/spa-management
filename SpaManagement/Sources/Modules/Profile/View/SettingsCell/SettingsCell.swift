//
//  SettingsCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 09/10/2021.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

class SettingsCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var changePasswordButton: UIButton!
    
    private let disposeBag = DisposeBag()
    
    var didTapChangePassword: VoidCallBack?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainContainerView.makeAppBorderStyle(conerRadius: 16)
        
        changePasswordButton
            .rx.tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.didTapChangePassword?()
            }
            .disposed(by: disposeBag)
    }
    
}

//
//  SupportsCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 09/10/2021.
//

import UIKit
import Reusable
import RxCocoa
import RxSwift

class SupportsCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var shadowView: UIView!
    
    @IBOutlet private weak var contactSupportButton: UIButton!
    @IBOutlet private weak var moreInfoAboutMeButton: UIButton!
    @IBOutlet private weak var rateAppButton: UIButton!
    @IBOutlet private weak var shareAppButton: UIButton!
    
    private let disposeBag = DisposeBag()
    
    public var didTapContactSupport: VoidCallBack?
    public var didTapMoreInfo: VoidCallBack?
    public var didTapRateApp: VoidCallBack?
    public var didTapShareApp: VoidCallBack?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
        setupActions()
    }
    
    private func setupViews() {
        mainContainerView.makeAppBorderStyle(conerRadius: 16)
    }
    
    private func setupActions() {
        contactSupportButton
            .rx.tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.didTapContactSupport?()
            }
            .disposed(by: disposeBag)
        
        moreInfoAboutMeButton
            .rx.tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.didTapMoreInfo?()
            }
            .disposed(by: disposeBag)
        
        rateAppButton
            .rx.tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.didTapRateApp?()
            }
            .disposed(by: disposeBag)
        
        shareAppButton
            .rx.tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.didTapShareApp?()
            }
            .disposed(by: disposeBag)
    }
}

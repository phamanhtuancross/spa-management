//
//  LogoutCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 21/09/2021.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

class LogoutCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var logOutButton: UIButton!
    private let disposeBag = DisposeBag()
    
    var didTap: VoidCallBack?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        
        logOutButton.makeAppBorderStyle()
        logOutButton
            .rx.tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.didTap?()
            }
            .disposed(by: disposeBag)
    }
    
}

//
//  PurchaseProductCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 10/10/2021.
//

import UIKit
import Reusable
import StoreKit
import SwiftUI
import RxSwift

struct PurchaseProductViewModel {
    let model: SKProduct
    let isPurchased:  Bool
    
    init(model: SKProduct, isPurchased:  Bool) {
        
        self.model = model
        self.isPurchased = isPurchased
        
        priceFormatter.locale = model.priceLocale
    }
    
   let priceFormatter: NumberFormatter = {
      let formatter = NumberFormatter()
      
      formatter.formatterBehavior = .behavior10_4
      formatter.numberStyle = .currency
      
      return formatter
    }()

    var title: String {
        return model.localizedTitle
    }
    
    var productDescription: String {
        return model.localizedDescription
    }
    
    var price: String {
        return priceFormatter.string(from: model.price).orEmpty
    }
    
    var isAvailablePurchase: Bool {
        return IAPHelper.canMakePayments()
    }
}

private let iconContainerColors: [UIColor] = [ #colorLiteral(red: 0, green: 0.9810667634, blue: 0.5736914277, alpha: 0.822481168), #colorLiteral(red: 0.4620226622, green: 0.8382837176, blue: 1, alpha: 1), #colorLiteral(red: 1, green: 0.2644898375, blue: 0.1355772473, alpha: 1) ]
private let icons: [UIImage?] = [
    UIImage(),
    UIImage(),
    UIImage()
//    UIImage(systemName: "calendar"),
//    UIImage(systemName: "heart.fill"),
//    UIImage(systemName: "hourglass.start")
]

class PurchaseProductCell: UITableViewCell, NibReusable {
    
    @IBOutlet private weak var iconContainerView: UIView!
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var derscriptionLabel: UILabel!
    @IBOutlet private weak var purchaseButton: UIButton!
    @IBOutlet private weak var dividerLineView: UIView!
    
    private var product: SKProduct?
    
    private let disposeBag = DisposeBag()
    
    var buyHandler: ((_ product: SKProduct) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }

    private func setupViews() {
        iconContainerView.makeAppBorderStyle()
        purchaseButton.makeAppBorderStyle(borderColor: Colors.primarya500)
        derscriptionLabel.setStyle(DS.T16R(color: Colors.ink400))
        dividerLineView.backgroundColor = Colors.blue200
        
        purchaseButton
            .rx.tap
            .subscribeNext { [weak self] in
                guard let self = self, let product = self.product else { return }
                self.buyHandler?(product)
            }
            .disposed(by: disposeBag)
    }
}

extension PurchaseProductCell {
    
    func configureCell(viewModel: PurchaseProductViewModel, indexPath: IndexPath) {
        
        product = viewModel.model
        
        titleLabel.text = viewModel.title
        derscriptionLabel.text = viewModel.productDescription
        
        let index: Int = indexPath.row % iconContainerColors.count
        iconContainerView.backgroundColor = iconContainerColors[index]
        iconImageView.image = icons[index]
        
        purchaseButton.setTitle(viewModel.price, for: .normal)
        
        if viewModel.isPurchased {
            accessoryType = .checkmark
            purchaseButton.isHidden = true
        } else {
            purchaseButton.isHidden = !viewModel.isAvailablePurchase
            accessoryType = .none
        }
    }
}

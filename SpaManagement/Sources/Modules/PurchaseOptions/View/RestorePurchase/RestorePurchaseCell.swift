//
//  RestorePurchaseCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 10/10/2021.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

class RestorePurchaseCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var iconContainerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var restoreButton: UIButton!
    private let disposeBag = DisposeBag()
    
    var restoreHandler: VoidCallBack?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews() {
        iconContainerView.makeAppBorderStyle()
        restoreButton.makeAppBorderStyle(borderColor: Colors.primarya500)
        
        restoreButton.rx.tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.restoreHandler?()
            }
            .disposed(by: disposeBag)
    }
}

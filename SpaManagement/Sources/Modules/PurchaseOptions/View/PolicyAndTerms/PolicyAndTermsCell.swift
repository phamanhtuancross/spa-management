//
//  PolicyAndTermsCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 10/10/2021.
//

import UIKit
import Reusable

class PolicyAndTermsCell: UITableViewCell, NibReusable {
    
    @IBOutlet private weak var subscriptionTermsLabel: UILabel!
    @IBOutlet private weak var firstTermLabel: UILabel!
    @IBOutlet private weak var secondTermLabel: UILabel!
    @IBOutlet private weak var thirthTermLabel: UILabel!
    
    var subscriptionTermLabels: [UILabel] {
        return [
            firstTermLabel,
            secondTermLabel,
            thirthTermLabel
        ]
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        subscriptionTermLabels.forEach {
            $0.setStyle(DS.T14R(color: Colors.ink400))
        }
    }
}

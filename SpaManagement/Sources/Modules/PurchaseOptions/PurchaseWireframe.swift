//
//  PurchaseWireframe.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 09/10/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class PurchaseWireframe: BaseWireframe {

    // MARK: - Private properties -

    private let storyboard = UIStoryboard(name: "Purchase", bundle: nil)

    // MARK: - Module setup -

    init() {
        let moduleViewController = storyboard.instantiateViewController(ofType: PurchaseViewController.self)
        super.init(viewController: moduleViewController)

        let interactor = PurchaseInteractor()
        let presenter = PurchasePresenter(view: moduleViewController, interactor: interactor, wireframe: self)
        moduleViewController.presenter = presenter
    }

}

// MARK: - Extensions -

extension PurchaseWireframe: PurchaseWireframeInterface {
}

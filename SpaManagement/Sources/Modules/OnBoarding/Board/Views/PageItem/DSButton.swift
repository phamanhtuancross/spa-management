//
//  DSButton.swift
//  SpaManagement
//
//  Created by Tuan Pham on 07/11/2021.
//

import UIKit

class DSAppPrimaryButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = Colors.appColor
        setTitleColor(.white, for: .normal)
        makeCornerRadidus(radius: 12)
        
        titleLabel?.setStyle(DS.T16M(color: .white))
    }
}

class DSAppSecondaryButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = .white
        setTitleColor(Colors.appColor, for: .normal)
        
        makeAppBorderStyle(borderWidth: 1, borderColor: Colors.appColor, conerRadius: 12)
    }
}


//
//  OnBoardingPageItemCell.swift
//  SpaManagement
//
//  Created by Tuan Pham on 07/11/2021.
//

import UIKit
import Reusable


struct OnBoardingPageItemViewModel {
    let title: String
    let pageDescription: String
    let image: UIImage?
    
    init(title: String, pageDescription: String, image: UIImage?) {
        self.title = title
        self.pageDescription = pageDescription
        self.image = image
    }
}

extension OnBoardingPageItemViewModel: Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return false
    }
}

class OnBoardingPageItemCell: UICollectionViewCell, NibReusable {

    @IBOutlet private weak var imageContainerView: UIView!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        imageContainerView.makeCornerRadidus(radius: 60.0)
        
        titleLabel.setStyle(DS.T24SM(color: Colors.ink500))
        descriptionLabel.setStyle(DS.T16R(color: Colors.purple300))
    }
    
    func configureCell(viewModel: OnBoardingPageItemViewModel) {
        imageView.image = viewModel.image
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.pageDescription
    }

}

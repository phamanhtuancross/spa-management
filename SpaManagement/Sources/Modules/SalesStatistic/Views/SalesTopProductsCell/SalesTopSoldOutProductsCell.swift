//
//  SalesTopSoldOutProductsCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 25/09/2021.
//

import UIKit
import Reusable

struct SalesTopSoldOutProductsViewModel {
    
}


class SalesTopSoldOutProductsCell: UITableViewCell, NibReusable {
    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var contentContainerView: UIView!
    
    @IBOutlet private weak var productImagContainerView: UIView!
    
    @IBOutlet weak var productNameLabel: UILabel!
    
    @IBOutlet weak var totalPriceValueLabel: UILabel!
    @IBOutlet weak var totalSoldOutLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {

        mainContainerView.setShadowStyle(.shadow4)
        contentContainerView.makeAppBorderStyle()
        
        productImagContainerView.makeAppBorderStyle()
        productNameLabel.setStyle(DS.T16M(color: Colors.ink500))
        totalSoldOutLabel.setStyle(DS.T16M(color: Colors.orange400))
        totalPriceValueLabel.setStyle(DS.T16R(color: Colors.green500))
    }
    
    func configureCell(viewModel: SalesTopSoldOutProductsViewModel) {
        
    }
    
}

//
//  SalesTopSoldOutProductsCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 25/09/2021.
//

import UIKit
import Reusable

struct ProductViewModel {
    let model: Product
    var total: Int = 1
    var isDiplayTotalSolDout = false
    
    var name: String {
        return model.name
    }
    
    var priceDisplayed: String {
        return model.price.toDouble.toVNDFormat()
    }
    
    var totalSoldOut: String {
        if isDiplayTotalSolDout {
            return "+\(total) đã bán"
        }
        
        return ""
    }
    
    var productImageURL: URL? {
        return URL(string: model.imageUrl.orEmpty)
    }
}

extension Int {
    var toDouble: Double {
        return Double(self)
    }
}



class ProductCell: UITableViewCell, NibReusable {
    
    private let backgroundColors: [UIColor] = [
        #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),#colorLiteral(red: 1, green: 0.1857388616, blue: 0.5733950138, alpha: 0.5476740149),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
    ]
    
    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var contentContainerView: UIView!
    
    @IBOutlet private weak var productImagContainerView: UIView!
    
    @IBOutlet private weak var productImageView: UIImageView!
    @IBOutlet private weak var productNameLabel: UILabel!
    
    @IBOutlet private weak var totalPriceValueLabel: UILabel!
    @IBOutlet private weak var totalSoldOutLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {

        contentContainerView.makeAppBorderStyle()
        
        productImagContainerView.makeAppBorderStyle()
        productNameLabel.setStyle(DS.T16M(color: Colors.ink500))
        totalSoldOutLabel.setStyle(DS.T16M(color: Colors.orange400))
        totalPriceValueLabel.setStyle(DS.T16R(color: Colors.green500))
    }
    
    func configureCell(viewModel: ProductViewModel, itemIndex: Int) {
        productNameLabel.text = viewModel.name
        totalPriceValueLabel.text = viewModel.priceDisplayed
        productImagContainerView.backgroundColor = backgroundColors[itemIndex % backgroundColors.count].withAlphaComponent(0.25)
        
        totalSoldOutLabel.text = viewModel.totalSoldOut
        totalSoldOutLabel.isHidden = viewModel.totalSoldOut.isEmpty
        
        if let url = viewModel.productImageURL {
            productImageView.kf.setImage(with: url, placeholder: nil, options: [.transition(.fade(0.15))])
        } else {
            productImageView.image = nil
        }
    }
}

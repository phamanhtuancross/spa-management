//
//  SalesChartsCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 25/09/2021.
//

import UIKit
import Reusable
import Charts
import SnapKit

struct SalesStatisticChartsViewModel {
    let style: Style
    let filteredDateType: FilteredDateType
    
    enum Style {
        case barChart(data: BarChartData?)
        case circleChart(data: PieChartData?)
        case lineChart(data: LineChartData?)
    }
}

class SalesChartsCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var contentContainerView: UIView!
    
    @IBOutlet private weak var contentHeightConstraint: NSLayoutConstraint!
    
    private lazy var barCharView = makeBarChardView()
    private lazy var lineChartView = makeLineChardView()
    private lazy var pieChartView = makePieChartView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    var charts: [UIView] {
        return [
            barCharView,
            lineChartView,
            pieChartView
        ]
    }
    
    private func setupView() {

        mainContainerView.setShadowStyle(.shadow4)
        contentContainerView.makeAppBorderStyle()
        
        contentContainerView.addSubview(barCharView)
        barCharView.snp.makeConstraints { $0.edges.equalToSuperview().inset(8) }
        
        contentContainerView.addSubview(lineChartView)
        lineChartView.snp.makeConstraints { $0.edges.equalToSuperview().inset(8) }
        
        contentContainerView.addSubview(pieChartView)
        pieChartView.snp.makeConstraints { $0.edges.equalToSuperview().inset(8) }
        
        contentHeightConstraint.constant = UIScreen.main.bounds.width * 9 / 16
    }
    
    func configureCell(viewModel: SalesStatisticChartsViewModel) {
        
        charts.forEach { $0.isHidden = true }
        switch viewModel.style {
        case .barChart(let data):
            barCharView.isHidden = false
            setupBarChardView(for: viewModel.filteredDateType, data: data)
            
        case .circleChart(let data):
            contentHeightConstraint.constant = UIScreen.main.bounds.width * 1.2
            pieChartView.isHidden = false
            setupPieChartView(for: viewModel.filteredDateType, data: data)
        
        case .lineChart(let data):
            lineChartView.isHidden = false
            setupLineChartView(for: viewModel.filteredDateType, data: data)
        }
    }
    
    private func makeBarChardView() -> BarChartView {
        let chartView = BarChartView()
        
        chartView.borderColor = .clear
        chartView.drawValueAboveBarEnabled = false
        chartView.drawBordersEnabled = false
        chartView.drawValueAboveBarEnabled = false
        chartView.drawGridBackgroundEnabled = false
        chartView.gridBackgroundColor = .clear
        chartView.animate(yAxisDuration: 2)
        chartView.rightAxis.enabled = false
        chartView.leftAxis.enabled = false
        chartView.chartDescription?.enabled = false
        
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.gridColor = .clear
        chartView.xAxis.labelTextColor = Colors.ink400s
        chartView.xAxis.labelFont = .systemFont(ofSize: 8, weight: .light)
        chartView.xAxis.wordWrapEnabled = true
        chartView.xAxis.axisLineColor = Colors.blue200
        chartView.xAxis.granularity = 1
        chartView.xAxis.labelCount = 31
        
        chartView.legend.enabled = false
        chartView.highlightFullBarEnabled = false
        
        chartView.scaleYEnabled = false
        chartView.scaleXEnabled = false
        chartView.pinchZoomEnabled = false
        chartView.doubleTapToZoomEnabled = false
        
        return chartView
    }
    
    
    private func makeLineChardView() -> LineChartView {
        let chartView = LineChartView()
        chartView.borderColor = .clear
        chartView.drawBordersEnabled = false
        chartView.drawGridBackgroundEnabled = false
        chartView.gridBackgroundColor = .white
        chartView.animate(yAxisDuration: 2)
        chartView.rightAxis.enabled = false
        chartView.leftAxis.enabled = false
        chartView.chartDescription?.enabled = false
        
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.gridColor = .clear
        chartView.xAxis.labelTextColor = Colors.ink400s
        chartView.xAxis.labelFont = .systemFont(ofSize: 8, weight: .light)
        chartView.xAxis.wordWrapEnabled = true
        chartView.xAxis.axisLineColor = Colors.blue200
        chartView.xAxis.granularity = 1
        chartView.xAxis.labelCount = 31
        
        chartView.scaleYEnabled = false
        chartView.scaleXEnabled = false
        chartView.pinchZoomEnabled = false
        chartView.doubleTapToZoomEnabled = false
        
        chartView.legend.enabled = false
        
        return chartView
    }
    
    private func makePieChartView() -> PieChartView {
        let chartView = PieChartView()
        
        chartView.entryLabelColor = Colors.ink500
        chartView.entryLabelFont = .systemFont(ofSize: 8, weight: .light)
        
        
        chartView.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
        return chartView
    }
    
    func setupBarChardView(for filteredDateType: FilteredDateType, data: BarChartData?) {
        
        barCharView.xAxis.valueFormatter = DateAxisValueFormatter(filterDateType: filteredDateType)
        barCharView.data = data
    }
    
    func setupLineChartView(for filteredDateType: FilteredDateType, data: LineChartData?) {
        
        lineChartView.xAxis.valueFormatter = DateAxisValueFormatter(filterDateType: filteredDateType)
        lineChartView.data = data
    }
    
    func setupPieChartView(for filteredDateType: FilteredDateType, data: PieChartData?) {
        
        pieChartView.data = data
    }
}

class DateAxisValueFormatter: IAxisValueFormatter {
    let filterDateType: FilteredDateType
    
    init(filterDateType: FilteredDateType) {
        self.filterDateType = filterDateType
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return filterDateType.columTitle(at: Int(value))
    }
}

class MoneyAxisValueFormatter: IValueFormatter {
    func stringForValue(_ value: Double,
                        entry: ChartDataEntry,
                        dataSetIndex: Int,
                        viewPortHandler: ViewPortHandler?) -> String {
        return value.toVNDFormat()
    }
}

extension FilteredDateType {
    func columTitle(at index: Int) -> String {
        switch self {
        case .thisWeek, .lasWeek:
            return TimesFormater.daysInWeek[index % TimesFormater.daysInWeek.count]
            
        case .thisMonth, .lastMonth:
            let dateRange = TimesFormater.daysOfWeekInMonth(date: startDate)
            return dateRange[index % dateRange.count]
            
        case .thisYear:
            return TimesFormater.monthsInYear[index % TimesFormater.monthsInYear.count]
            
        case .today, .yesterday:
            return self.startDate.toFormat("dd MMM")
            
        case .chooseARange:
            return ""
        }
    }
}

enum TimesFormater {
    static var daysInWeek: [String] {
        return [
            "Thứ 2",
            "Thứ 3",
            "Thứ 4",
            "Thứ 5",
            "Thứ 6",
            "Thứ 7",
            "Chủ nhật",
        ]
    }
    
    static var monthsInYear: [String] {
        return ["Jan", "Feb", "Mar",
                "Apr", "May", "Jun",
                "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec"]
    }
    
    static func daysOfWeekInMonth(date: Date) -> [String] {
        
        let month = date.toFormat("MMM")
        let totalDays = date.monthDays
        return [
            "1-7 \(month)",
            "8-14 \(month)",
            "15-21 \(month)",
            "22-28 \(month)",
            "28-\(totalDays) \(month)"
        ]
    }
    
    static var daysInMonth: [String] {
        return [1...31].map { "\($0)" }
    }
}

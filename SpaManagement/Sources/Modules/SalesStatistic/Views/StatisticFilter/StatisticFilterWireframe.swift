//
//  StatisticFilterWireframe.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 26/09/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class StatisticFilterWireframe: BaseWireframe {
    
    // MARK: - Private properties -
    
    private let storyboard = UIStoryboard(name: "StatisticFilter", bundle: nil)
    
    // MARK: - Module setup -
    
    init(filteredDateType: FilteredDateType,
         listener: StatisticFilterListener) {
        let moduleViewController = storyboard.instantiateViewController(ofType: StatisticFilterViewController.self)
        super.init(viewController: moduleViewController)
        
        let interactor = StatisticFilterInteractor()
        let presenter = StatisticFilterPresenter(view: moduleViewController,
                                                 interactor: interactor,
                                                 wireframe: self,
                                                 listener: listener,
                                                 filteredDateType: filteredDateType)
        moduleViewController.presenter = presenter
    }
    
}

// MARK: - Extensions -

extension StatisticFilterWireframe: StatisticFilterWireframeInterface {
}

//
//  SatisticFilterDateTypeCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 27/09/2021.
//

import UIKit
import Reusable

class FilterDateTypeCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var contentContainerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        contentContainerView.makeAppBorderStyle(borderWidth: 1.5,
                                                conerRadius: 16)
        titleLabel.setStyle(DS.T16R(color: Colors.ink400s))
    }
    
    func configureCell(viewModel: FilteredDateTypeViewModel) {
        titleLabel.text = viewModel.title
        
        if viewModel.isSelected {
            contentContainerView.backgroundColor = Colors.green500
            titleLabel.textColor = .white
        } else {
            contentContainerView.backgroundColor = .white
            titleLabel.textColor = Colors.ink400s
        }
    }
}

//
//  TimePeroidCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 03/10/2021.
//

import UIKit
import Reusable

class TimePeroidCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var leftIconImageView: UIImageView!
    
    @IBOutlet private weak var rightIconImageView: UIImageView!
    @IBOutlet private weak var rightIconWrapView: UIView!
    
    @IBOutlet private weak var timeTitleLabel: UILabel!
    @IBOutlet private weak var timeValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        timeTitleLabel.setStyle(DS.T16M(color: Colors.ink500))
        timeValueLabel.setStyle(DS.T14R(color: Colors.ink300s))
        
        rightIconImageView.image = Asset.Icon.Calendar.icCheckMark.image
    }
    
    func configureCell(viewModel: FilteredDateTypeViewModel) {
        
        rightIconWrapView.isHidden = !viewModel.isSelected
        
        timeTitleLabel.text = viewModel.title
        timeValueLabel.text = viewModel.timeDescription
        
        timeValueLabel.isHidden = viewModel.timeDescription.isEmpty
        
        
    
        if viewModel.isSelected {
            timeTitleLabel.textColor = Colors.green400
            leftIconImageView.image = viewModel.selectedIcon
        } else {
            timeTitleLabel.textColor = Colors.ink500
            leftIconImageView.image = viewModel.unSelectedIcon
        }
    }
}

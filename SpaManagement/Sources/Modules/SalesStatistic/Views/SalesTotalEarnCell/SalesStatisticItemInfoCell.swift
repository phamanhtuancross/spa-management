//
//  SalesStatisticTotalEarnCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 25/09/2021.
//

import UIKit
import Reusable

struct SalesStatisticItemInfoViewModel {
    let title: String
    let value: String
    let icon: UIImage?
    var iconBackgroundColor: UIColor = Colors.red400
}

class SalesStatisticItemInfoCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var contentContainerView: UIView!
    @IBOutlet private weak var iconImageContainerView: UIView!
    @IBOutlet private weak var valueLabel: UILabel!
    @IBOutlet private weak var diffValueLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var leftIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {

        mainContainerView.setShadowStyle(.shadow4)
        contentContainerView.makeAppBorderStyle()

        iconImageContainerView.makeBorder()
        iconImageContainerView.makeCornerRadidus(radius: 32)
        
        valueLabel.font = .systemFont(ofSize: 24, weight: .bold)
        valueLabel.textColor = Colors.ink500
        diffValueLabel.setStyle(DS.T16M(color: Colors.green500))
        titleLabel.setStyle(DS.T16R(color: Colors.ink400s))
        
        diffValueLabel.isHidden = true
    }
    
    func configureCell(viewModel: SalesStatisticItemInfoViewModel) {
        titleLabel.text = viewModel.title
        valueLabel.text = viewModel.value
        leftIconImageView.image = viewModel.icon
        iconImageContainerView.backgroundColor = viewModel.iconBackgroundColor
    }
}

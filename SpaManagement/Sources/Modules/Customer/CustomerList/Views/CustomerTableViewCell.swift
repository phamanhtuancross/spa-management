//
//  CustomerTableViewCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//

import UIKit
import Reusable
import Foundation

class CustomerTableViewCell: UITableViewCell, NibReusable {

    private let avatarColors: [UIColor] = [
        #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),#colorLiteral(red: 1, green: 0.1857388616, blue: 0.5733950138, alpha: 0.5476740149),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
    ]
    
    @IBOutlet private weak var avatarContainerView: UIView!
    @IBOutlet private weak var customerNameLabel: UILabel!
    @IBOutlet private weak var customerShortcutNameLabel: UILabel!
    @IBOutlet private weak var rankingProgressBar: UIProgressView!
    @IBOutlet private weak var rankDescriptionLabel: UILabel!
    //    @IBOutlet private weak var phoneNumberLabel: UILabel!
//    @IBOutlet private weak var customerTotalMoneySpendLabel: UILabel!
//    @IBOutlet private weak var customerLastDayJoinLabel: UILabel!

    override func layoutSubviews() {
        super.layoutSubviews()
        avatarContainerView.makeCircular()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        
        customerNameLabel.setStyle(DS.T16M(color: Colors.ink500))
//        customerLastDayJoinLabel.setStyle(DS.T14R(color: Colors.ink500))
        
//        customerTotalMoneySpendLabel.setStyle(DS.T14R(color: Colors.ink400s))
//        phoneNumberLabel.setStyle(DS.T14R(color: Colors.ink400s))
        customerShortcutNameLabel.setStyle(DS.T30M(color: UIColor.white))
        
        rankingProgressBar.layer.cornerRadius = 4.0
        rankingProgressBar.layer.masksToBounds = true
        
        rankingProgressBar.progressTintColor = Colors.yellow
        rankingProgressBar.trackTintColor = Colors.blue200s
        rankDescriptionLabel.setStyle(DS.T12L(color: Colors.ink400s))
        
    }
}

extension CustomerTableViewCell {
    func updateAvatar(by index: Int) {
        avatarContainerView.backgroundColor = avatarColors[index % avatarColors.count].withAlphaComponent(0.75)
    }
    
    func configureCell(viewModel: CustomerViewModel) {
        customerNameLabel.text = viewModel.name
        customerShortcutNameLabel.text = viewModel.shortCutName
//        phoneNumberLabel.text = viewModel.phoneNumber
//        customerTotalMoneySpendLabel.text = viewModel.totalSpend
//        customerLastDayJoinLabel.text = viewModel.joinAt
    }
}

extension UIView {
    func makeCircular() {
        layer.cornerRadius = bounds.height / 2
        layer.masksToBounds = true
    }
}


import SwiftDate

struct CustomerViewModel {
    let model: Customer
    
    var shortCutName: String {
        return String(model.name.split(separator: " ").last(where: { !$0.isEmpty }).or([]).first.or(Character("X")))
    }
    
    var name: String {
        return model.name
    }
    
    var totalSpend: String {
        return model.totalSpend.toDouble.toVNDFormat()
    }
    
    var phoneNumber: String {
        return model.phoneNumber
    }
    
    var email: String {
        return model.email.or("")
    }
    
    var joinAt: String {
        let date = Date(timeIntervalSince1970: TimeInterval(model.createdAt))
            .toFormat("dd-MMM-yyyy",
                      locale: Locale(identifier: "vi"))
        return date
    }
    
    var dob: String {
        return Date(timeIntervalSince1970: TimeInterval(model.dateOfBirth)).toVNDateFormated()
    }
}

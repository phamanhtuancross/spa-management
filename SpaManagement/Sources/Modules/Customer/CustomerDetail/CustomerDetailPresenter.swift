//
//  CustomerDetailPresenter.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import Foundation
import RxSwift
import RxCocoa
import Action
import CloudKit
    
enum CustomerDetailNavigationOption {
    case addPayment
    case tranctionList
    case transactionDetail(transaction: Transaction)
    case calendar(calendar: Event?)
    case eventDetail(event: Event)
}
protocol CustomerDetailWireframeInterface: WireframeInterface {
    func navigate(to option: CustomerDetailNavigationOption)
    func presentCustomerEditableForm()
    func detachCustomerEditableForm(completion: VoidCallBack?)
    
    func detachCalendarEditable(completion: VoidCallBack?)
}

protocol CustomerDetailViewInterface: ViewInterface {
    var cellTypes: BehaviorRelay<[CustomerDetailCellType]> { get }
}

final class CustomerDetailPresenter: CustomerDetailPresenterInterface {
    
    // MARK: - Private properties -
    
    private unowned let view: CustomerDetailViewInterface
    private let interactor: CustomerDetailInteractorInterface
    private let wireframe: CustomerDetailWireframeInterface
    private let customer: Customer
    
    let addPaymentTrigger = PublishRelay<Void>()
    let viewTransactionsTrigger = PublishRelay<Void>()
    let editCustomerTrigger = PublishRelay<Void>()
    let viewTransactionDetailTrigger = PublishRelay<TransactionViewModel>()
    
    let selectedEventTrigger = PublishRelay<Event>()
    let addCalendarTrigger = PublishRelay<Void>()
    let deleteCalendarTrigger = PublishRelay<Event>()
    
    private lazy var getTransactionsAction = makeGetTransactionsAction()
    private lazy var getEventsAction = makeGetEventsAction()
    private lazy var deleteCalendarAction = makeDeleteCalendarAction()
    
    private let disposeBag = DisposeBag()
    
    // MARK: - Lifecycle -
    
    init(
        view: CustomerDetailViewInterface,
        interactor: CustomerDetailInteractorInterface,
        wireframe: CustomerDetailWireframeInterface,
        customer: Customer
    ) {
        self.view = view
        self.interactor = interactor
        self.wireframe = wireframe
        self.customer = customer
        
        configurePresenter()
        configureViewActions()
    }
    
    private func configurePresenter() {
        let transactionsRelay = BehaviorRelay<[Transaction]?>(value: nil)
        let eventsRelay = BehaviorRelay<[Event]?>(value: nil)
        
        Observable.combineLatest(
                transactionsRelay,
                eventsRelay
            )
            .map { [weak self] transactions, events -> CustomerDetailDataMaker? in
                guard let self = self else { return nil }
                return CustomerDetailDataMaker(customer: self.customer,
                                               transactions: transactions,
                                               events: events)
            }
            .filterNil()
            .map { $0.makeCellTypes() }
            .bind(to: view.cellTypes)
            .disposed(by: disposeBag)
        
        getTransactionsAction
            .elements
            .bind(to: transactionsRelay)
            .disposed(by: disposeBag)
        
        getEventsAction
            .elements
            .bind(to: eventsRelay)
            .disposed(by: disposeBag)
        
        deleteCalendarAction
            .elements
            .map { deletedCalendar -> [Event]? in
                guard let calendars = eventsRelay.value
                else { return nil }
                
                return calendars.filter { $0.id != deletedCalendar.id }
            }
            .bind(to: eventsRelay)
            .disposed(by: disposeBag)
        
        Observable.merge(
                getTransactionsAction.underlyingError,
                getEventsAction.underlyingError
            )
            .subscribeNext { error in
                print(error)
            }
            .disposed(by: disposeBag)
        
        getEventsAction.execute()
        getTransactionsAction.execute()
    }
    
    private func configureViewActions() {
        addPaymentTrigger.subscribeNext { [weak wireframe] _ in
            guard let wireframe = wireframe else { return }
            wireframe.navigate(to: .addPayment)
        }
        .disposed(by: disposeBag)
        
        viewTransactionsTrigger
            .subscribeNext { [weak wireframe] in
                guard let wireframe = wireframe else { return }
                wireframe.navigate(to: .tranctionList)
            }
            .disposed(by: disposeBag)
        
        editCustomerTrigger
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.wireframe.presentCustomerEditableForm()
            }
            .disposed(by: disposeBag)
        
        viewTransactionDetailTrigger
            .map { $0.model }
            .subscribeNext { [weak self] transaction in
                guard let self = self else { return }
                self.wireframe.navigate(to: .transactionDetail(transaction: transaction))
            }
            .disposed(by: disposeBag)
        
        addCalendarTrigger
            .subscribeNext { [weak wireframe] in
                guard let wireframe = wireframe else { return }
                wireframe.navigate(to: .calendar(calendar: nil))
            }
            .disposed(by: disposeBag)
        
        deleteCalendarTrigger
            .subscribeNext { [weak self] appCalendar in
                guard let self = self else { return }
                self.deleteCalendarAction.execute(appCalendar.id)
            }
            .disposed(by: disposeBag)
        
        selectedEventTrigger
            .subscribeNext { [weak wireframe] event in
                guard let wireframe = wireframe else { return }
                wireframe.navigate(to: .eventDetail(event: event))
            }
            .disposed(by: disposeBag)
    }
    
    private func reloadData() {
        let cellTypes = CustomerDetailDataMaker(customer: customer, transactions: nil, events: nil).makeCellTypes()
        
        getEventsAction.execute()
        getTransactionsAction.execute()
        
        view.cellTypes.accept(cellTypes)
    }
}

// MARK: - Extensions -

extension CustomerDetailPresenter {
    private func makeGetTransactionsAction() -> Action<Void, [Transaction]> {
        return .init { [weak self] _ in
            guard let self = self else { return .empty() }
            return self.interactor.getTransactions()
        }
    }
    
    private func makeGetEventsAction() -> Action<Void, [Event]> {
        return .init { [weak self] _ in
            guard let self = self else { return .empty() }
            return self.interactor.getCalendars()
        }
    }
    
    private func makeDeleteCalendarAction() -> Action<String, Event> {
        return .init { [weak self] id in
            guard let self = self else { return .empty() }
            return self.interactor.deleteCalendar(id: id)
        }
    }
}

extension CustomerDetailPresenter: CustomerEditableFormListener {
    func customerEditableForm(receviedData: Customer) {
        wireframe.detachCustomerEditableForm { [weak self] in
            guard let self = self else { return }
            self.reloadData()
        }
    }
}

extension CustomerDetailPresenter: CalendarEditableListner {
    func calendarEditable(didCreatedCalendar calendar: Event) {
        
        wireframe.detachCalendarEditable { [weak self] in
            guard let self = self else { return }
            self.reloadData()
        }
    }
}

struct CustomerDetailDataMaker {
    let customer: Customer
    let transactions: [Transaction]?
    let events: [Event]?
    
    func makeCellTypes() -> [CustomerDetailCellType] {
        return makeHeaderCells() + makeCalendarsCells() + makeTransactionCells()
    }
    
    private func makeHeaderCells() -> [CustomerDetailCellType] {
        return [.header(viewModel: .init(model: customer))]
    }
    
    private func makeCalendarsCells() -> [CustomerDetailCellType] {
        return [.sectionHeader(title: "Lịch hẹn"),  .events(events: events.or([]))]
    }
    
    private func makeTransactionCells() -> [CustomerDetailCellType] {
        
        var cells: [CustomerDetailCellType] = []
        
        guard let transactions = transactions else {
            cells.append(.line)
            cells.append(.transactionsLoading)
            return cells
        }
        
        if transactions.isNotEmpty {
            cells.append(.line)
            cells.append(.sectionHeader(title: "Giao dịch gần đây"))
            cells += transactions.map { .transaction(viewModel: .init(model: $0))}
        }
        
        return cells
    }
}

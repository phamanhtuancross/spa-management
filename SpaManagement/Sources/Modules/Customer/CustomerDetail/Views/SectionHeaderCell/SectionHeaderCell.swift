//
//  SectionHeaderCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//

import UIKit
import Reusable

class SectionHeaderCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        titleLabel.font = .systemFont(ofSize: 20, weight: .medium)
        titleLabel.textColor = Colors.ink400s
    }
    
    func configureCell(title: String) {
        titleLabel.text = title
    }
}

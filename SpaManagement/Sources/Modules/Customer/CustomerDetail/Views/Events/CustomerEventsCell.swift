//
//  CustomerEventsCell.swift
//  SpaManagement
//
//  Created by Tuan Pham on 10/11/2021.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa
import RxDataSources

class CustomerEventsCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    private lazy var dataSource = makeDataSource()
    private var eventViewModels = PublishRelay<[EventItemViewModel]>()
    private let disposeBag = DisposeBag()
    
    var didSelectedEvent: ((Event) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureViews()
        bindingData()
    }
    
    private func configureViews() {
        configureTableView()
    }
    
    private func configureTableView() {
        registerCells()
        handleSelection()
    }
    
    private func handleSelection() {
        tableView.rx.modelSelected(EventItemViewModel.self)
            .subscribeNext { [weak self] viewModel in
                guard let self = self else { return }
                self.didSelectedEvent?(viewModel.model)
            }
            .disposed(by: disposeBag)
    }
    
    private func registerCells() {
        tableView.register(cellType: EventCell.self)
    }
    
    private func bindingData() {
        eventViewModels
            .map { [.init(model: .zero, items: $0)] }
            .asDriver(onErrorDriveWith: .never())
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
    
    func configureCell(events: [Event]) {
        tableViewHeightConstraint.constant = CGFloat(events.count) * UIConfig.cellHeight
        eventViewModels.accept(events.map { .init(model: $0)})
    }
    
}

extension CustomerEventsCell {
    typealias Section = SectionModel<Int, EventItemViewModel>
    typealias DataSource = RxTableViewSectionedReloadDataSource<Section>
    
    private func makeDataSource() -> DataSource {
        return .init { _, tableView, indexPath, viewModel in
            let cell: EventCell = tableView.dequeueReusableCell(for: indexPath)
            cell.configureCell(viewModel: viewModel)
            return cell
        }
    }
}

extension CustomerEventsCell {
    enum UIConfig {
        static let cellHeight: CGFloat = 148.0
    }
}

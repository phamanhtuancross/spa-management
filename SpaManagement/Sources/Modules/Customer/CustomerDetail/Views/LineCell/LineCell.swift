//
//  LineCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 20/09/2021.
//

import UIKit
import Reusable

class LineCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
        
        containerView.backgroundColor = Colors.backgroundDark
    }
    
    private func setupView() {}
}

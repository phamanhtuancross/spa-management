//
//  StatsInfoView.swift
//  SpaManagement
//
//  Created by Tuan Pham on 10/11/2021.
//

import UIKit
import Reusable

class StatsInfoView: UIView, NibOwnerLoadable {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        loadNibContent()
    }

}

//
//  CustomerDetailHeaderCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//

import UIKit
import Reusable

class CustomerDetailHeaderCell: UITableViewCell, NibReusable {

    @IBOutlet weak var contentShadowView: UIView!
    @IBOutlet weak var contentWrapView: UIView!
    @IBOutlet weak var editProfileContainerView: UIView!
    //
//    @IBOutlet private weak var rankIconImageView: UIImageView!
//    @IBOutlet private weak var rankTitleLabel: UILabel!
//    @IBOutlet private weak var rankLabel: UILabel!
//
//    @IBOutlet private weak var avatarContainerView: DSDashedBorderView!
//    @IBOutlet private weak var nameLabel: UILabel!
//    @IBOutlet private weak var mobilePhoneLabel: UILabel!
//
//    @IBOutlet private weak var totalBillContainerView: UIView!
//    @IBOutlet private weak var totalBillTitleLabel: UILabel!
//    @IBOutlet private weak var totalBillLabel: UILabel!
    
    var isLayoutFirstTime: Bool = true
//
//    var allTitleLabels: [UILabel] {
//        return [
//            rankTitleLabel,
//            totalBillTitleLabel
//        ]
//    }
//
//    var allValueLabels: [UILabel] {
//        return [
//            rankLabel,
//            nameLabel,
//            mobilePhoneLabel,
//            totalBillLabel
//        ]
//    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentWrapView.roundCorners([.topLeft, .topRight], cornerRadius: 32)
        contentShadowView.setShadowStyle(.shadow4Revert)
        
        editProfileContainerView.makeCircular()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
//        customerInfoCotentWrapView.makeBorder()
//
//        allTitleLabels.forEach {
//            $0.setStyle(DS.T12R(color: #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)))
//        }
//
//        allValueLabels.forEach {
//            $0.font = .systemFont(ofSize: 20, weight: .bold)
//        }
//
//        mobilePhoneLabel.setStyle(DS.T16R(color: Colors.ink400s))
//        nameLabel.setStyle(DS.T16M(color: Colors.ink500))
//
//        rankTitleLabel.text = "Hạng"
//        rankIconImageView.image = Asset.Icon.Ranking.icRankGold.image
//
//        totalBillContainerView.makeAppBorderStyle()
//        totalBillContainerView.backgroundColor = Colors.green200s
//        totalBillLabel.textColor = Colors.green500
//        totalBillTitleLabel.text = "Tổng hoá đơn"
    }
    
    func configureCell(viewModel: CustomerHeaderViewModel) {
//        rankLabel.text = viewModel.rankTitle
//        nameLabel.text = viewModel.name.uppercased()
//        mobilePhoneLabel.text = viewModel.phoneNumber.uppercased()
//        totalBillLabel.text = viewModel.totalSpend
    }
}

struct CustomerHeaderViewModel {
    let model: Customer
    
    var name: String {
        return model.name
    }
    
    var phoneNumber: String {
        return model.phoneNumber
    }
    
    var totalSpend: String {
        return "\(Double(model.totalSpend).toVNDFormat())"
    }
    
    var rankTitle: String {
        switch model.rankType {
        case .bronze:
            return "Hạng Đồng"
            
        case .silver:
            return "Hạng Bạc"
            
        case .gold:
            return "Hạng Vàng"
        }
    }
}

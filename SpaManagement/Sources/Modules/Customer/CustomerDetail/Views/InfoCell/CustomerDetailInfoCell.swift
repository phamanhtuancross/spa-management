//
//  CustomerDetailInfoCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//

import UIKit
import Reusable

struct CustomerDetailInfoModel {
    let title: String
    let infoDescription: String
    let icon: UIImage?
}

class CustomerDetailInfoCell: UITableViewCell, NibReusable {
    @IBOutlet private weak var leftIconImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet weak var dividerLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        titleLabel.setStyle(DS.T16R(color: Colors.ink400s))
        descriptionLabel.setStyle(DS.T16R(color: Colors.ink500))
        dividerLineView.backgroundColor = Colors.blue200s
    }
}

extension CustomerDetailInfoCell {
    func configureCell(viewModel: CustomerDetailInfoModel) {
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.infoDescription
        leftIconImageView.image = viewModel.icon
    }
 }

//
//  EmptyCalendarCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 07/10/2021.
//

import UIKit
import Reusable

class EmptyCalendarCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var mainContainerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainContainerView.makeAppBorderStyle()
    }

}

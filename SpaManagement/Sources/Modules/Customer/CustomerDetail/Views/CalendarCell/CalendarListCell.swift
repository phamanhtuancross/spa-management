//
//  CalendarListCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//

import UIKit
import Reusable
import RxDataSources
import RxSwift
import RxCocoa

struct CalendarListViewModel {
    let calendars: [Event]?
    
    var cellTypes: [CalendarListCellType] {
        
        guard let calendars = calendars else {
            return []
        }
        
        if calendars.isEmpty {
            return [
                .empty,
                .addCalendar
            ]
        }
        
        
        return calendars.map { .calendar(viewModel: .init(model: $0)) } + [.addCalendar]
    }
}

enum CalendarListCellType {
    case empty
    case calendar(viewModel: CalendarItemModel)
    case addCalendar
}

class CalendarListCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private lazy var dataSource = makeDataSource()
    private let cellTypes = BehaviorRelay<[CalendarListCellType]>(value: [])
    private let disposeBag = DisposeBag()
    
    var didTapAddCalendarAction: VoidCallBack?
    var didTapDeleteCalendar: ((Event) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCollectionView()
    }
    
    private func configureCollectionView() {

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        
        collectionView.register(cellType: CalendarCell.self)
        collectionView.register(cellType: AddCalendarCell.self)
        collectionView.register(cellType: EmptyCalendarCell.self)
        collectionView.rx.setDelegate(self).disposed(by: disposeBag)
        
        collectionView.contentInset = .init(top: .zero, left: 16, bottom: .zero, right: 16)
        loadingView.showAnimatedGradientSkeleton()
        
        cellTypes
            .filterEmpty()
            .map { [.init(model: 0, items: $0)]}
            .asDriver(onErrorDriveWith: .never())
            .doOnNext{ [weak self] _ in
                guard let self = self else { return }
                self.hideSkeletonView()
            }
            .drive(collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
    
    private func hideSkeletonView() {
        UIView.animate(withDuration: 0.25) { [weak self] in
            guard let self = self else { return }
            self.loadingView.alpha = 0
             
        } completion: { [weak self] finished in
            guard let self = self else { return }
            guard finished else { return }
            self.loadingView.isHidden = true
        }
    }
    
    func configureCell(viewModel: CalendarListViewModel) {
        cellTypes.accept(viewModel.cellTypes)
    }
}

extension CalendarListCell {
    typealias Section = SectionModel<Int,CalendarListCellType>
    typealias DataSource = RxCollectionViewSectionedReloadDataSource<Section>
}

extension CalendarListCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellType = dataSource.sectionModels[indexPath.section].items[indexPath.item]
        switch cellType {
        case .addCalendar:
            return .init(width: 196, height: 196)
        default:
            return .init(width: 270, height: 196)
        }
    }
}

extension CalendarListCell {
    func makeDataSource() -> DataSource {
        return .init { [weak self] _, collectionView, indexPath, cellType in
            
            switch cellType {
            case .empty:
                let cell: EmptyCalendarCell = collectionView.dequeueReusableCell(for: indexPath)
                return cell
                
            case .calendar(let viewModel):
                let cell: CalendarCell = collectionView.dequeueReusableCell(for: indexPath)
                cell.onCancelAction = { [weak self] in
                    guard let self = self else { return }
                    self.didTapDeleteCalendar?(viewModel.model)
                }
                cell.updateBackgroundColor(at: indexPath.row)
                cell.configureCell(viewModel: viewModel)
                return cell
                
            case .addCalendar:
                let cell: AddCalendarCell = collectionView.dequeueReusableCell(for: indexPath)
                cell.didTapAction = { [weak self] in
                    guard let self = self else { return }
                    self.didTapAddCalendarAction?()
                }
                return cell
            }
        }
    }
}

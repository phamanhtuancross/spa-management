//
//  CalendarCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//

import UIKit
import Reusable

class CalendarCell: UICollectionViewCell, NibReusable {

    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var calendarView: CalendarItemView!
    
    var onCancelAction: VoidCallBack?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        shadowView.setShadowStyle(.shadow8)
        
        calendarView.onCancelAction = { [weak self] in
            guard let self = self else { return }
            self.onCancelAction?()
        }
    }
    
    func updateBackgroundColor(at index: Int) {
        calendarView.updateBackgroundColor(for: index)
    }

    func configureCell(viewModel: CalendarItemModel) {
        calendarView.configureView(viewModel: viewModel)
    }
}

//
//  AddCalendarCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 07/10/2021.
//

import UIKit
import Reusable
import RxCocoa
import RxSwift

class AddCalendarCell: UICollectionViewCell, NibReusable {

    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var addCalendarButton: UIButton!
    
    private let disposeBag = DisposeBag()
    
    var didTapAction: VoidCallBack?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainContainerView.makeAppBorderStyle()
        shadowView.setShadowStyle(.shadow8)
        
        addCalendarButton
            .rx.tap
            .subscribeNext { [weak self] _ in
                guard let self = self else { return }
                self.didTapAction?()
            }
            .disposed(by: disposeBag)
    }

}

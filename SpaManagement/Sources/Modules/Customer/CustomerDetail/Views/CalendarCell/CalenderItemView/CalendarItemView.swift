//
//  CalendarItemView.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 14/09/2021.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

fileprivate let backgroundColors: [UIColor] = [
    #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),#colorLiteral(red: 1, green: 0.1857388616, blue: 0.5733950138, alpha: 0.5476740149),#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
]
class CalendarItemView: UIView, NibOwnerLoadable {
    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var happenAtLabel: UILabel!
    @IBOutlet private weak var availableInLabel: UILabel!
    @IBOutlet private weak var gradientView: DSGradientView!
    @IBOutlet private weak var cancelButton: UIButton!
    
    private let disposeBag = DisposeBag()
    private var isUpdatedGradientLayer = false
    
    var onCancelAction: VoidCallBack?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }

    private func setupView() {
        loadNibContent()
        
        mainContainerView.layer.cornerRadius = 8
        mainContainerView.layer.masksToBounds = true
        
        mainContainerView.layer.borderWidth = 1.0
        mainContainerView.layer.borderColor = Colors.blue200.cgColor
        
        titleLabel.setStyle(DS.T24M(color: Colors.white500))
        cancelButton.makeAppBorderStyle()
        
        cancelButton
            .rx.tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.onCancelAction?()
            }
            .disposed(by: disposeBag)
    }
    
    func updateBackgroundColor(for index: Int) {
        
        guard !isUpdatedGradientLayer else { return }
        defer { isUpdatedGradientLayer = true }
        let color = backgroundColors[index % backgroundColors.count]
        gradientView.gradientConfig = .init(colors: [color, color.withAlphaComponent(0.1)],
                                            direction: .topToBottom)
    }
    
    func configureView(viewModel: CalendarItemModel) {
        titleLabel.text = viewModel.title
        happenAtLabel.text = viewModel.happenAt
        availableInLabel.text = viewModel.availableIn
        
    }
}

import SwiftDate
struct CalendarItemModel {
    let model: Event
    
    var title: String {
        return model.description
    }
    
    var happenAt: String {
        return Date(timeIntervalSince1970: TimeInterval(model.happenAt))
            .toFormat("dd-MMM-yyyy",
                      locale: Locale(identifier: "vi"))
    }
    
    var availableIn: String {
        let days: Int = (model.happenAt - Int(Date().timeIntervalSince1970)) / (60 * 60 * 24)
        
        if days == 0 {
            return "Đến hẹn hôm nay"
        }
        
        if days < 0 {
            return "Đã diễn ra"
        }
        
        return "Còn \(days) ngày nữa"
        
    }
}

extension Date {
    func toVNDateFormated() -> String {
        return self.toFormat("dd-MMM-yyyy",
                  locale: Locale(identifier: "vi"))
    }
}

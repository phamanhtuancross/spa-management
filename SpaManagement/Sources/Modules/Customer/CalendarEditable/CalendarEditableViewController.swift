//
//  CalendarEditableViewController.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 08/10/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit
import PanModal
import RxSwift
import RxCocoa
import FSCalendar

struct CreateEventRequest {
    let calendarDescription: String
    let happenAt: Int
}

protocol CalendarEditablePresenterInterface: PresenterInterface {
    var createCalendarTrigger: PublishRelay<CreateEventRequest> { get }
}

final class CalendarEditableViewController: UIViewController, PanModalPresentable {

    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var calendarView: FSCalendar!
    @IBOutlet private weak var descriptionTitleLabel: UILabel!
    @IBOutlet private weak var descriptionTextView: UITextView!
    // MARK: - Public properties -

    @IBOutlet private weak var doneButton: UIButton!
    
    private let selectedDateRelay = BehaviorRelay<Date?>(value: nil)
    private let calendarDescriptionRelay = BehaviorRelay<String>(value: "")
    
    private let disposeBag = DisposeBag()
    var createRequest: CreateEventRequest?
    var presenter: CalendarEditablePresenterInterface!

    // MARK: - Lifecycle -
    var panScrollable: UIScrollView? {
        return scrollView
    }
    
    var shortFormHeight: PanModalHeight {
        return .maxHeightWithTopInset(120)
    }
    
    var longFormHeight: PanModalHeight {
        return shortFormHeight
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        defer {
            presenter.viewDidLoad()
        }
        configureView()
        bindingData()
    }

    private func configureView() {
        descriptionTextView.setStyle(DS.T16R(color: Colors.ink500))
        descriptionTextView.makeAppBorderStyle()
        doneButton.makeAppBorderStyle()
        
        calendarView.delegate = self
        calendarView.select(Date.trueTime)
        selectedDateRelay.accept(Date.trueTime)
        
        descriptionTextView
            .rx
            .text
            .orEmpty
            .bind(to: calendarDescriptionRelay)
            .disposed(by: disposeBag)
        
        let isEnableDoneButtonRelay = Observable.combineLatest(
                calendarDescriptionRelay,
                selectedDateRelay.asObservable()
            )
            .doOnNext{ [weak self] calendarDescription, selectedDate in
                guard let self = self else { return }
                self.createRequest = .init(calendarDescription: calendarDescription,
                                           happenAt: Int((selectedDate?.timeIntervalSince1970).or(.zero)))
            }
            .map { $0.isNotEmpty && $0.contains(where: { $0 != " " }) && $1 != nil }
            .share(replay: 1, scope: .whileConnected)
        
        isEnableDoneButtonRelay
            .subscribeNext { [weak self] isEnable in
                guard let self = self else { return }
                if isEnable {
                    self.doneButton.backgroundColor = Colors.green500
                } else {
                    self.doneButton.backgroundColor = Colors.ink300s
                }
            }
            .disposed(by: disposeBag)
        
        doneButton
            .rx.tap
            .takeLatestWhen(isEnableDoneButtonRelay.asObservable())
            .subscribeNext { [weak self] in
                guard let self = self,
                      let createRequest = self.createRequest
                else { return }
                self.presenter.createCalendarTrigger.accept(createRequest)
            }
            .disposed(by: disposeBag)
    }
    
    private func bindingData() {}
}

// MARK: - Extensions -

extension CalendarEditableViewController: CalendarEditableViewInterface {
}

extension CalendarEditableViewController: FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        selectedDateRelay.accept(date)
    }
}

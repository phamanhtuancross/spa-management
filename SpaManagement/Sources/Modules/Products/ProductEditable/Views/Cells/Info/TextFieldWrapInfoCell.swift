//
//  TextFieldWrapInfoCell.swift
//  SpaManagement
//
//  Created by Tuan Pham on 19/11/2021.
//

import UIKit
import Reusable

class TextFieldWrapInfoCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var infoTextField: DSInfoTextField!
    @IBOutlet private weak var infoTextViewContainer: UIView!
    @IBOutlet private weak var infoTextView: UITextView!
    
    private(set) var style: Style = .textField(style: .normal) {
        didSet {
            updateStyle()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        infoTextViewContainer.makeAppBorderStyle(borderWidth: 0.5, borderColor: #colorLiteral(red: 0.7215686275, green: 0.7215686275, blue: 0.8235294118, alpha: 1), conerRadius: 12)
    }
    
    private func updateStyle() {
        switch style {
        case .textField(let textFieldStyle):
            infoTextField.isHidden = false
            infoTextViewContainer.isHidden = true
            
        case .textView:
            infoTextField.isHidden = true
            infoTextViewContainer.isHidden = false
        }
    }
}

extension TextFieldWrapInfoCell {
    func updateStyle(_ style: Style) {
        self.style = style
    }
}

extension TextFieldWrapInfoCell {
    enum Style {
        case textField(style: TextFieldStyle)
        case textView
    }
    
    enum TextFieldStyle {
        case normal
        case currency
        case number
    }
}

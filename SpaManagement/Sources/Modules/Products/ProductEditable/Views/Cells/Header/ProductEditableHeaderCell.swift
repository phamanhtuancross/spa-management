//
//  ProductEditableHeaderCell.swift
//  SpaManagement
//
//  Created by Tuan Pham on 18/11/2021.
//

import UIKit
import Reusable

class ProductEditableHeaderCell: UITableViewCell, NibReusable {

    @IBOutlet weak var contentWrapView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
    }
}

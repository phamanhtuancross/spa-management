//
//  ChangePasswordRequest.swift
//  SpaManagement
//
//  Created by Tuan Pham on 16/10/2021.
//

import Foundation

struct ChangePasswordRequest {
    
    let oldPassword: String
    let newPassword: String
    let confirmNewPassword: String
    
    func validate() throws -> Bool {
        
        guard oldPassword.isNotEmpty, oldPassword.isValidPassword() else {
            throw ChangePasswordValidatorError.invalidOldPassword
        }
        
        guard newPassword.isNotEmpty, newPassword.isValidPassword() else {
            throw ChangePasswordValidatorError.invalidNewPassword
        }
        
        guard newPassword != oldPassword else {
            throw ChangePasswordValidatorError.newPasswordAreTheSameWithOldPassword
        }
        
        guard newPassword == confirmNewPassword else {
            throw ChangePasswordValidatorError.confirmPasswordNotCorrect
        }
        
        return true
    }
}

enum ChangePasswordValidatorError: Error {
    case confirmPasswordNotCorrect
    case invalidOldPassword
    case invalidNewPassword
    case newPasswordAreTheSameWithOldPassword
}

//
//  SignUpInteractor.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 22/09/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import Foundation
import Resolver
import RxSwift

protocol SignUpInteractorInterface: InteractorInterface {
    func signUp(request: SignUpRequest) -> Observable<User>
}

final class SignUpInteractor {
    @LazyInjected var userService: UserServiceType
}

// MARK: - Extensions -

extension SignUpInteractor: SignUpInteractorInterface {
    func signUp(request: SignUpRequest) -> Observable<User> {
        return userService.signUp(username: request.username, phoneNumber: request.phoneNumber, password: request.password)
    }
}

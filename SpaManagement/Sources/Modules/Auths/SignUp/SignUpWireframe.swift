//
//  SignUpWireframe.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 22/09/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class SignUpWireframe: BaseWireframe {

    // MARK: - Private properties -

    private let storyboard = UIStoryboard(name: "SignUp", bundle: nil)

    // MARK: - Module setup -

    init() {
        let moduleViewController = storyboard.instantiateViewController(ofType: SignUpViewController.self)
        super.init(viewController: moduleViewController)

        let interactor = SignUpInteractor()
        let presenter = SignUpPresenter(view: moduleViewController, interactor: interactor, wireframe: self)
        moduleViewController.presenter = presenter
    }

}

// MARK: - Extensions -

extension SignUpWireframe: SignUpWireframeInterface {
    func navigate(to option: SignUpNaivgationOption) {
        switch option {
        case .home:
            let wireframe = MainWireframe()
            let presentViewControler = UINavigationController(rootViewController: wireframe.viewController)
            presentViewControler.modalPresentationStyle = .overFullScreen
            
            navigationController?.present(presentViewControler, animated: true, completion: nil)
        }
    }
}

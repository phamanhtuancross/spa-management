//
//  EventCell.swift
//  SpaManagement
//
//  Created by Tuan Pham on 10/11/2021.
//

import UIKit
import Reusable

struct EventItemViewModel {
    let model: Event
}

class EventCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var mainContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        mainContainerView.makeCornerRadidus(radius: 16)
        mainContainerView.backgroundColor = Colors.appColor.withAlphaComponent(0.35)
    }
    
    func configureCell(viewModel: EventItemViewModel) {}
}

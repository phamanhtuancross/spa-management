//
//  PaymentMethodCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 18/09/2021.
//

import UIKit
import Reusable

struct PaymentMethodItemViewModel {
    let paymentMethodType: PaymentMethodType
    
    var title: String {
        switch paymentMethodType {
        case .cash:
            return "Trả bằng tiền mặt"
            
        case .transfer:
            return "Chuyển khoản qua ngân hàng"
            
        case .momo:
            return "Chuyển qua Momo"
            
        case .free:
            return "Giao dịch miễn phí"
        }
    }
    
    var icon: UIImage? {
        switch paymentMethodType {
        case .cash:
            return Asset.Icon.Payment.icPaymentMoney.image
            
        case .transfer:
            return Asset.Icon.Payment.icPaymentTransfer.image
            
        case .momo:
            return Asset.Icon.Payment.icPaymentMomo.image
            
        case .free:
            return Asset.Icon.Payment.icPaymentFree.image
        }
    }
}

class PaymentMethodCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var iconContainerView: UIView!
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var dividerLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        titleLabel.setStyle(DS.T16M(color: Colors.ink500))
        dividerLineView.backgroundColor = Colors.blue200
    }
    
    func configureCell(viewModel: PaymentMethodItemViewModel) {
        titleLabel.text = viewModel.title
        iconImageView.image = viewModel.icon
        iconContainerView.makeAppBorderStyle()
        iconImageView.backgroundColor = .clear
    }
}

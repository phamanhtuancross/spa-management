//
//  TransactionDetailHeaderCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 06/10/2021.
//

import UIKit
import Reusable

struct TransactionDetailHeaderViewModel {
    let model: Transaction
    
    var toalCost: String {
        return model.total.toDouble.toVNDFormat()
    }
    
    var status: String {
        switch model.paymentStatus {
        case .received:
            return "Đã thanh toán"
        case .pending:
            return "Chưa thanh toán"
        }
    }
    
    var statusColor: UIColor {
        switch model.paymentStatus {
        case .received:
            return Colors.green500
        case .pending:
            return Colors.red500
        }
    }
}

class TransactionDetailHeaderCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var totalCostValueLabel: UILabel!
    @IBOutlet private weak var transactionStatusLabel: UILabel!
    @IBOutlet private weak var mainContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    private func setupView() {
        mainContainerView.makeAppBorderStyle()
    }
}

extension TransactionDetailHeaderCell {
    func configureCell(viewModel: TransactionDetailHeaderViewModel) {
        totalCostValueLabel.text = viewModel.toalCost
        transactionStatusLabel.text = viewModel.status
        transactionStatusLabel.textColor = viewModel.statusColor
    }
}

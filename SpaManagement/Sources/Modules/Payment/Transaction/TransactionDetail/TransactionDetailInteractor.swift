//
//  TransactionDetailInteractor.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 06/10/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import Foundation
import Resolver
import RxSwift

protocol TransactionDetailInteractorInterface: InteractorInterface {
    func updateTransactionStatus(_ status: PaymentStatus) -> Observable<Transaction>
}


final class TransactionDetailInteractor {
    @LazyInjected var transactionService: TransactionServiceType
    let transactionId: String
    
    init(transactionId: String) {
        self.transactionId = transactionId
    }
}

// MARK: - Extensions -

extension TransactionDetailInteractor: TransactionDetailInteractorInterface {
    func updateTransactionStatus(_ status: PaymentStatus) -> Observable<Transaction> {
        return transactionService.updateTransaction(for: transactionId, status: status, method: nil)
    }
}

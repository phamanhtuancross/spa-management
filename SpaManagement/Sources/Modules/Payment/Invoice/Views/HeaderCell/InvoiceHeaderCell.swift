//
//  InvoiceHeaderCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 16/09/2021.
//

import UIKit
import Reusable

struct InvoiceHeaderViewModel {
    let model: Customer
    
    var customerName: String {
        return model.name
    }
    
    var phoneNumber: String {
        return model.phoneNumber
    }
    
    var shortCutName: String {
        return String(model.name.split(separator: " ").last(where: { !$0.isEmpty }).or([]).first.or(Character("X")))
    }
}

class InvoiceHeaderCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var customerPhoneNumberLabel: UILabel!
    @IBOutlet private weak var shortcutNameLabel: UILabel!
    @IBOutlet private weak var customerNameLabel: UILabel!
    @IBOutlet private weak var avatarWrapView: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        avatarWrapView.makeCircular()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        mainContainerView.makeAppBorderStyle()
        
        customerNameLabel.setStyle(DS.T16M(color: Colors.ink500))
        customerPhoneNumberLabel.setStyle(DS.T16R(color: Colors.ink400s))
    }
}

extension InvoiceHeaderCell {
    func configureCell(viewModel: InvoiceHeaderViewModel) {
        customerNameLabel.text = viewModel.customerName.uppercased()
        customerPhoneNumberLabel.text = viewModel.phoneNumber
        shortcutNameLabel.text = viewModel.shortCutName
    }
}

//
//  AddInvoinceItemCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 16/09/2021.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

typealias VoidCallBack = () -> Void

class AddInvoinceItemCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var addButton: UIButton!
    
    public var didTap: VoidCallBack?
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        mainContainerView.makeAppBorderStyle()
        titleLabel.setStyle(DS.T14M(color: Colors.green500))
        titleLabel.text = "Thêm liệu trình / sản phẩm".uppercased()
        
        addButton.rx.tap
            .subscribeNext { [weak self] _ in
                guard let self = self else { return }
                self.didTap?()
            }
            .disposed(by: disposeBag)
    }
}

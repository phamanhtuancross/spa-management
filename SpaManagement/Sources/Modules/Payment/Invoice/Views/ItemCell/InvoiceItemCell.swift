//
//  InvoiceItemCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 16/09/2021.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

class InvoiceItemCell: UITableViewCell, NibReusable {

    @IBOutlet weak var mainContainerView: UIView!
    
    @IBOutlet private weak var invoiceDescriptionTitleLabel: UILabel!
    @IBOutlet private weak var invoiceDescriptionLabel: UILabel!
    
    @IBOutlet private weak var quantityTitleLabel: UILabel!
    @IBOutlet private weak var quantityLabel: UILabel!
    
    @IBOutlet private weak var priceTitleLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    
    @IBOutlet private weak var actionButtonsContainerView: UIView!
    @IBOutlet private weak var editButtonContainerView: UIView!
    @IBOutlet private weak var editButton: UIButton!
    
    @IBOutlet private weak var deleteButtonContainerView: UIView!
    @IBOutlet private weak var deleteButton: UIButton!
    
    private var style: Style = .mutable {
        didSet {
            updateStyle()
        }
    }
    
    private let disposeBag = DisposeBag()
    
    var onDelete: VoidCallBack?
    var onEdit: VoidCallBack?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        editButtonContainerView.makeCircular()
        editButtonContainerView.makeBorder()
        
        deleteButtonContainerView.makeCircular()
        deleteButtonContainerView.makeBorder()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
        setupActions()
    }
    
    private func setupView() {
        
        style = .mutable
        mainContainerView.makeAppBorderStyle()
        
        [
            invoiceDescriptionTitleLabel,
            quantityTitleLabel,
            priceTitleLabel
        ].forEach {
            $0?.setStyle(DS.T12R.init(color: Colors.ink400s))
        }
        
        [
            invoiceDescriptionLabel,
            quantityLabel,
            priceLabel
        ].forEach {
            $0?.setStyle(DS.T14M.init(color: Colors.ink500))
        }
    }
    
    private func setupActions() {
        deleteButton
            .rx.tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.onDelete?()
            }
            .disposed(by: disposeBag)
        
        editButton
            .rx.tap
            .subscribeNext { [weak self] in
                guard let self = self else { return }
                self.onEdit?()
            }
            .disposed(by: disposeBag)
    }
    
    private func updateStyle() {
        switch style {
        case .mutable:
            actionButtonsContainerView.isHidden = false
        case .immutable:
            actionButtonsContainerView.isHidden = true
        }
    }
}


extension InvoiceItemCell {
    enum Style {
        case mutable
        case immutable
    }
}

struct InvoiceItemViewModel {
    let model: Invoice
    var id: String {
        return model.productCode
    }
    
    var title: String {
        return model.title
    }
    
    var quatity: String {
        return "\(model.quantity)"
    }
    
    var totalPrice: String {
        return Double(model.quantity * model.pricePerItem).toVNDFormat()
    }
}

extension InvoiceItemCell {
    func configureCell(viewModel: InvoiceItemViewModel) {
        invoiceDescriptionLabel.text = viewModel.title
        quantityLabel.text = viewModel.quatity
        priceLabel.text = viewModel.totalPrice
    }
    
    func setStyle(_ style: Style) {
        self.style = style
    }
}

//
//  InvoiceHeaderSectionCell.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 16/09/2021.
//

import UIKit
import Reusable

class InvoiceHeaderSectionCell: UITableViewCell, NibReusable {
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {}
    
    func configureCell(title: String) {
        titleLabel.text = title.uppercased()
    }
}

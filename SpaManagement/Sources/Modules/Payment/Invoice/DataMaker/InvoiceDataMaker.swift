//
//  InvoiceDataMaker.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 05/10/2021.
//

import Foundation
import CoreMedia

class InvoiceDataMaker {
    
    let customer: Customer
    init(customer: Customer) {
        self.customer = customer
    }
    
    var invoiceCells: [InvoiceCellType] {
        
        
        return invoices.enumerated().map { .invoiceItem(viewModel: .init(model: $0.element)) }
    }
    
    var invoices: [Invoice] = []
    
    func makeCellTypes() -> [InvoiceCellType] {
        return [.sectionHeader(title: "Thông tin khách hàng"),
                .header(viewModel: .init(model: customer)),
                .sectionHeader(title: "Đơn hàng")] + invoiceCells + [.addInvoice]
    }
    
    var total: Int {
        return invoices.map { $0.quantity * $0.pricePerItem }
            .reduce(0, {$0 + $1})
    }
    
    var canPay: Bool {
        return invoices.count > .zero
    }
    
    @discardableResult
    func addInvoice(_ invoice: Invoice) -> Self {
        for index in 0..<invoices.count {
            if invoices[index].productCode == invoice.productCode {
                invoices[index] = .init(title: invoice.title,
                                        quantity: invoice.quantity + invoices[index].quantity,
                                        pricePerItem: invoice.pricePerItem,
                                        description: invoice.description,
                                        productCode: invoice.productCode)
                return self
            }
        }
        
        invoices.append(invoice)
        return self
    }
    
    @discardableResult
    func updateInvoice(_ invoice: Invoice) -> Self {
        
        for index in 0..<invoices.count {
            if invoices[index].productCode == invoice.productCode {
                invoices[index] = invoice
                return self
            }
        }
        
        invoices.append(invoice)
        return self
    }
    
    @discardableResult
    func deleteInvoce(byId id: String) -> Self {
        invoices = invoices.filter { $0.productCode != id }
        return self
    }
}

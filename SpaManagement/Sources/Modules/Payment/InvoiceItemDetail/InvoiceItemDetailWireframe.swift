//
//  InvoiceItemDetailWireframe.swift
//  SpaManagement
//
//  Created by PHAM ANH TUAN on 16/09/2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

enum EditableMode {
    case new
    case existed
}

final class InvoiceItemDetailWireframe: BaseWireframe {

    // MARK: - Private properties -

    private let storyboard = UIStoryboard(name: "InvoiceItemDetail", bundle: nil)
    private var presenter: InvoiceItemDetailPresenter?
    private var productsWireframe: ProductsWireframe?
    private let moduleViewController: InvoiceItemDetailViewController

    // MARK: - Module setup -

    init(listener: InvoiceItemDetailListener, product: Product, mode: EditableMode, quantity: Int = 1) {
        moduleViewController = storyboard.instantiateViewController(ofType: InvoiceItemDetailViewController.self)
        super.init(viewController: moduleViewController)

        let interactor = InvoiceItemDetailInteractor()
        presenter = InvoiceItemDetailPresenter(view: moduleViewController,
                                               interactor: interactor,
                                               wireframe: self,
                                               product: product,
                                               mode: mode,
                                               quantity: quantity)
        
        presenter?.listener = listener
        moduleViewController.presenter = presenter
    }

}

// MARK: - Extensions -

extension InvoiceItemDetailWireframe: InvoiceItemDetailWireframeInterface {
    func navigate(to option: InvoiceItemDetailNavigationOption) {
        switch option {
        case .products:
            guard let presenter = presenter else { return }
            productsWireframe = ProductsWireframe(viewMode: .viewOnly, listener: presenter)
            guard let wireframe = productsWireframe else { return }
            let presentVC = AppNavigationController(rootViewController: wireframe.viewController)
//            moduleViewController.present(presentVC, animated: true, completion: nil)
            moduleViewController.presenntWithCloseButton(viewController: presentVC)
        }
    }
    
    func detachProductsWireframe(completion: VoidCallBack?) {
        guard let wireframe = productsWireframe else { return }
        wireframe.viewController.dismiss(animated: true, completion: completion)
    }
}

extension UIViewController {
    @objc
    private func didTapClose() {
        dismiss(animated: true)
    }
}

//
//  SceneDelegate.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/4/21.
//

import UIKit
import Resolver
import Floaty

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var networkManager = NetworkManager.default
    var window: UIWindow?
    var rootWirframe: BaseWireframe!
    
    lazy var initializers: [Initializable] = [
        ThemeInitializer(),
        HUBInitializer()
    ]

    private func getCurrentWireFrame() -> BaseWireframe {
        if networkManager.isSignedIn() {
            return MainWireframe()
        }
        return SignInWireframe()
//        return OnBoardingWireframe()
    }
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        guard let windowScene = (scene as? UIWindowScene) else { return }
        initializers.forEach { $0.performInitialization() }
        Resolver.registerAppServices()
    
        self.window = UIWindow(windowScene: windowScene)
        
        let initialController = AppNavigationController()
        
        rootWirframe = getCurrentWireFrame()
        initialController.setRootWireframe(rootWirframe)
        
        self.window?.rootViewController = initialController
        self.window?.makeKeyAndVisible()
        
        PushNotificationManager.shared.register()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }

}


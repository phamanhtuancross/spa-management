//
//  ServicesRegister.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/6/21.
//

import Resolver
import IQKeyboardManager
import CoreGraphics
import Floaty
import Firebase

extension Resolver {
    static func registerAppServices() {
    
        registerFirebaseServices()
        
        register { CustomerService.default }
            .implements(CustomerServiceType.self)
        
        register { TransactionService.default }
            .implements(TransactionServiceType.self)
        
        register { UserService.default }
            .implements(UserServiceType.self)
        
        register { ProductService.default }
            .implements(ProductServiceType.self)
        
        register { SalesStatisticService.default }
            .implements(SalesStatisticServiceType.self)
        
        register { EventsService.default }
            .implements(EventsServiceType.self)
        
        register { IAPInternalService.default }
            .implements(IAPInternalServiceType.self)
        
        register { UploadFilesService.default }
            .implements(UploadFilesServiceType.self)
        
        registerKeyboard()
        setupNTP()
    }
    
    private static func registerKeyboard() {
        IQKeyboardManager.shared().isEnabled = true
    }
    
    private static func setupNTP() {
        DispatchQueue.global(qos: .background).async {
            NTPClient.default.syncDate()
        }
    }
    
    private static func registerFirebaseServices() {
    }
    
}

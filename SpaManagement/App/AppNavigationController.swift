//
//  MaiNgaBeatyMMNavigationController.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/9/21.
//

import UIKit

extension UIViewController {
    
    func presenntWithCloseButton(viewController: UIViewController) {
        
        guard let navController = viewController as? UINavigationController else { return }
        navController.children.first?.navigationItem.leftBarButtonItem = .closeItem(target: self,
                                                                                    action: #selector(didTapCloseButton))
//                                                                                        .init(title: "Đóng",
//                                                                               style: .plain,
//                                                                               target: self,
//                                                                               action: #selector(didTapCloseButton))
        
        self.present(navController, animated: true, completion: nil)
    }
    
    @objc
    private func didTapCloseButton() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc
    private func didTapBackButton() {
        navigationController?.popViewController(animated: true)
    }
    
    func setupAppBackButton() {
        navigationItem.leftBarButtonItem = .backItem(target: self,
                                                                           action: #selector(didTapBackButton))
    }
}

class AppNavigationController: UINavigationController, UIGestureRecognizerDelegate, UINavigationControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self
        delegate = self
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        interactivePopGestureRecognizer?.isEnabled = false
    }
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        interactivePopGestureRecognizer?.isEnabled = true
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
}

extension UINavigationController {
    func hideShadowLine() {
        self.navigationBar.setValue(true, forKey: "hidesShadow")
    }
}

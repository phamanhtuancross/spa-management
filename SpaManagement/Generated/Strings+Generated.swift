// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {

  internal enum ChangePassword {
    internal enum Validator {
      internal enum InvalidConfirmNewPassword {
        /// Mật khẩu xác nhận không giống mật khẩu mới!
        internal static let message = L10n.tr("Localizable", "changePassword.validator.invalidConfirmNewPassword.message")
      }
      internal enum InvalidNewPassword {
        /// Mật khẩu mới không hợp lệ.\n Mật khẩu phải chứa số và kí tự In Hoa
        internal static let message = L10n.tr("Localizable", "changePassword.validator.invalidNewPassword.message")
      }
      internal enum InvalidNewPasswordIsTheSameCurrent {
        /// Mật khẩu mới không được giống mật khẩu hiện tại!
        internal static let message = L10n.tr("Localizable", "changePassword.validator.invalidNewPasswordIsTheSameCurrent.message")
      }
      internal enum InvalidOldPassword {
        /// Mật khẩu cũ không hợp lệ.\n Mật khẩu phải chứa số và kí tự In Hoa
        internal static let message = L10n.tr("Localizable", "changePassword.validator.invalidOldPassword.message")
      }
    }
  }

  internal enum Common {
    /// https://apps.apple.com/vn/app/spalux/id1586460273
    internal static let appLink = L10n.tr("Localizable", "common.appLink")
    /// Thông báo
    internal static let notiftyTitle = L10n.tr("Localizable", "common.notiftyTitle")
    /// 0335558699
    internal static let phoneNumber = L10n.tr("Localizable", "common.phoneNumber")
    internal enum Erorro {
      internal enum Unknow {
        /// Lỗi không xác định. vui lòng thử lại!
        internal static let description = L10n.tr("Localizable", "common.erorro.unknow.description")
      }
    }
    internal enum Loading {
      internal enum Normal {
        /// Đang xử lý...
        internal static let title = L10n.tr("Localizable", "common.loading.normal.title")
      }
    }
    internal enum Product {
      internal enum Create {
        /// Thêm sản phẩm thành công
        internal static let success = L10n.tr("Localizable", "common.product.create.success")
      }
    }
  }

  internal enum InvoiceDetail {
    internal enum AddProduct {
      internal enum Added {
        internal enum Failed {
          /// Thêm sản phẩm thất bại. Bạn hãy thử lại nhé
          internal static let message = L10n.tr("Localizable", "invoiceDetail.addProduct.added.failed.message")
        }
        internal enum Success {
          /// Thêm sản phẩm thành công
          internal static let message = L10n.tr("Localizable", "invoiceDetail.addProduct.added.success.message")
        }
      }
      internal enum Adding {
        /// Đang thêm sản phẩm...
        internal static let message = L10n.tr("Localizable", "invoiceDetail.addProduct.adding.message")
      }
    }
  }

  internal enum Profile {
    internal enum Purchase {
      /// Nếu bạn chưa đăng kí gói cước nào,  để sử dụng phần mềm lâu dài vui lòng đăng kí. Hoặc Nếu bạn đã đăng kí  và  muốn thay đổi gói cước hiện tại. Bấm để đăng kí.
      internal static let description = L10n.tr("Localizable", "profile.purchase.description")
      /// Chọn gói đăng kí
      internal static let title = L10n.tr("Localizable", "profile.purchase.title")
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type

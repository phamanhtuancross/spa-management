// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accentColor = ColorAsset(name: "AccentColor")
  internal enum Banner {
    internal static let bnCustomerDetail = ImageAsset(name: "bn_customer_detail")
    internal static let bnFashionLogin = ImageAsset(name: "bn_fashion_login")
    internal enum Calendar {
      internal static let _2760249 = ImageAsset(name: "2760249")
    }
  }
  internal enum Icon {
    internal enum BarButton {
      internal static let icBackButton = ImageAsset(name: "ic_back_button")
      internal static let icCloseButton = ImageAsset(name: "ic_close_button")
    }
    internal enum Calendar {
      internal static let date = ImageAsset(name: "date")
      internal static let dateNormal = ImageAsset(name: "date_normal")
      internal static let icCheckMark = ImageAsset(name: "ic_check_mark")
    }
    internal enum Common {
      internal static let icHideShowPassword = ImageAsset(name: "ic_hide_show_password")
      internal static let icShowPassword = ImageAsset(name: "ic_show_password")
    }
    internal enum ConnectedApp {
      internal static let icConnectedAddTransaction = ImageAsset(name: "ic_connected_add_transaction")
      internal static let icConnectedCustomer = ImageAsset(name: "ic_connected_customer")
      internal static let icConnectedNotify = ImageAsset(name: "ic_connected_notify")
      internal static let icConnectedStats = ImageAsset(name: "ic_connected_stats")
    }
    internal enum Events {
      internal enum Detail {
        internal static let icEventDetail = ImageAsset(name: "ic_event_detail")
      }
      internal static let icEvents = ImageAsset(name: "ic_events")
    }
    internal enum SalesOverview {
      internal static let icSalesOverViewHeart = ImageAsset(name: "ic_salesOverView_heart")
      internal static let icSalesOverviewCoins = ImageAsset(name: "ic_salesOverview_coins")
    }
    internal enum Statistic {
      internal static let icNewUsers = ImageAsset(name: "ic_new_users")
      internal static let icTotalSalesCost = ImageAsset(name: "ic_total_sales_cost")
    }
    internal enum Tabbar {
      internal static let icCollectionTab = ImageAsset(name: "ic_collection_tab")
      internal static let icHomeTab = ImageAsset(name: "ic_home_tab")
      internal static let icProductsTab = ImageAsset(name: "ic_products_tab")
      internal static let icSearchTab = ImageAsset(name: "ic_search_tab")
      internal static let icSettingsTab = ImageAsset(name: "ic_settings_tab")
    }
    internal enum Transaction {
      internal static let icFilter = ImageAsset(name: "ic_filter")
    }
    internal enum Category {
      internal static let icShowMeTheBank = ImageAsset(name: "ic_show_me_the_bank")
      internal static let icStatistics = ImageAsset(name: "ic_statistics")
      internal static let icUsers = ImageAsset(name: "ic_users")
    }
    internal static let icAdd = ImageAsset(name: "ic_add")
    internal static let icCancel = ImageAsset(name: "ic_cancel")
    internal static let icCellPhone = ImageAsset(name: "ic_cell_phone")
    internal static let icCreateInvoice = ImageAsset(name: "ic_create_invoice")
    internal static let icDelete = ImageAsset(name: "ic_delete")
    internal static let icEdit = ImageAsset(name: "ic_edit")
    internal static let icEye = ImageAsset(name: "ic_eye")
    internal static let icFacebook = ImageAsset(name: "ic_facebook")
    internal static let icMoney = ImageAsset(name: "ic_money")
    internal static let icPayment = ImageAsset(name: "ic_payment")
    internal static let icProfileName = ImageAsset(name: "ic_profile_name")
    internal enum Payment {
      internal static let icPaymentFree = ImageAsset(name: "ic_payment_free")
      internal static let icPaymentMomo = ImageAsset(name: "ic_payment_momo")
      internal static let icPaymentMoney = ImageAsset(name: "ic_payment_money")
      internal static let icPaymentTransfer = ImageAsset(name: "ic_payment_transfer")
    }
    internal enum Ranking {
      internal static let icRankBroze = ImageAsset(name: "ic_rank_broze")
      internal static let icRankGold = ImageAsset(name: "ic_rank_gold")
      internal static let icRankSilver = ImageAsset(name: "ic_rank_silver")
    }
  }
  internal enum Image {
    internal enum OnBoarding {
      internal static let imOnboardingFinal = ImageAsset(name: "im_onboarding_final")
      internal static let imOnboardingFunctional = ImageAsset(name: "im_onboarding_functional")
      internal static let imOnboardingIntro = ImageAsset(name: "im_onboarding_intro")
    }
    internal enum Placeholder {
      internal static let imAppPlaceholder = ImageAsset(name: "im_app_placeholder")
    }
  }
  internal static let image1 = ImageAsset(name: "Image-1")
  internal static let image = ImageAsset(name: "Image")
  internal static let appLogo = ImageAsset(name: "app_logo")
  internal static let bgStretchPlaceHolder = ImageAsset(name: "bg_stretch_place_holder")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = {
    guard let color = Color(asset: self) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }()

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init?(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
}

internal extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type

// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length implicit_return

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum BannerTemplate: StoryboardType {
    internal static let storyboardName = "BannerTemplate"

    internal static let bannerTemplateViewController = SceneType<BannerTemplateViewController>(storyboard: BannerTemplate.self, identifier: "BannerTemplateViewController")
  }
  internal enum CalendarEditable: StoryboardType {
    internal static let storyboardName = "CalendarEditable"

    internal static let calendarEditableViewController = SceneType<CalendarEditableViewController>(storyboard: CalendarEditable.self, identifier: "CalendarEditableViewController")
  }
  internal enum ChangePassword: StoryboardType {
    internal static let storyboardName = "ChangePassword"

    internal static let changePasswordViewController = SceneType<ChangePasswordViewController>(storyboard: ChangePassword.self, identifier: "ChangePasswordViewController")
  }
  internal enum CommingSoon: StoryboardType {
    internal static let storyboardName = "CommingSoon"

    internal static let commingSoonViewController = SceneType<CommingSoonViewController>(storyboard: CommingSoon.self, identifier: "CommingSoonViewController")
  }
  internal enum ConnectedAppWidget: StoryboardType {
    internal static let storyboardName = "ConnectedAppWidget"

    internal static let connectedAppWidgetViewController = SceneType<ConnectedAppWidgetViewController>(storyboard: ConnectedAppWidget.self, identifier: "ConnectedAppWidgetViewController")
  }
  internal enum CustomerDetail: StoryboardType {
    internal static let storyboardName = "CustomerDetail"

    internal static let customerDetailViewController = SceneType<CustomerDetailViewController>(storyboard: CustomerDetail.self, identifier: "CustomerDetailViewController")
  }
  internal enum CustomerEditableForm: StoryboardType {
    internal static let storyboardName = "CustomerEditableForm"

    internal static let customerEditableFormViewController = SceneType<CustomerEditableFormViewController>(storyboard: CustomerEditableForm.self, identifier: "CustomerEditableFormViewController")
  }
  internal enum CustomerList: StoryboardType {
    internal static let storyboardName = "CustomerList"

    internal static let customerListViewController = SceneType<CustomerListViewController>(storyboard: CustomerList.self, identifier: "CustomerListViewController")
  }
  internal enum EventDetail: StoryboardType {
    internal static let storyboardName = "EventDetail"

    internal static let eventDetailViewController = SceneType<EventDetailViewController>(storyboard: EventDetail.self, identifier: "EventDetailViewController")
  }
  internal enum Events: StoryboardType {
    internal static let storyboardName = "Events"

    internal static let eventsViewController = SceneType<EventsViewController>(storyboard: Events.self, identifier: "EventsViewController")
  }
  internal enum EventsWidget: StoryboardType {
    internal static let storyboardName = "EventsWidget"

    internal static let eventsWidgetViewController = SceneType<EventsWidgetViewController>(storyboard: EventsWidget.self, identifier: "EventsWidgetViewController")
  }
  internal enum Home: StoryboardType {
    internal static let storyboardName = "Home"

    internal static let homeViewController = SceneType<HomeViewController>(storyboard: Home.self, identifier: "HomeViewController")
  }
  internal enum Invoice: StoryboardType {
    internal static let storyboardName = "Invoice"

    internal static let invoiceViewController = SceneType<InvoiceViewController>(storyboard: Invoice.self, identifier: "InvoiceViewController")
  }
  internal enum InvoiceItemDetail: StoryboardType {
    internal static let storyboardName = "InvoiceItemDetail"

    internal static let invoiceItemDetailViewController = SceneType<InvoiceItemDetailViewController>(storyboard: InvoiceItemDetail.self, identifier: "InvoiceItemDetailViewController")
  }
  internal enum LaunchScreen: StoryboardType {
    internal static let storyboardName = "LaunchScreen"

    internal static let initialScene = InitialSceneType<UIKit.UIViewController>(storyboard: LaunchScreen.self)
  }
  internal enum Main: StoryboardType {
    internal static let storyboardName = "Main"

    internal static let initialScene = InitialSceneType<MainViewController>(storyboard: Main.self)

    internal static let mainViewController = SceneType<MainViewController>(storyboard: Main.self, identifier: "MainViewController")
  }
  internal enum OnBoarding: StoryboardType {
    internal static let storyboardName = "OnBoarding"

    internal static let onBoardingViewController = SceneType<OnBoardingViewController>(storyboard: OnBoarding.self, identifier: "OnBoardingViewController")
  }
  internal enum PaymentMethod: StoryboardType {
    internal static let storyboardName = "PaymentMethod"

    internal static let paymentMethodViewController = SceneType<PaymentMethodViewController>(storyboard: PaymentMethod.self, identifier: "PaymentMethodViewController")
  }
  internal enum ProductEditable: StoryboardType {
    internal static let storyboardName = "ProductEditable"

    internal static let productEditableViewController = SceneType<ProductEditableViewController>(storyboard: ProductEditable.self, identifier: "ProductEditableViewController")
  }
  internal enum Products: StoryboardType {
    internal static let storyboardName = "Products"

    internal static let productsViewController = SceneType<ProductsViewController>(storyboard: Products.self, identifier: "ProductsViewController")
  }
  internal enum Profile: StoryboardType {
    internal static let storyboardName = "Profile"

    internal static let profileViewController = SceneType<ProfileViewController>(storyboard: Profile.self, identifier: "ProfileViewController")
  }
  internal enum Purchase: StoryboardType {
    internal static let storyboardName = "Purchase"

    internal static let purchaseViewController = SceneType<PurchaseViewController>(storyboard: Purchase.self, identifier: "PurchaseViewController")
  }
  internal enum RankingWidget: StoryboardType {
    internal static let storyboardName = "RankingWidget"

    internal static let rankingWidgetViewController = SceneType<RankingWidgetViewController>(storyboard: RankingWidget.self, identifier: "RankingWidgetViewController")
  }
  internal enum SalesOverviewWidget: StoryboardType {
    internal static let storyboardName = "SalesOverviewWidget"

    internal static let salesOverviewWidgetViewController = SceneType<SalesOverviewWidgetViewController>(storyboard: SalesOverviewWidget.self, identifier: "SalesOverviewWidgetViewController")
  }
  internal enum SalesStatistic: StoryboardType {
    internal static let storyboardName = "SalesStatistic"

    internal static let salesStatisticViewController = SceneType<SalesStatisticViewController>(storyboard: SalesStatistic.self, identifier: "SalesStatisticViewController")
  }
  internal enum SignIn: StoryboardType {
    internal static let storyboardName = "SignIn"

    internal static let signInViewController = SceneType<SignInViewController>(storyboard: SignIn.self, identifier: "SignInViewController")
  }
  internal enum SignUp: StoryboardType {
    internal static let storyboardName = "SignUp"

    internal static let signUpViewController = SceneType<SignUpViewController>(storyboard: SignUp.self, identifier: "SignUpViewController")
  }
  internal enum StatisticFilter: StoryboardType {
    internal static let storyboardName = "StatisticFilter"

    internal static let statisticFilterViewController = SceneType<StatisticFilterViewController>(storyboard: StatisticFilter.self, identifier: "StatisticFilterViewController")
  }
  internal enum StatisticsWidget: StoryboardType {
    internal static let storyboardName = "StatisticsWidget"

    internal static let statisticsWidgetViewController = SceneType<StatisticsWidgetViewController>(storyboard: StatisticsWidget.self, identifier: "StatisticsWidgetViewController")
  }
  internal enum TopSoldPorductWidget: StoryboardType {
    internal static let storyboardName = "TopSoldPorductWidget"

    internal static let topSoldPorductWidgetViewController = SceneType<TopSoldPorductWidgetViewController>(storyboard: TopSoldPorductWidget.self, identifier: "TopSoldPorductWidgetViewController")
  }
  internal enum TransactionDetail: StoryboardType {
    internal static let storyboardName = "TransactionDetail"

    internal static let transactionDetailViewController = SceneType<TransactionDetailViewController>(storyboard: TransactionDetail.self, identifier: "TransactionDetailViewController")
  }
  internal enum TransactionFilter: StoryboardType {
    internal static let storyboardName = "TransactionFilter"

    internal static let transactionFilterViewController = SceneType<TransactionFilterViewController>(storyboard: TransactionFilter.self, identifier: "TransactionFilterViewController")
  }
  internal enum TransactionList: StoryboardType {
    internal static let storyboardName = "TransactionList"

    internal static let transactionListViewController = SceneType<TransactionListViewController>(storyboard: TransactionList.self, identifier: "TransactionListViewController")
  }
  internal enum UserProfileWidget: StoryboardType {
    internal static let storyboardName = "UserProfileWidget"

    internal static let userProfileWidgetViewController = SceneType<UserProfileWidgetViewController>(storyboard: UserProfileWidget.self, identifier: "UserProfileWidgetViewController")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: BundleToken.bundle)
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    return storyboard.storyboard.instantiateViewController(identifier: identifier, creator: block)
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController(creator: block) else {
      fatalError("Storyboard \(storyboard.storyboardName) does not have an initial scene.")
    }
    return controller
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type

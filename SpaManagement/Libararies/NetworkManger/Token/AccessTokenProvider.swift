//
//  AccessTokenProvider.swift
//  Beepee
//
//  Created by PHAM ANH TUAN on 3/6/21.
//

import Foundation
import Moya

class BeepeeJsonDecoder {
    
}


protocol AccesTokenProviderType {
    var currentToken: JWTToken { get }
    func store(token: JWTToken)
    func clearToken()
}


class RefreshTokenAPIProvider: MoyaProvider<MultiTarget> {
    static let `default` = RefreshTokenAPIProvider()
}


class AccessTokenProvider: AccesTokenProviderType {
    
    static let `default` = AccessTokenProvider(apiProvider: RefreshTokenAPIProvider.default, accessTokenStorage: KeychainAccessTokenStorage.default)
    
    private let apiProvider: MoyaProvider<MultiTarget>
    private let accessTokenStorage: AccessTokenStoragable
    init(apiProvider: MoyaProvider<MultiTarget>, accessTokenStorage: AccessTokenStoragable) {
        self.apiProvider = apiProvider
        self.accessTokenStorage = accessTokenStorage
    }
    
    var currentToken: JWTToken {
        return accessTokenStorage.currentToken
    }
    
    func store(token: JWTToken) {
        accessTokenStorage.store(token: token)
    }
    
    func clearToken() {
        accessTokenStorage.clearToken()
    }
}

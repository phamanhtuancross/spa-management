//
//  NetworkManager.swift
//  Beepee
//
//  Created by PHAM ANH TUAN on 3/6/21.
//

import Foundation
import Moya
import Alamofire
import RxSwift
import RxCocoa
import Accelerate

protocol Networkable {
    func request(target: TargetType) -> Single<Response>
    func authenticateRequest(target: TargetType, refreshTokenOnFailure: Bool) -> Single<Response>
    func storage(tokenInString: String)
    func clearToken()
    func isSignedIn() -> Bool
}

class APIProvider: MoyaProvider<MultiTarget> {
    static let `default` = APIProvider()
    
    public static var plugins: [PluginType] {
        
        let accessTokenProvider = AccessTokenProvider.default
        return [
            NetworkLoggerPlugin(configuration: .init(logOptions: .verbose)),
            AccessTokenPlugin(tokenClosure: { _ -> String in
                return accessTokenProvider.currentToken.accessToken
            })
        ]
    }
    
    init(endpointClosure: @escaping EndpointClosure = MoyaProvider<MultiTarget>.defaultEndpointMapping,
         requestClosure: @escaping RequestClosure = MoyaProvider<MultiTarget>.defaultRequestMapping,
         stubClosure: @escaping StubClosure = MoyaProvider.neverStub,
         callbackQueue: DispatchQueue? = nil,
         plugins: [PluginType] = APIProvider.plugins,
         trackInflights: Bool = false,
         allowCache: Bool = true) {
        
        super.init(
            endpointClosure: endpointClosure,
            requestClosure: requestClosure,
            stubClosure: stubClosure,
            callbackQueue: callbackQueue,
            session: MoyaProvider<MultiTarget>.defaultAlamofireSession(),
            plugins: plugins,
            trackInflights: trackInflights
        )
    }
}

class NetworkManager: Networkable {
    
    static let `default` = NetworkManager(provider: APIProvider.default, tokenProvider: AccessTokenProvider.default)
    
    private let provider: MoyaProvider<MultiTarget>
    private let tokenProvider: AccesTokenProviderType
    
    init(provider: MoyaProvider<MultiTarget>, tokenProvider: AccesTokenProviderType = AccessTokenProvider.default) {
        self.provider = provider
        self.tokenProvider = tokenProvider
    }
    
    func request(target: TargetType) -> Single<Response> {
        return provider.rx.request(MultiTarget(target)).catchAppError().filterSuccessfulStatusCodes()
    }
    
    func authenticateRequest(target: TargetType, refreshTokenOnFailure: Bool) -> Single<Response> {
        fatalError("Not implementation method")
    }
    
    func storage(tokenInString: String) {
        tokenProvider.store(token: JWTToken(type: "Bearer", accessToken: tokenInString, refreshToken: tokenInString))
    }
    
    func clearToken() {
        tokenProvider.clearToken()
    }
    
    func isSignedIn() -> Bool {
        return tokenProvider.currentToken.accessToken.isNotEmpty
    }
}

struct AppErrorResponse: Decodable {
    let status: String
    let data: AppErrorData
}
struct AppErrorData: Error, Decodable {
    let errorCode: String
    let message: String
    let detail: String
    
    var errorCodeType: AppError {
        return .init(errorCode: errorCode)
    }
}

enum AppError: Error {
    case authenticateError
    case invalidLoginInfo
    case productNotFoundError
    case internalServerError
    case badGateWayError
    case existedError
    case unknow
    
    init(errorCode: String) {
        switch errorCode {
        case "401000":
            self = .authenticateError
            
        case "401002":
            self = .invalidLoginInfo
            
        case "404601":
            self = .productNotFoundError
            
        case "500001":
            self = .internalServerError
            
        case "400000":
            self = .badGateWayError
            
        case "400200":
            self = .existedError
            
        default:
            self = .unknow
        }
    }
}

extension PrimitiveSequence where Trait == SingleTrait, Element == Response {
    
    func catchAppError() -> Single<Element> {
        return flatMap { response in
            guard (200...299).contains(response.statusCode) else {
                do {
                    let appError = try response.map(AppErrorResponse.self)
                    throw appError.data
                } catch {
                    throw error
                }
            }
            
            return .just(response)
        }
    }
}
